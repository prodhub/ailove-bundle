<?php

namespace ADW\AiloveBundle;

/**
 * Class AiloveEvents.
 *
 * @author Artur Vesker
 */
class AiloveEvents
{
    const POST_USER_DECORATED = 'ailove.post_user_decorated';
}
