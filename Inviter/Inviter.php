<?php

namespace ADW\AiloveBundle\Inviter;

use ADW\AiloveBundle\Model\InvitationCode;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\User\Invitation\ActivateInvitationCodeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Invitation\GenerateInvitationCodeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Invitation\ListInvitationCodesMethodDescription;
use ADW\CommonBundle\Exception\ValidationViolationException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class Inviter.
 *
 * @author Artur Vesker
 */
class Inviter
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param Client                $client
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Client $client, TokenStorageInterface $tokenStorage)
    {
        $this->client = $client;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Create new invitation code.
     *
     * @param User $user
     * @param $program
     * @param InviteStrategyInterface $strategy
     *
     * @return \ADW\AiloveBundle\Model\User\Invitation\InvitationCode
     */
    public function generateCode(User $user, $program, InviteStrategyInterface $strategy = null, array $strategyOptions = [])
    {
        $options = [
            'uid' => $user->getId(),
            'program' => $program,
        ];

        if ($strategy) {
            $options['additional'] = array_merge($strategy->getExtra($user, $strategyOptions), [
                'promo' => $strategy->getPromo(),
                'selector' => $strategy->getSelector($user, $strategyOptions),
            ]);
        }

        return $this->client->request(new GenerateInvitationCodeMethodDescription(), $options);
    }

    /**
     * Invite user.
     *
     * @param InviteStrategyInterface $strategy
     * @param $program
     * @param array $options
     *
     * @return mixed
     */
    public function invite(InviteStrategyInterface $strategy, $program, array $options)
    {
        $optionsResolver = new OptionsResolver();
        $strategy->configureOptions($optionsResolver);
        $options = $optionsResolver->resolve($options);

        $inviter = $this->tokenStorage->getToken()->getUser();

        $code = $this->generateCode($inviter, $program, $strategy, $options);
        $strategy->handle($inviter, $program, $code, $options);

        return $code;
    }

    /**
     * Check code is valid.
     *
     * @param $code
     *
     * @return bool
     */
    public function isValidCode($code)
    {
        try {
            return $this->client->request(new ActivateInvitationCodeMethodDescription(), [
                'check_only' => true,
                'code' => (string) $code,
                'uid' => (int) $this->getCurrentUser()->getId(),
            ]);
        } catch (ValidationViolationException $e) {
            return false;
        }
    }

    /**
     * Activate invitation code for current user.
     *
     * @param $code
     *
     * @return InvitationCode
     */
    public function activateCode($code)
    {
        return $this->client->request(new ActivateInvitationCodeMethodDescription(), [
            'check_only' => false,
            'code' => (string) $code,
            'uid' => (int) $this->getCurrentUser()->getId(),

        ]);
    }

    /**
     *
     */
    public function getInvitationCodes()
    {
        return $this->client->request(new ListInvitationCodesMethodDescription(), ['uid' => $this->getCurrentUser()->getId()]);
    }

    /**
     * @return User
     */
    protected function getCurrentUser()
    {
        if (!$token = $this->tokenStorage->getToken()) {
            throw new \RuntimeException('Token not provided');
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new \RuntimeException('User must be instance ADW\AiloveBundle\Model\User');
        }

        return $user;
    }
}
