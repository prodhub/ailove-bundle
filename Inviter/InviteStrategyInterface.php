<?php

namespace ADW\AiloveBundle\Inviter;

use ADW\AiloveBundle\Model\User;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Interface StrategyInterface.
 */
interface InviteStrategyInterface
{
    /**
     * @param User  $inviter
     * @param array $options
     *
     * @return mixed
     */
    public function getExtra(User $inviter, array $options);

    /**
     * @return string
     */
    public function getPromo();

    /**
     * @return string
     */
    public function getSelector(User $inviter, array $options);

    /**
     * @param OptionsResolver $optionsResolver
     */
    public function configureOptions(OptionsResolver $optionsResolver);

    public function handle(User $inviter, $program, User\Invitation\InvitationCode $code, array $options);
}
