<?php

namespace ADW\AiloveBundle\Event;

use ADW\AiloveBundle\Model\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UnknownUserEvent.
 *
 * @author Artur Vesker
 */
class UnknownUserEvent extends Event
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
