<?php

namespace ADW\AiloveBundle\Event;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Security\User\UserDecoratorInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserDecoratedEvent.
 *
 * @author Artur Vesker
 */
class UserDecoratedEvent extends Event
{
    /**
     * @var UserDecoratorInterface
     */
    protected $decoratedUser;

    /**
     * @param UserDecoratorInterface $decoratedUser
     */
    public function __construct(UserDecoratorInterface $decoratedUser)
    {
        $this->decoratedUser = $decoratedUser;
    }

    /**
     * @return User
     */
    public function getDecoratedUser()
    {
        return $this->decoratedUser;
    }
}
