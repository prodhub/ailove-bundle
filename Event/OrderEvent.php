<?php

namespace ADW\AiloveBundle\Event;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Order\Order;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderEvent
 *
 * @package ADW\AiloveBundle\Event
 * @author Artur Vesker
 */
class OrderEvent extends Event
{

    const NAME = 'ailove.order';

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var mixed
     */
    protected $result;

    /**
     * @var User
     */
    protected $user;

    /**
     * OrderEvent constructor.
     *
     * @param Order $order
     * @param User $user
     * @param mixed $result
     */
    public function __construct(Order $order, User $user, $result)
    {
        $this->order = $order;
        $this->user = $user;
        $this->result = $result;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}