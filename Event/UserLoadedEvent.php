<?php

namespace ADW\AiloveBundle\Event;

use ADW\AiloveBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserLoadedEvent.
 *
 * @author Artur Vesker
 */
class UserLoadedEvent extends Event
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
