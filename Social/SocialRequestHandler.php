<?php

namespace ADW\AiloveBundle\Social;

use ADW\AiloveBundle\Model\User;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SocialRequestHandler
 *
 * @package ADW\AiloveBundle\Social
 * @author Artur Vesker
 */
class SocialRequestHandler
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Request $request
     * @return SocialRequestHandler
     */
    public static function create(Request $request)
    {
        return new self($request);
    }

    /**
     * @param User $user
     * @return User
     */
    public function setSocialData(User $user)
    {
        $user->setFirstName($this->request->get('firstname'));
        $user->setLastName($this->request->get('lastname'));

        if ($socialType = $this->request->get('social_type')) {
            $socialSetter = 'set' . ucfirst($socialType) . 'Id';
            $user->$socialSetter($this->request->get('social_id'));
        }

        return $user;
    }

}