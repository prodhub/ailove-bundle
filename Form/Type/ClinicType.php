<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Repository\ClinicRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ClinicType.
 *
 * @author Artur Vesker
 */
class ClinicType extends AbstractType
{
    /**
     * @var ClinicRepository
     */
    protected $repository;

    /**
     * @param ClinicRepository $repository
     */
    public function __construct(ClinicRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choice_list' => new ObjectChoiceList($this->repository->findAll(), 'name', [], null, 'systemName'),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_clinic';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
