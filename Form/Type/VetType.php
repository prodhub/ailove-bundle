<?php

namespace ADW\AiloveBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VetType.
 *
 * @author Artur Vesker
 */
class VetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institution', 'ailove_institution')
            ->add('graduationYear', null, ['required' => true])
            ->add('clinic', 'ailove_clinic', ['required' => true]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'ADW\AiloveBundle\Model\VetData');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'vet';
    }
}
