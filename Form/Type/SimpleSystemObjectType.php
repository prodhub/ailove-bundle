<?php

namespace ADW\AiloveBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SimpleSystemObjectType
 *
 * @package ADW\AiloveBundle\Form\Type
 * @author Artur Vesker
 */
class SimpleSystemObjectType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('systemName', 'text');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', 'ADW\AiloveBundle\Model\SimpleSystemObject');
    }

    public function getName()
    {
        return 'simple_system_object';
    }

}