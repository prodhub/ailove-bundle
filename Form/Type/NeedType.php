<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\Repository\NeedRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class NeedType.
 *
 * @author Artur Vesker
 */
class NeedType extends AbstractType
{
    /**
     * @var NeedRepository
     */
    protected $repository;

    /**
     * @param NeedRepository $needRepository
     */
    public function __construct(NeedRepository $needRepository)
    {
        $this->repository = $needRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $choiceList = function (Options $options) {
            return new ObjectChoiceList($this->repository->findByPetType($options['type']), 'name', [], null, 'systemName');
        };

        $resolver
            ->setRequired('type')
            ->setDefault('allow_delete', true)
            ->setAllowedValues('type', [Pet::TYPE_CAT, Pet::TYPE_DOG])
            ->setDefaults([
                'choice_list' => $choiceList,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['pet_type'] = $options['type'];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_need';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
