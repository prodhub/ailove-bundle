<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\Repository\BreedRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class BreedType.
 *
 * @author Artur Vesker
 */
class BreedType extends AbstractType
{
    /**
     * @var BreedRepository
     */
    protected $repository;

    /**
     * @param BreedRepository $breedRepository
     */
    public function __construct(BreedRepository $breedRepository)
    {
        $this->repository = $breedRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $choiceList = function (Options $options) {
            return new ObjectChoiceList($this->repository->findByPetType($options['type']), 'name', [], null, 'systemName');
        };

        $resolver
            ->setRequired('type')
            ->setAllowedValues('type', [Pet::TYPE_CAT, Pet::TYPE_DOG])
            ->setDefaults([
                'multiple' => false,
                'choice_list' => $choiceList,
                'placeholder' => 'Выберите породу'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_breed';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
