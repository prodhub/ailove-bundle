<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Repository\InstitutionRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstitutionType.
 *
 * @author Artur Vesker
 */
class InstitutionType extends AbstractType
{
    /**
     * @var InstitutionRepository
     */
    protected $repository;

    /**
     * @param InstitutionRepository $repository
     */
    public function __construct(InstitutionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choice_list' => new ObjectChoiceList($this->repository->findAll(), 'name', [], null, 'systemName'),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_institution';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
