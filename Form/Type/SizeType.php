<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Repository\DogSizeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SizeType.
 *
 * @author Artur Vesker
 */
class SizeType extends AbstractType
{
    /**
     * @var DogSizeRepository
     */
    protected $repository;

    /**
     * @param DogSizeRepository $dogSizeRepository
     */
    public function __construct(DogSizeRepository $dogSizeRepository)
    {
        $this->repository = $dogSizeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => '\ADW\AiloveBundle\Model\Pet\Size',
                'choice_list' => new ObjectChoiceList($this->repository->findAll(), 'name', [], null, 'systemName'),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_size';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
