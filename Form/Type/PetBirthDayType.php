<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Form\DataTransformer\PetBirthDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PetBirthDayType
 *
 * @package ADW\AiloveBundle\Form\Type
 * @author Artur Vesker
 */
class PetBirthDayType extends AbstractType
{

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new PetBirthDateTransformer());
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('block_name', 'pet_birth_day');
    }

    /**
     * @inheritdoc
     */
    public function getParent()
    {
        return TextType::class;
    }


}