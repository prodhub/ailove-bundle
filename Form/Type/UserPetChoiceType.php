<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Pet\Need;
use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\Repository\PetRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserPetType
 *
 * @author Artur Vesker
 */
class UserPetChoiceType extends AbstractType
{

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choices' => function(Options $options) {
                    return $options['repository']->findByType($options['pet_type']);
                },
                'choices_as_values' => true,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'pet_type' => null
            ])
            ->setRequired('repository')
            ->setAllowedTypes('repository', [PetRepository::class])
        ;
    }

    /**
     * @inheritdoc
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

}