<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Form\DataTransformer\SonataMediaToUploadedFileTransformer;
use ADW\AiloveBundle\Model\UploadImage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AvatarType
 *
 * @package ADW\AiloveBundle\Form\Type
 * @author Artur Vesker
 */
class AvatarType extends AbstractType
{

    /**
     * @var SonataMediaToUploadedFileTransformer
     */
    protected $transformer;

    /**
     * @var string
     */
    protected $mediaClass;

    /**
     * AvatarType constructor.
     *
     * @param SonataMediaToUploadedFileTransformer $transformer
     * @param $mediaClass
     */
    public function __construct(SonataMediaToUploadedFileTransformer $transformer, $mediaClass)
    {
        $this->transformer = $transformer;
        $this->mediaClass = $mediaClass;
    }

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function($value) {
                if (!$value) {
                    return null;
                }

                if (!$value instanceof UploadImage) {
                    throw new TransformationFailedException('Excepted UploadImage');
                }

                return $value->getImage();
            },
            function($value) {
                return new UploadImage($value, $value);
            }
        ));
    }

    /**
     * @inheritdoc
     */
    public function getParent()
    {
        return TextType::class;
    }

}