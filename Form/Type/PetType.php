<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Pet\Pet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PetType.
 *
 * @author Artur Vesker
 */
class PetType extends AbstractType
{

    protected $genders = [
        'cat' => [
            Pet::GENDER_MALE => 'Кот',
            Pet::GENDER_FEMALE => 'Кошка',
        ],
        'dog' => [
            Pet::GENDER_MALE => 'Кобель',
            Pet::GENDER_FEMALE => 'Сука',
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ages = range(0, 12);

        $builder
            ->add('name', TextType::class)
            ->add('month', ChoiceType::class, [
                'choices' => array_combine($ages, $ages),
                'placeholder' => 'Возраст (мес)'
            ])
            ->add('year', TextType::class)
            ->add('birth_day', PetBirthDayType::class)
            ->add('size', SizeType::class)
            ->add('avatar', AvatarType::class)
            ->add('type', HiddenType::class)
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            if ($pet = $event->getData()) {

                $form = $event->getForm();

                $form
                    ->add('breed', BreedType::class, ['type' => $pet->getType()])
                    ->add('needs', NeedType::class, [
                        'multiple' => true,
                        'expanded' => true,
                        'type' => $pet->getType(),
                    ])
                    ->add('gender', ChoiceType::class, [
                        'choices' => $this->genders[$pet->getType()],
                    ])
                ;
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            $data = $event->getData();

            if (array_key_exists('type', $data)) {
                $form = $event->getForm();

                $type = $data['type'];

                $form
                    ->add('breed', BreedType::class, ['type' => $type])
                    ->add('needs', NeedType::class, [
                        'multiple' => true,
                        'expanded' => true,
                        'type' => $type,
                    ])
                    ->add('gender', ChoiceType::class, [
                        'choices' => $this->genders[$type],
                    ])
                ;
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefault('data_class', 'ADW\AiloveBundle\Model\Pet\Pet')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pet';
    }
}
