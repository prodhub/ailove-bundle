<?php

namespace ADW\AiloveBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PetAgeType.
 *
 * @author Artur Vesker
 */
class PetAgeType extends AbstractType
{

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year', 'text')
            ->add('month', 'text');
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', 'ADW\AiloveBundle\Model\Pet\PetAge');
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'age';
    }
}
