<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Contact;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\Vet;
use ADW\AiloveBundle\RestClient\Method\Upload\UploadImageMethodDescription;
use ADW\AiloveBundle\Security\User\UserDecoratorInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class UserType.
 *
 * @author Artur Vesker
 */
class UserType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('last_name', TextType::class, ['constraints' => [new NotBlank()]])
            ->add('phone', TextType::class, ['constraints' => [new NotBlank()], 'block_name' => 'user_phone'])
            ->add('middle_name', TextType::class, ['required' => false])
            ->add('email', EmailType::class, ['constraints' => [new Email(), new NotBlank()]])
            ->add('birth_day', BirthdayType::class, [
                'years' => range(date('Y') - 90, date('Y') - 13),
                'choice_translation_domain' => [
                    'month' => 'ADWAiloveBundle'
                ]
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    User::GENDER_FEMALE => 'М',
                    User::GENDER_MALE => 'Ж',
                ]
            ])
            ->add('avatar', AvatarType::class)
            ->add('contacts', CollectionType::class, [
                'type' => ContactType::class,
                'allow_add' => true
            ])

            ->add('main_address', ContactType::class, ['validation_groups' => false])
            ->add('delivery_address', ContactType::class, ['validation_groups' => false])

            ->add('password1', PasswordType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Regex(['pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/', 'message' => 'Слабый пароль'])
                ]
            ])
            ->add('password2', PasswordType::class, ['constraints' => [new NotBlank()]])

            ->add('process_data_permission', CheckboxType::class, [
                'constraints' => [
                    new IsTrue(['message' => 'Подтвердите ваше согласие на обработку данных'])
                ]
            ])
            ->add('send_email_permission', CheckboxType::class, ['required' => false])
            ->add('send_sms_permission', CheckboxType::class, ['required' => false])
            ->add('vkontakte_id', HiddenType::class, ['required' => false])
            ->add('facebook_id', HiddenType::class, ['required' => false])
            ->add('google_id', HiddenType::class, ['required' => false])
            ->add('twitter_id', HiddenType::class, ['required' => false])
            ->add('odnoklassniki_id', HiddenType::class, ['required' => false])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();

            if ($data instanceof Vet || ($data instanceof UserDecoratorInterface && $data->getDecorated() instanceof Vet)) {
                $this->addVetFields($event->getForm(), $event->getForm()->getConfig()->getOptions());
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            if (array_key_exists('main_address', $data)) {
                $form->add('main_address', ContactType::class, ['type' => Contact::TYPE_GENERAL]);
            }

            if (array_key_exists('delivery_address', $data)) {
                $form->add('delivery_address', ContactType::class, ['type' => Contact::TYPE_DELIVERY]);
            }
        });
    }

    /**
     * @param FormInterface $form
     * @param array $options
     */
    public function addVetFields(FormInterface $form, array $options)
    {
        $form
            ->add('institution', InstitutionType::class)
            ->add('graduation_year', TextType::class, ['required' => true])
            ->add('clinic', ClinicType::class, ['required' => true]);
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'csrf_protection' => false,
                'allow_extra_fields' => true
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user';
    }
}
