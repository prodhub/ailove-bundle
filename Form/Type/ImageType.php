<?php

namespace ADW\AiloveBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Image;
use ADW\RestClientBundle\Client\Client;

/**
 * Class ImageType.
 *
 * @author Artur Vesker
 */
class ImageType extends AbstractType
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', [
                'choices' => [
                    'pet' => 'pet',
                    'avatar' => 'avatar'
                ]
            ])
            ->add('base64', 'text', ['required' => false])
            ->add('file', 'file', ['constraints' => [new Image(['maxSize' => '30M'])]]);
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {

            $data = $event->getData();
            $binaryContent = $data['media']['binaryContent'];

            if (is_string($binaryContent)) {
                list($type, $value) = explode(';', $binaryContent);
                list(, $value)      = explode(',', $value);
                $value = base64_decode($value);
                list(, $extension) = explode('/', $type);
                $temp = tempnam(sys_get_temp_dir(), 'IMAGE_');
                file_put_contents($temp, $value);
                list(, $type) = explode(':', $type);
                $value = new UploadedFile($temp, uniqid() . '.' . $extension, $type, sizeof($value), null, true);
                $data['media']['binaryContent'] = $value;
                $event->setData($data);
            }

            $event->getForm()->add('media', 'sonata_media_type', [
                'provider' => 'sonata.media.provider.ailove',
                'context'  => $event->getData()['type']
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_image';
    }
}
