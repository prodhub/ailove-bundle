<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Repository\InstitutionRepository;
use ADW\AiloveBundle\Repository\PromoRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PromoType.
 *
 * @author Artur Vesker
 */
class PromoType extends AbstractType
{
    /**
     * @var PromoRepository
     */
    protected $repository;

    /**
     * @param PromoRepository $repository
     */
    public function __construct(PromoRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choice_list' => new ObjectChoiceList($this->repository->findAll(), 'name', [], null, 'systemName'),
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_promo';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
