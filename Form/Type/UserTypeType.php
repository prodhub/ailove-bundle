<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\UserType as AiloveUserType;
use ADW\AiloveBundle\Repository\UserTypeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InstitutionType.
 *
 * @author Artur Vesker
 */
class UserTypeType extends AbstractType
{
    /**
     * @var UserTypeRepository
     */
    protected $userTypeRepository;

    /**
     * @param UserTypeRepository $userTypeRepository
     */
    public function __construct(UserTypeRepository $userTypeRepository)
    {
        $this->userTypeRepository = $userTypeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'choice_list' => new ObjectChoiceList($this->userTypeRepository->findAll(), 'name', [], null, 'systemName'),
                'empty_data' => function (FormInterface $form) {
                    return new AiloveUserType('', $form->getData());
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_user_type';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
