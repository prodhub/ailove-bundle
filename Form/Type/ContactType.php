<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ContactType.
 *
 * @author Artur Vesker
 */
class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', CountryType::class, ['placeholder' => 'Выбрать'])
            ->add('city', TextType::class, ['constraints' => new NotBlank(['groups' => ['delivery']])])
            ->add('zipcode', TextType::class, ['constraints' => new NotBlank(['groups' => ['delivery']])])
            ->add('street', TextType::class, ['constraints' => new NotBlank(['groups' => ['delivery']])])
            ->add('house', TextType::class, ['constraints' => new NotBlank(['groups' => ['delivery']])])
            ->add('apartment')
            ->add('building')
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            $data = $event->getData();

            if ((!$data) || (($data instanceof Contact) && (!$data->getType()))) {
                $event->getForm()->add('type', ChoiceType::class, [
                    'choices' => [
                        Contact::TYPE_GENERAL => 'General',
                        Contact::TYPE_DELIVERY => 'Delivery',
                        Contact::TYPE_BILLING => 'Billing'
                    ],
                    'empty_data' => $event->getForm()->getConfig()->getOption('type')
                ]);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Contact::class);
        $resolver->setDefault('type', null);
        $resolver->setDefault('empty_data', function(FormInterface $form) {
            $class = $form->getConfig()->getOption('data_class');
            return new $class($form->get('type')->getData());
        });
        $resolver->setDefault('validation_groups', function (FormInterface $form) {
            return ['Default', $form->getData()->getType()];
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'contact';
    }
}
