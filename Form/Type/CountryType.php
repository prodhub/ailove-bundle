<?php

namespace ADW\AiloveBundle\Form\Type;

use ADW\AiloveBundle\Model\Country;
use ADW\AiloveBundle\Repository\CountryRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CountryType.
 *
 * @author Artur Vesker
 */
class CountryType extends AbstractType
{
    /**
     * @var CountryRepository
     */
    protected $repository;

    /**
     * @param CountryRepository $repository
     */
    public function __construct(CountryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $choices = array_merge(iterator_to_array($this->repository->findAll()));
        $resolver
            ->setDefaults([
                'choice_list' => new ObjectChoiceList($choices, 'name', [], null, 'systemName'),
                'block_name' => 'user_country'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ailove_country';
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'choice';
    }
}
