<?php

namespace ADW\AiloveBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class PetBirthDateTransformer
 *
 * @package ADW\AiloveBundle\Form\DataTransformer
 * @author Artur Vesker
 */
class PetBirthDateTransformer implements DataTransformerInterface
{

    /**
     * @inheritdoc
     */
    public function transform($value)
    {
        if (null === $value) {
            return '';
        }

        if (!$value instanceof \DateTime && !$value instanceof \DateTimeInterface) {
            throw new TransformationFailedException('Expected a \DateTime or \DateTimeInterface.');
        }

        if (!$value instanceof \DateTimeImmutable) {
            $value = clone $value;
        }

        return $value->format('m/Y');
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        if (empty($value)) {
            return null;
        }

        if (!is_string($value)) {
            throw new TransformationFailedException('Expected a string.');
        }

        try {
            if (!$date = \DateTime::createFromFormat('m/Y', $value)) {
                throw new \InvalidArgumentException('Invalid date format');
            }

        } catch (\Exception $e) {
            throw new TransformationFailedException($e->getMessage(), $e->getCode(), $e);
        }

        return $date;
    }


}