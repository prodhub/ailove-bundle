<?php

namespace ADW\AiloveBundle\Form\DataTransformer;

use ADW\AiloveBundle\Model\UploadImage;
use Sonata\MediaBundle\Entity\MediaManager;
use Sonata\MediaBundle\Model\MediaInterface;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class SonataMediaToUploadedFileTransformer
 *
 * @package ADW\AiloveBundle\Form\DataTransformer
 * @author Artur Vesker
 */
class SonataMediaToUploadedFileTransformer implements DataTransformerInterface
{

    /**
     * @var MediaManager
     */
    protected $mediaManager;

    /**
     * SonataMediaToUploadedFileTransformer constructor.
     *
     * @param MediaManager $mediaManager
     */
    public function __construct(MediaManager $mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }
    
    /**
     * @inheritdoc
     */
    public function transform($value)
    {
        if ($value === null) {
            return null;
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($value)
    {
        if (!$value instanceof MediaInterface) {
            return null;
        }

        return $value;
    }

}