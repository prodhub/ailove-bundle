<?php

namespace ADW\AiloveBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ADW\RestClientBundle\Client\Client;

/**
 * Class FileToApiUploadFileTransformer.
 *
 * @author Artur Vesker
 */
class FileToApiUploadFileTransformer implements DataTransformerInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $type;

    /**
     * @param Client $client
     * @param string $type
     */
    public function __construct(Client $client, $type)
    {
        $this->client = $client;
        $this->type = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if (!$value) {
            return;
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return;
        }

        if ($value instanceof UploadedFile) {
            $value = $value->move(sys_get_temp_dir(), uniqid($this->type).'.'.$value->getClientOriginalExtension());
        }

        if (!$value instanceof File) {
            throw new TransformationFailedException();
        }

        //TODO: implement
        //return $this->client->uploadImage($value, $this->type);
    }
}
