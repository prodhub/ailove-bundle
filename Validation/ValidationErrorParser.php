<?php

namespace ADW\AiloveBundle\Validation;
use JMS\Serializer\Serializer;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Create Violation list from errors array
 *
 * @author Artur Vesker
 */
class ValidationErrorParser
{

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param Serializer $serializer
     * @param null $prefix
     */
    public function __construct(Serializer $serializer, $prefix = null)
    {
        $this->prefix = $prefix;
        $this->serializer = $serializer;
    }


}
