<?php

namespace ADW\AiloveBundle\Validation;

use ADW\AiloveBundle\Exception\Handler\ExceptionHandlerInterface;
use ADW\AiloveBundle\Exception\UserAlreadyExistsException;
use ADW\CommonBundle\Exception\ValidationViolationException;
use GuzzleHttp\Exception\RequestException;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use Metadata\ClassMetadata;
use Metadata\MetadataFactory;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class ValidationErrorHandler.
 *
 * @author Artur Vesker
 */
class ValidationErrorHandler implements ExceptionHandlerInterface
{

    /**
     * @var MetadataFactory
     */
    protected $metadataFactory;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var SerializedNameAnnotationStrategy
     */
    protected $namingStrategy;

    /**
     * @var PropertyAccessor
     */
    protected $propertyAccessor;

    /**
     * @var string[]
     */
    protected $forcedNonFieldMessages = [
        'Введенные данные некорректны и не могут быть сохранены. Пожалуйста, свяжитесь с нами, если эта ошибка повторится.'
    ];

    /**
     * @var string[]
     */
    protected $usersExistsMessages = [
        'Введенные данные некорректны и не могут быть сохранены. Пожалуйста, свяжитесь с нами, если эта ошибка повторится.'
    ];

    /**
     * @param MetadataFactory $metadataFactory
     * @param bool|false $debug
     */
    public function __construct(MetadataFactory $metadataFactory, $debug = false)
    {
        $this->metadataFactory = $metadataFactory;

        if ($debug) {
            $this->prefix = 'Ailove Error: ';
        }

        $this->namingStrategy = new SerializedNameAnnotationStrategy(new CamelCaseNamingStrategy());
    }

    /**
     * @param RequestException $exception
     * @param MethodDescriptionInterface $methodDescription
     * @param array $options
     */
    public function handle(RequestException $exception, MethodDescriptionInterface $methodDescription, array $options)
    {
        if (!$response = $exception->getResponse()) {
            return;
        }

        if ($response->getStatusCode() != 400) {
            return;
        }

        $error = json_decode((string) $response->getBody(), true)['data'];

        if (!isset($error['error_code'])) {
            return;
        }

        if ($error['error_code'] != 'validation_error') {
            return;
        }

        if (in_array($methodDescription->getMethod(), ['POST', 'PUT', 'PATCH', 'DELETE'])) {
            $subject = $methodDescription->getRequestData($options);
        } else {
            $subject = $methodDescription->getQuery($options);
        }

        $message = $error['error_message'];

        unset($error['status_code']);
        unset($error['error_message']);
        unset($error['error_code']);

        throw new ValidationViolationException($this->parse($error, $subject), $message);
    }

    /**
     * @param array $data
     * @param $subject
     * @return ConstraintViolationList
     */
    public function parse(array $data, $subject)
    {
        $violationList = new ConstraintViolationList();

        if (array_key_exists('non_field_errors', $data)) {
            foreach ($data['non_field_errors'] as $message) {
                $violationList->add($this->createViolation(null, $this->prefix . $message, null, null));
            }

            unset($data['non_field_errors']);
        }

        $metadata = null;

        if (is_object($subject)) {
            $metadata = $this->metadataFactory->getMetadataForClass(get_class($subject));
        }

        foreach ($data as $name => $entry) {
            $propertyTypeMetadata = null;
            $fieldSubject = null;

            if ($metadata) {
                if ($property = $this->findProperty($metadata, $name)) {
                    $name = $property->name;
                    $fieldSubject = $this->readProperty($subject, $property->name);
                    $propertyTypeMetadata = $this->getPropertyTypeMetadata($property, $fieldSubject);
                } else {
                    $name = null;
                }
            }

            $this->fillList($violationList, $name, $entry, $fieldSubject, $propertyTypeMetadata);
        }

        return $violationList;
    }

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @param $path
     * @param $data
     * @param $subject
     * @param ClassMetadata $metadata
     */
    protected function fillList(ConstraintViolationListInterface $constraintViolationList, $path, $data, $subject, ClassMetadata $metadata = null)
    {
        if ($this->isAssoc($data)) {
            foreach ($data as $fieldName => $entry) {
                $propertyTypeMetadata = null;
                $fieldSubject = null;

                if ($metadata) {
                    if ($property = $this->findProperty($metadata, $fieldName)) {
                        $fieldName = $property->name;
                        $fieldSubject = $this->readProperty($subject, $property->name);
                        $propertyTypeMetadata = $this->getPropertyTypeMetadata($property, $fieldSubject);
                    } else {
                        $fieldName = null;
                    }

                }

                $this->fillList($constraintViolationList, $this->createPath($path, $fieldName), $entry, $fieldSubject, $propertyTypeMetadata);
            }
            return;
        }


        if (is_array($data)) {
            foreach ($data as $key => $entry) {
                if ($this->isAssoc($entry)) {
                    $subject = $subject[$key];
                    $metadata = null;

                    if (is_object($subject)) {
                        $metadata = $this->metadataFactory->getMetadataForClass(get_class($subject));
                    }
                    $this->fillList($constraintViolationList, $this->createPath($path, $key), $entry, $subject, $metadata);

                    return;
                }
            }

            foreach ($data as $message) {
                $this->fillList($constraintViolationList, $path, $message, $subject, $metadata);
            }

            return;
        }

        if (is_string($data)) {
            $path = in_array($data, $this->forcedNonFieldMessages) ? null : $path;
            $constraintViolationList->add($this->createViolation($path, $this->prefix . $data, $subject, null));

            if (in_array($data, $this->usersExistsMessages)) {
                throw new UserAlreadyExistsException($constraintViolationList);
            }

            return;
        }

        throw new \InvalidArgumentException($data);
    }

    /**
     * @param $path
     * @param $fieldName
     * @return string
     */
    protected function createPath($path, $fieldName)
    {
        if (is_integer($fieldName)) {
            if ($path !== null) {
                return $path . '[' . $fieldName . ']';
            }
        } else {
            if ($path !== null) {
                return $path . '.' . $fieldName;
            }
        }

        return $fieldName;
    }

    /**
     * @param $path
     * @param $message
     * @param $root
     * @param $value
     * @return ConstraintViolation
     */
    protected function createViolation($path, $message, $root, $value)
    {
        return new ConstraintViolation($message, null, [], $root, $path, $value);
    }

    /**
     * @param $arr
     *
     * @return bool
     */
    protected function isAssoc($arr)
    {
        if (!is_array($arr)) {
            return false;
        }

        if (count($arr) == 0) {
            return false;
        }

        return array_keys($arr) !== range(0, count($arr) - 1);
    }


    /**
     * @param ClassMetadata $classMetadata
     * @param $serializedName
     * @return PropertyMetadata|null
     */
    protected function findProperty(ClassMetadata $classMetadata, $serializedName)
    {
        foreach ($classMetadata->propertyMetadata as $property) {
            if ($this->namingStrategy->translateName($property) == $serializedName) {
                return $property;
            }
        }

        return null;
    }

    /**
     * @param PropertyMetadata $propertyMetadata
     * @param $subject
     * @return ClassMetadata|null TODO: consider nested collections
     *
     * TODO: consider nested collections
     */
    protected function getPropertyTypeMetadata(PropertyMetadata $propertyMetadata, $subject)
    {
        $type = $propertyMetadata->type;

        if ($this->isScalarType($type['name'])) {
            return null;
        }

        if (is_array($subject) || $subject instanceof \Traversable) {
            return $this->getCollectionSubjectMetadata($subject, $type);
        }
    }

    protected function getCollectionSubjectMetadata($collection, $type)
    {
        if (isset($type['params'][0])) {

            $type = $type['params'][0]['name'];

            if ($this->isScalarType($type)) {
                return null;
            }

            if (class_exists($type)) {
                return $this->metadataFactory->getMetadataForClass($type);
            }
        }

        return null;
    }

    protected function isScalarType($type)
    {
        return in_array($type, ['string', 'integer', 'boolean', 'float']);
    }

    /**
     * @param $subject
     * @param $field
     * @return mixed
     */
    protected function readProperty($subject, $field)
    {
        if (!$this->propertyAccessor) {
            $this->propertyAccessor = new PropertyAccessor();
        }

        return $this->propertyAccessor->getValue($subject, $field);
    }

}
