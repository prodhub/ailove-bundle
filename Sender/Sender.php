<?php

namespace ADW\AiloveBundle\Sender;

use ADW\AiloveBundle\Model\Mailing\Message;
use ADW\AiloveBundle\Model\Mailing\SMSMessage;
use ADW\AiloveBundle\Model\Message\SuccessMessage;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Auth\RequestResetPasswordMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ResendEmailConfirmationMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ResendPhoneConfirmationMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Mailings\SendEmailMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Mailings\SendSmsMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class Sender.
 *
 * @author Artur Vesker
 */
class Sender
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Client $client
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * Send custom email from Ailove.
     *
     * @param Message $message
     * @param bool    $async
     *
     * @return \ADW\AiloveBundle\Collection\LazyCollection|array|object
     */
    public function sendEmail(Message $message, $async = false)
    {
        if ($async) {
            $result = $this->client->asyncRequest(new SendEmailMethodDescription(), ['message' => $message]);
        } else {
            $result = $this->client->request(new SendEmailMethodDescription(), ['message' => $message]);
        }

        $this->logger->info('Send email for '.$message->getEmail());

        return $result;
    }

    /**
     * Send custom sms from Ailove.
     *
     * @param SMSMessage $message
     * @param bool       $async
     *
     * @return \ADW\AiloveBundle\Collection\LazyCollection|array|object
     */
    public function sendSMS(SMSMessage $message, $async = false)
    {
        $method = new SendSmsMethodDescription();
        $options = ['message' => $message];

        if ($async) {
            $result = $this->client->asyncRequest($method, $options);
        } else {
            $result = $this->client->request($method, $options);
        }

        $this->logger->info('Send sms for '.$message->getPhone());

        return $result;
    }

    /**
     * Resend email confirmation link.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function resendEmailConfirmation(User $user)
    {
        $result = $this->client->request(new ResendEmailConfirmationMethodDescription(), ['uid' => $user->getId()]);

        $this->logger->info('User resend email confimation.');

        return $result;
    }

    /**
     * Resend Phone confirmation sms code.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function resendPhoneConfirmation(User $user)
    {
        $result = $this->client->request(new ResendPhoneConfirmationMethodDescription(), ['uid' => $user->getId()]);

        $this->logger->info('User resend phone confimation.');

        return $result;
    }



    /**
     * Send email with link for restore password.
     *
     * @param $email
     * @param bool $async
     *
     * @return SuccessMessage
     */
    public function sendRestorePasswordMessage($email, $async = false)
    {
        $options = ['email' => $email];
        $method = new RequestResetPasswordMethodDescription();

        if ($async) {
            $result = $this->client->asyncRequest($method, $options);
        } else {
            $result = $this->client->request($method, $options);
        }

        $this->logger->info("Send restore password message {$email}");

        return $result;
    }
}
