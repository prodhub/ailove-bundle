<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Pet\Need;
use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\RestClient\Method\Pet\ListCatNeedsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Pet\ListDogNeedsMethodDescription;

/**
 * Class NeedRepository.
 *
 * @author Artur Vesker
 */
class NeedRepository extends AbstractRepository
{

    /**
     * Get needs by pet type.
     *
     * @param $type
     *
     * @return Need[]
     *
     * @deprecated
     */
    public function findByPetType($type)
    {
        if (!in_array($type, [Pet::TYPE_CAT, Pet::TYPE_DOG])) {
            throw new \InvalidArgumentException('Invalid pet type');
        }

        if ($type == Pet::TYPE_DOG) {
            return $this->client->request(new ListDogNeedsMethodDescription());
        }

        if ($type == Pet::TYPE_CAT) {
            return $this->client->request(new ListCatNeedsMethodDescription());
        }

        return;
    }

    /**
     * @return LazyCollection|Need[]
     */
    public function findForDog()
    {
        return $this->client->request(new ListDogNeedsMethodDescription());
    }

    /**
     * @return LazyCollection|Need[]
     */
    public function findForCat()
    {
        return $this->client->request(new ListCatNeedsMethodDescription());
    }
}
