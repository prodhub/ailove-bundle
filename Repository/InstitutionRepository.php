<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Institution;
use ADW\AiloveBundle\RestClient\Method\Collection\Institution\ListInstitutionsMethodDescription;

/**
 * Class InstitutionRepository.
 *
 * @author Artur Vesker
 */
class InstitutionRepository extends AbstractRepository
{
    /**
     * Get all institutions.
     *
     * @return LazyCollection|Institution[]
     */
    public function findAll()
    {
        return $this->client->request(new ListInstitutionsMethodDescription());
    }
}
