<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\Promo\FelixPromo;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GetPrizeFelixMethodDescription;

/**
 * Class FelixPromoRepository
 *
 * @package ADW\AiloveBundle\Repository
 * @author Anton Prokhorov
 */
class FelixPromoRepository extends AbstractRepository
{

    /**
     * @return LazyCollection|FelixPromo[]
     */
    public function findAll($promo)
    {
        return $this->client->request(new GetPrizeFelixMethodDescription(), [
            'promo' => $promo
        ]);
    }

}