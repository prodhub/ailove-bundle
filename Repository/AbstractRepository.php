<?php

namespace ADW\AiloveBundle\Repository;

use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class AbstractRepository.
 *
 * @author Artur Vesker
 */
abstract class AbstractRepository
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Client $client
     * @param LoggerInterface|null $logger
     */
    public function __construct(Client $client, LoggerInterface $logger = null)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * Set values via reflection.
     *
     * @param $target
     * @param $values
     */
    protected function setValuesViaReflection($target, $values)
    {
        $targetReflection = new \ReflectionObject($target);

        foreach ($values as $key => $value) {
            $property = $targetReflection->getProperty($key);
            $property->setAccessible(true);
            $property->setValue($target, $value);
        }
    }
}
