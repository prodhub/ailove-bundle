<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\UserAction;
use ADW\AiloveBundle\RestClient\Method\User\Action\DoActionMethodDescription;

/**
 * Class ActionRepository
 *
 * @author Artur Vesker
 */
class ActionRepository extends UserRelatedAbstractRepository
{

    /**
     * Save action for user
     *
     * @param $actionName
     * @return UserAction
     */
    public function save($actionName)
    {
        $promo = null;
        $selector = null;

        $result = $this->client->request(new DoActionMethodDescription(), [
            'uid' => $this->user->getId(),
            'action' => $actionName,
            'promo' => $promo,
            'selector' => $selector
        ]);

        if ($this->logger) {
            $this->logger->info("Performed action {$result->getAction()} #{$result->getId()}");
        }

        return $result;
    }

}