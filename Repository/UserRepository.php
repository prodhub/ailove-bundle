<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\User\CreateUserMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\DeleteUserMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\GetUserMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\ListUsersMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\PutUserMethodDescription;

/**
 * Class UserRepository.
 *
 * @author Artur Vesker
 */
class UserRepository extends AbstractRepository
{
    /**
     * Find user by id.
     *
     * @param $id
     *
     * @return User
     */
    public function find($id)
    {
        return $this->client->request(new GetUserMethodDescription(), ['id' => (int) $id]);
    }

    /**
     * Find all users.
     *
     * @return LazyCollection|User[]
     */
    public function findAll()
    {
        return $this->client->request(new ListUsersMethodDescription());
    }

    /**
     * Find users by email or phone or social id.
     *
     * @param string $search
     *
     * @return LazyCollection|User[]
     */
    public function findByEmailOrPhoneOrSocial($search)
    {
        return $this->client->request(new ListUsersMethodDescription(), ['search' => $search]);
    }

    /**
     * Create new user or update existing.
     *
     * @param User $user
     *
     * @return $this
     */
    public function save(User $user)
    {
        if (empty($user->getMiddleName())) {
            $user->setMiddleName(null);
        }

        if (!$id = $user->getId()) {
            $result = $this->client->request(new CreateUserMethodDescription(), ['user' => $user]);
            $this->logger->info('User Created');
        } else {
            $result = $this->client->request(new PutUserMethodDescription(), ['user' => $user, 'id' => $user->getId()]);
            $this->logger->info('User Updated');
        }

        $this->setValuesViaReflection($user, ['id' => $result->getId()]);

        return $this;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->client->request(new DeleteUserMethodDescription(), ['id' => (int)$id]);
    }
}
