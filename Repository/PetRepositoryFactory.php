<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\User;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class PetRepositoryFactory.
 *
 * @author Artur Vesker
 */
class PetRepositoryFactory
{
    /**
     * @var \ADW\RestClientBundle\Client\Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var array
     */
    protected $config;

    /**
     * @param \ADW\RestClientBundle\Client\Client $client
     * @param Logger $logger
     * @param array $config
     */
    public function __construct(Client $client, Logger $logger, array $config)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * Create pet repository for user.
     *
     * @param $user
     *
     * @return \ADW\AiloveBundle\Repository\PetRepository
     */
    public function createForUser($user)
    {
        if ($user instanceof User) {
            //$user = $user->getId();
        }

        if ($user === null) {
            throw new \InvalidArgumentException('User id cannot be null');
        }

        $class = $this->config['repository_class'];

        return new $class($this->client, $user, $this->logger);
    }
}
