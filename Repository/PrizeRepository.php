<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Prize;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Promo\ListPrizesMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GivePrizeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\ListUserPrizesMethodDescription;

/**
 * Class PrizeRepository
 *
 * @package ADW\AiloveBundle\Repository
 * @author Artur Vesker
 */
class PrizeRepository extends AbstractRepository
{

    /**
     * @return LazyCollection|Prize[]
     */
    public function findAll()
    {
        return $this->client->request(new ListPrizesMethodDescription());
    }

    /**
     * @param $systemName
     * @return Prize|mixed|null
     */
    public function find($systemName)
    {
        foreach ($this->findAll() as $prize) {
            if ($prize->getSystemName() == $systemName) {
                return $prize;
            }
        }

        return null;
    }

    /**
     * @param string $systemName
     * @return bool
     */
    public function isAvailable($systemName)
    {
        if (!$prize = $this->find($systemName)) {
            return false;
        }

        return $prize->getAmountLeft() > 0;
    }

    /**
     * @param User $user
     * @return LazyCollection|PrizeWinner[]
     */
    public function findByUser(User $user)
    {
        return $this->client->request(new ListUserPrizesMethodDescription(), ['uid' => $user->getId()]);
    }

    /**
     * @param User $user
     * @param string $promo
     * @return LazyCollection|\ADW\AiloveBundle\Model\PrizeWinner[]
     */
    public function findByUserAndPromo(User $user, $promo)
    {
        $results = [];

        foreach ($this->findByUser($user) as $prizeWinner) {
            if ($prizeWinner->getPrize()->getPromoName() == $promo) {
                $results[] = $prizeWinner;
            }
        }

        return $results;
    }

    /**
     * @param User $user
     * @param $name
     * @return LazyCollection|PrizeWinner[]
     */
    public function findByUserAndName(User $user, $name)
    {
        $results = [];

        foreach ($this->findByUser($user) as $winner) {
            if ($winner->getPrize()->getSystemName() == $name) {
                $results[] = $winner;
            }
        }

        return $results;
    }


    /**
     * @param $promoName
     * @return Prize[]
     */
    public function findByPromo($promoName)
    {
        $results = [];

        foreach ($this->findAll() as $prize) {
            if (($prize->getPromoName() == $promoName)) {
                $results[] = $prize;
            }
        }

        return $results;
    }

    /**
     * @param $promoName
     * @return Prize[]
     */
    public function findAvailableByPromo($promoName)
    {
        $results = [];

        foreach ($this->findAll() as $prize) {
            if (($prize->getPromoName() == $promoName) && ($prize->getAmountLeft() > 0)) {
                $results[] = $prize;
            }
        }

        return $results;
    }

    /**
     * @param User $user
     * @param $prizeName
     * @return PrizeWinner
     */
    public function add(User $user, $prizeName)
    {
        $result = $this->client->request(new GivePrizeMethodDescription(), [
            'uid' => $user->getId(),
            'prize' => $prizeName,
        ]);

        $this->logger->info("Add prize \"{$prizeName}\" #{$result->getId()}");

        return $result;
    }

}