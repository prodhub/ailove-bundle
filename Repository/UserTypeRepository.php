<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\UserType;
use ADW\AiloveBundle\RestClient\Method\User\ListUserTypesMethodDescription;

/**
 * Class UserTypeRepository.
 *
 * @author Artur Vesker
 */
class UserTypeRepository extends AbstractRepository
{
    /**
     * @return LazyCollection|UserType[]
     */
    public function findAll()
    {
        return $this->client->request(new ListUserTypesMethodDescription());
    }
}
