<?php

namespace ADW\AiloveBundle\Repository\Factory;

use ADW\AiloveBundle\Model\User;

/**
 * Class InvitationRepositoryFactory
 *
 * @package ADW\AiloveBundle\Repository\Factory
 * @author Artur Vesker
 */
class InvitationRepositoryFactory extends AbstractUserRelatedRepositoryFactory
{

    /**
     * @inheritdoc
     */
    public function getClass()
    {
        return 'ADW\AiloveBundle\Repository\InvitationRepository';
    }

    /**
     * @inheritdoc
     * @return \ADW\AiloveBundle\Repository\InvitationRepository
     */
    public function create(User $user)
    {
        return parent::create($user);
    }

}