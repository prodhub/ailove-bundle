<?php

namespace ADW\AiloveBundle\Repository\Factory;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\UserRelatedAbstractRepository;
use Psr\Log\LoggerInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class AbstractUserRelatedRepositoryFactory
 *
 * @package ADW\AiloveBundle\Repository\Factory
 * @author Artur Vesker
 */
abstract class AbstractUserRelatedRepositoryFactory
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AbstractUserRelatedRepositoryFactory constructor.
     *
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    abstract public function getClass();

    /**
     * @param User $user
     * @return UserRelatedAbstractRepository
     */
    public function create(User $user)
    {
        $class = $this->getClass();

        return new $class($this->client, $user, $this->logger);
    }

}
