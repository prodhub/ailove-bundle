<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\User\Invitation\InvitationCode;
use ADW\AiloveBundle\RestClient\Method\User\Invitation\GenerateInvitationCodeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Invitation\ListInvitationCodesMethodDescription;

/**
 * Class InvitationRepository
 *
 * @package ADW\AiloveBundle\Repository
 * @author Artur Vesker
 */
class InvitationRepository extends UserRelatedAbstractRepository
{

    /**
     * @inheritdoc
     */
    public function findAll()
    {
        return $this->client->request(new ListInvitationCodesMethodDescription(), ['uid' => $this->user->getId()]);
    }

    /**
     * @param $program
     * @param string|null $promo
     * @param string|null $selector
     * @return InvitationCode
     */
    public function create($program, $promo = null, $selector = null)
    {
        return $this->client->request(new GenerateInvitationCodeMethodDescription(), [
                'program' => $program,
                'uid' => $this->getUser()->getId(),
                'additional' => [
                    'promo' => $promo,
                    'selector' => $selector
                ]
            ]
        );
    }

}