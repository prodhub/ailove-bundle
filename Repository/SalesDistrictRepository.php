<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\SalesDistrict;
use ADW\AiloveBundle\RestClient\Method\Collection\SalesDistrict\ListSalesDistrictsMethodDescription;

/**
 * Class SalesDistrictRepository.
 *
 * @author Artur Vesker
 */
class SalesDistrictRepository extends AbstractRepository
{
    /**
     * Get all available sales districts.
     *
     * @return LazyCollection|SalesDistrict[]
     */
    public function findAll()
    {
        return $this->client->request(new ListSalesDistrictsMethodDescription());
    }

    /**
     * Get sales district for region.
     *
     * @param $region
     *
     * @return LazyCollection|SalesDistrict[]
     */
    public function findByRegion($region)
    {
        return $this->client->request(new ListSalesDistrictsMethodDescription(), ['search' => $region]);
    }
}
