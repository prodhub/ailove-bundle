<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\UserPromo;
use ADW\AiloveBundle\RestClient\Method\Promo\ListPromoMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\UserOrdersReceiptsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\ListUserPromoMethodDescription;

/**
 * Class PromoRepository.
 *
 * @author Artur Vesker
 */
class PromoRepository extends AbstractRepository
{

    /**
     * @return LazyCollection
     */
    public function findAll()
    {
        return $this->client->request(new ListPromoMethodDescription());
    }

    /**
     * @param User $user
     * @return LazyCollection|UserPromo[]
     */
    public function findByUser(User $user)
    {
        return $this->client->request(new ListUserPromoMethodDescription(), ['uid' => (int) $user->getId()]);
    }

    /**
     * @param User $user
     * @param string $name
     * @return UserPromo|null
     */
    public function findOneByUserAndName(User $user, $name)
    {
        foreach ($this->findByUser($user) as $promo) {
            if ($promo->getPromo() == $name) {
                return $promo;
            }
        }

        return null;
    }

    /**
     * @param User $user
     * @param string[] $names
     * @return UserPromo[]|LazyCollection
     */
    public function findByUserAndNames(User $user, $names)
    {
        $selected = [];

        foreach ($this->findByUser($user) as $promo) {
            if (in_array($promo->getPromo(), $names)) {
                $selected[] = $promo;
            }
        }

        return $selected;
    }

    /**
     * @param $name
     * @return bool
     */
    public function isActive($name) {

        $isActive = false;

        foreach ($this->findAll() as $promo) {
                if (strtolower($promo->getSystemName()) == strtolower($name)) {
                $isActive = true;
            }
        }

        return $isActive;

    }


    /**
     * @param User $user
     * @param $receipt
     */
    public function receiptsave(User $user, $receipt)
    {
        $result = $this->client->request(new UserOrdersReceiptsMethodDescription(), [
            'uid' => $user->getId(),
            'receipt' => $receipt,
        ]);
        return $result;
    }

}
