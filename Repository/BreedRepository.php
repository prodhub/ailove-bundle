<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Pet\Breed;
use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\RestClient\Method\Pet\ListCatBreedsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Pet\ListDogBreedsMethodDescription;

/**
 * Class BreedRepository.
 *
 * @author Artur Vesker
 */
class BreedRepository extends AbstractRepository
{
    /**
     * Get breeds by pet type.
     *
     * @param $type
     *
     * @return Breed[]
     */
    public function findByPetType($type)
    {
        if (!in_array($type, [Pet::TYPE_CAT, Pet::TYPE_DOG])) {
            throw new \InvalidArgumentException('Invalid pet type');
        }

        if ($type == Pet::TYPE_DOG) {
            return $this->client->request(new ListDogBreedsMethodDescription());
        }

        if ($type == Pet::TYPE_CAT) {
            return $this->client->request(new ListCatBreedsMethodDescription());
        }

        return;
    }

    /**
     * @return Breed[]
     */
    public function findForDog()
    {
        return $this->sortByNames($this->client->request(new ListDogBreedsMethodDescription()));
    }

    /**
     * @return Breed[]
     */
    public function findForCat()
    {
        return $this->sortByNames($this->client->request(new ListCatBreedsMethodDescription()));
    }

    /**
     * @param Breed[] $values
     * @return Breed[]
     */
    protected function sortByNames($values)
    {
        if (is_object($values) && $values instanceof \Traversable) {
            $values = iterator_to_array($values);
        }

        usort($values, function(Breed $a, Breed $b) {
            return strcmp($a->getName(), $b->getName());
        });
        return $values;
    }
}
