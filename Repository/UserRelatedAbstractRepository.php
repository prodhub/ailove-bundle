<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\User;
use Psr\Log\LoggerInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class UserRelatedAbstractRepository
 *
 * @author Artur Vesker
 */
abstract class UserRelatedAbstractRepository extends AbstractRepository
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @param Client $client
     * @param LoggerInterface $logger
     * @param User $user
     */
    public function __construct(Client $client, User $user, LoggerInterface $logger = null)
    {
        parent::__construct($client, $logger);
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}
