<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\RestClient\Method\User\Action\GetUserPermissionsMethodDescription;

/**
 * Class PermissionRepository
 *
 * @author Artur Vesker
 */
class PermissionRepository extends UserRelatedAbstractRepository
{

    /**
     * @return string[]
     */
    public function findAll()
    {
        return $this->client->request(new GetUserPermissionsMethodDescription(), ['uid' => (int) $this->user->getId()]);
    }

    /**
     * @param $permission
     * @return bool
     */
    public function has($permission)
    {
        $permissions = $this->findAll();

        return in_array($permission, $permissions);
    }

}