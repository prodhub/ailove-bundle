<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Clinic;
use ADW\AiloveBundle\RestClient\Method\Collection\Clinic\ListClinicsMethodDescription;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ClinicRepository.
 *
 * @author Artur Vesker
 */
class ClinicRepository extends AbstractRepository
{

    /**
     * @return LazyCollection|Clinic[]
     */
    public function findAll()
    {
        return $this->client->request(new ListClinicsMethodDescription());
    }

    /**
     * @param $city
     *
     * @return Clinic[]|ArrayCollection
     */
    public function findByCity($city)
    {
        $clinics = $this->findAll();

        if (!$city) {
            return $clinics;
        }

        $filtered = [];

        foreach ($clinics as $clinic) {
            if ((mb_strtolower($clinic->getCity()) == mb_strtolower($city)) || empty($clinic->getCity())) {
                $filtered[] = $clinic;
            }
        }

        return new ArrayCollection($filtered);
    }
}
