<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Pet\Pet;
use ADW\AiloveBundle\RestClient\Method\User\Pet\CreatePetMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Pet\DeletePetMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Pet\GetPetMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Pet\ListUserPetsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Pet\PutPetMethodDescription;
use Traversable;

/**
 * Class PetRepository.
 *
 * @author Artur Vesker
 */
class PetRepository extends UserRelatedAbstractRepository implements \IteratorAggregate
{

    /**
     * @var Pet[]
     */
    protected $cached;

    /**
     * Get all user pets.
     *
     * @return LazyCollection|Pet[]
     */
    public function findAll()
    {
        if (!$this->cached) {
            return $this->cached = $this->client->request(new ListUserPetsMethodDescription(), ['uid' => (int)$this->user->getId()]);
        }

        return $this->cached;
    }

    /**
     * @param string $type
     * @return Pet[]
     */
    public function findByType($type = null)
    {
        if (!$type) {
            return $this->findAll();
        }

        $filtered = [];

        foreach ($this->findAll() as $pet) {
            if ($pet->getType() === $type) {
                $filtered[] = $pet;
            }
        }

        return $filtered;
    }

    /**
     * Get pet by id.
     *
     * @param $id
     *
     * @return Pet|null
     */
    public function find($id)
    {
        return $this->client->request(new GetPetMethodDescription(), ['uid' => (int)$this->user->getId(), 'id' => (int)$id]);
    }

    /**
     * Save or update pet.
     *
     * @param Pet $pet
     *
     * @return $this
     */
    public function save(Pet $pet)
    {
        if (!$id = $pet->getId()) {
            $result = $this->client->request(new CreatePetMethodDescription(), ['uid' => (int)$this->user->getId(), 'pet' => $pet]);
            $this->logger->info('Pet created', ['id' => $result->getId()]);
        } else {
            $result = $this->client->request(new PutPetMethodDescription(), ['uid' => (int)$this->user->getId(), 'id' => (int)$id, 'pet' => $pet]);
            $this->logger->info('Pet updated', ['id' => $result->getId()]);
        }

        $this->setValuesViaReflection($pet, ['id' => $result->getId()]);

        return $this;
    }

    /**
     * @param Pet|int $pet
     *
     * @return $this
     */
    public function remove($pet)
    {
        if ($pet instanceof Pet) {
            $pet = $pet->getId();
        }

        $this->client->request(new DeletePetMethodDescription(), ['uid' => (int)$this->user->getId(), 'id' => (int) $pet]);

        return $this;
    }

    /**
     * @param $selector
     * @return Pet|mixed|null
     */
    public function findByAdditionalData($selector)
    {
        $results = [];

        foreach ($this->findAll() as $pet) {
            foreach ($pet->getAdditionalData() as $data) {
                foreach ($selector as $key => $value) {
                    if ($data->getField()->getSystemName() === $key) {
                        foreach ($data->getValues() as $dataValue) {
                            if ($value == $dataValue->getSystemName()) {
                                $results[] = $pet;
                            }
                        }
                    }
                }
            }
        }

        return $results;
    }

    /**
     * @param $discriminator
     * @return Pet|mixed|null
     */
    public function findByDiscriminator($discriminator)
    {
        return $this->findByAdditionalData(['discriminator' => $discriminator]);
    }

    /**
     * @param $discriminator
     * @return Pet|mixed|null
     */
    public function findOneByDiscriminator($discriminator)
    {
        $results = $this->findByAdditionalData(['discriminator' => $discriminator]);

        if (count($results) < 1) {
            return null;
        }

        return $results[0];
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return $this->findAll();
    }

}
