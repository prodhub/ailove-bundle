<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Item;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Promo\ListItemMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\UserOrdersMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GetUserOrdersMethodDescription;

/**
 * Class ItemRepository
 *
 * @package ADW\AiloveBundle\Repository
 * @author Anton Prokhorov
 */
class ItemRepository extends AbstractRepository
{

    /**
     * @return LazyCollection|Item[]
     */
    public function findAll()
    {
        return $this->client->request(new ListItemMethodDescription());
    }

    /**
     * @param $systemName
     * @return Item|mixed|null
     */
    public function find($systemName)
    {
        foreach ($this->findAll() as $item) {
            if ($item->getSystemName() == $systemName) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param string $systemName
     * @return bool
     */
    public function isAvailable($systemName)
    {
        if (!$item = $this->find($systemName)) {
            return false;
        }

        return $item->getAmountLeft() > 0;
    }

    /**
     * @param User $user
     * @param $prizeName
     */
    public function add(User $user, $order)
    {
        $result = $this->client->request(new UserOrdersMethodDescription(), [
            'uid' => $user->getId(),
            'order' => $order,
        ]);

//        $this->logger->info("Add prize \"{$prizeName}\" #{$result->getId()}");

        return $result;
    }


    /**
     * @param User $user
     */

    public function get(User $user)
    {
        $result = $this->client->request(new GetUserOrdersMethodDescription(), [
            'uid' => $user->getId()
        ]);

        return $result;
    }


}