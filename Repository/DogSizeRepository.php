<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Pet\Size;
use ADW\AiloveBundle\RestClient\Method\Pet\ListDogSizesMethodDescription;

/**
 * Class DogSizeRepository.
 *
 * @author Artur Vesker
 */
class DogSizeRepository extends AbstractRepository
{
    /**
     * @return LazyCollection|Size[]
     */
    public function findAll()
    {
        return $this->client->request(new ListDogSizesMethodDescription());
    }
}
