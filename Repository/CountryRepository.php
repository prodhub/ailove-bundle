<?php

namespace ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\Country;
use ADW\AiloveBundle\RestClient\Method\Common\ListCountriesMethodDescription;

/**
 * Class CountryRepository.
 *
 * @author Artur Vesker
 */
class CountryRepository extends AbstractRepository
{
    /**
     * @return LazyCollection|Country[]
     */
    public function findAll()
    {
        return $this->client->request(new ListCountriesMethodDescription());
    }
}
