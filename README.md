# Что такое? #
Решение для интеграции с CRM Ailove.

Выполняет роль firewall при работе с ailove

Работа с ailove осуществляется по работы с api пример 

https://<crm_address>/api/user/types/
https://<crm_address>/api/auth/login/
https://<crm_address>/api/auth/me/

Поддержка методов 
GET
POST
PUT
DELETE
Опционально для каждого а api метода

Формат принимаемых данных json, формат отдаваемых данных json

В  папке RestClient реализуем метод(MethodDescription) для работы api

getOptions — Передаваемые данные(исходящие данные) на api
getQuery — Для передачи исходящих данных в качестве query строки
getRequestData - Для передачи исходящих данных в качестве POST запроса
getRequestDataFormat — формат передачи(исходящие данные) по умолчанию json
getRequestDataContext — группа для сериализации и 
getMethod каким методов передать (GET, POST, PUT, DELETE )
getResponseDataFormat — формат передачи(входящие данные) по умолчанию json
getResponseDataModel  — входящая модель данных

Модели входящих и исходящих данных хранятся в папке Model

# Установка #
В composer.json:

```
#!json
"repositories": [
   { "type": "vcs", "url": "git@bitbucket.org:prodhub/ailove-bundle.git" },
   { "type": "vcs", "url": "git@bitbucket.org:prodhub/rest-client-bundle.git" },
   { "type": "vcs", "url": "git@bitbucket.org:prodhub/guzzle-bundle.git" }
]
```
Выполнить:

```
#!bash
composer require adw/ailove-bundle dev-master

```

AppKernel.php:

```
#!php
$bundles = [
   new ADW\AiloveBundle\ADWAiloveBundle(),
   new ADW\RestClientBundle\ADWRestClientBundle(),
   new ADW\GuzzleBundle\ADWGuzzleBundle(),
];

```
