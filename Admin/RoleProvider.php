<?php

namespace ADW\AiloveBundle\Admin;

use Vesax\AdminExtraBundle\RoleProvider\RoleProviderInterface;

/**
 * Class RoleProvider
 *
 * @package ADW\AiloveBundle\Admin
 * @author Artur Vesker
 */
class RoleProvider implements RoleProviderInterface
{

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return [
            'ROLE_AILOVE_USER_SHOW' => 'Просмотр пользователей Ailove'
        ];
    }

}