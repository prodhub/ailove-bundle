<?php

namespace ADW\AiloveBundle\Admin\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

/**
 * Class MenuBuilderListener.
 *
 * @author Artur Vesker
 */
class MenuBuilderListener
{
    /**
     * @param ConfigureMenuEvent $event
     */
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $child = $menu->addChild('Ailove', [
            'extras' => [
                'icon' => '<i class="fa fa-plug"></i>',
                'roles' => ['ROLE_AILOVE_USER_SHOW'],
            ],
        ]);

        $child->addChild('Потребители', [
            'route' => 'ailove.admin.user.list',
        ]);

        $child->addChild('Победители', [
            'route' => 'ailove.admin.winner.list',
        ]);

        $child->addChild('Рейтинг', [
            'route' => 'ailove.admin.rating',
        ]);
    }
}
