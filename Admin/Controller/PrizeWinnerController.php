<?php

namespace ADW\AiloveBundle\Admin\Controller;

use ADW\AiloveBundle\RestClient\Method\Promo\ListPrizesMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Promo\ListPromoMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Promo\ListWinnersMethodDescription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PrizeController
 *
 * @package ADW\AiloveBundle\Admin\Controller
 * @author Artur Vesker
 *
 * @Route("/ailove/winner")
 */
class PrizeWinnerController extends Controller
{

    /**
     * @Route("/", name="ailove.admin.winner.list")
     * @Template("ADWAiloveBundle:Admin/Winner:list.html.twig")
     */
    public function listAction(Request $request)
    {
        $winners = $this->get('ailove_client')->request(new ListWinnersMethodDescription(), [
            'promo_name' => $request->query->get('promo'),
            'prize_name' => $request->query->get('prize'),
            'won_lte' => $request->query->get('won_lte'),
            'won_gte' => $request->query->get('won_gte')
        ]);

        $pagination = $this->get('knp_paginator')->paginate($winners, $request->query->get('page', 1), 50);

        $promos = $this->get('ailove.promo_repository')->findAll();
        $prizes = $this->get('ailove_client')->request(new ListPrizesMethodDescription());

        return [
            'base_template' => $this->getParameter('sonata.admin.configuration.templates')['layout'],
            'admin_pool' => $this->get('sonata.admin.pool'),
            'pagination' => $pagination,
            'promos' => $promos,
            'prizes' => $prizes
        ];
    }

}