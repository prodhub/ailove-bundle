<?php

namespace ADW\AiloveBundle\Admin\Controller;

use ADW\AiloveBundle\Admin\Form\ReplaceSelectorType;
use ADW\CommonBundle\Controller\Controller;
use ADW\CommonBundle\Exception\InvalidFormException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AiloveController.
 *
 * @author Artur Vesker
 *
 * @Route("/ailove")
 */
class AiloveController extends Controller
{
    /**
     * @Route("/", name="ailove.admin.dashboard")
     * @Template("ADWAiloveBundle:Admin/Ailove:dashboard.html.twig")
     */
    public function dashboardAction()
    {
        return [

        ];
    }

    /**
     * @Route("/utils/replace-rating-selector/", name="ailove.admin.utils.replace_rating_selector")
     */
    public function replaceRatingSelectorAction(Request $request)
    {
        $form = $this->createPostForm(new ReplaceSelectorType());

        if (!$form->handleRequest($request)->isValid()) {
            throw new InvalidFormException($form);
        }

        $data = $form->getData();
        $user = $this->get('ailove.user_repository')->find($data['user']);

        return new JsonResponse($this->get('ailove.utils')->replaceSelector($user, $data['promo']->getSystemName(), $data['old_selector'], $data['new_selector']));
    }
}
