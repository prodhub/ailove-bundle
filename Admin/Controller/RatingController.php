<?php

namespace ADW\AiloveBundle\Admin\Controller;

use ADW\AiloveBundle\RestClient\Method\Promo\ListPrizesMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Promo\ListPromoMethodDescription;
use GuzzleHttp\Exception\ClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RatingController
 *
 * @package ADW\AiloveBundle\Admin\Controller
 * @author Artur Vesker
 *
 * @Route("/ailove/rating")
 */
class RatingController extends Controller
{

    /**
     * @Route("/", name="ailove.admin.rating")
     * @Method("GET")
     * @Template("ADWAiloveBundle:Admin/Rating:list.html.twig")
     * @Security("has_role('ROLE_AILOVE_USER_SHOW')")
     */
    public function listAction(Request $request)
    {
        $users = [];

        try {
            $users = $this->get('ailove.point_system')->getRating($request->get('promo'), $request->get('selector'));
        } catch (ClientException $e) {
            foreach (json_decode($e->getResponse()->getBody(), true)['data'] as $key => $error) {
                $this->addFlash('error', $key . ': ' . implode(';', $error));
            }
        }

        $pagination = $this->get('knp_paginator')->paginate($users, $request->query->get('page', 1), 50);

        $promos = $this->get('ailove.promo_repository')->findAll();
        $prizes = $this->get('ailove_client')->request(new ListPrizesMethodDescription());

        return [
            'base_template' => $this->getParameter('sonata.admin.configuration.templates')['layout'],
            'admin_pool' => $this->get('sonata.admin.pool'),
            'pagination' => $pagination,
            'promos' => $promos,
            'prizes' => $prizes
        ];
    }

    /**
     * @Route("/{id}/", name="ailove.admin.rating.user_stats")
     * @Method("GET")
     */
    public function getUserStatsAction(Request $request, $id)
    {
        $user = $this->get('ailove.user_repository')->find($id);

        try  {
            $stats = $this->get('ailove.point_system')->getStats($user, $request->get('promo'), $request->get('selector'));
        } catch (ClientException $e) {
            $errors = [];

            foreach (json_decode($e->getResponse()->getBody(), true)['data'] as $key => $error) {
                $errors[$key] = implode(';', $error);
            }

            return new JsonResponse(['errors' => $errors], 400);
        }

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'balance' => $stats->getBalance(),
                'rating_position' => $stats->getRatingPosition()
            ]);
        }
    }
}