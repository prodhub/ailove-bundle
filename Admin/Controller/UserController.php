<?php

namespace ADW\AiloveBundle\Admin\Controller;

use ADW\AiloveBundle\Admin\Form\ReplaceSelectorType;
use ADW\AiloveBundle\AiloveUtils;
use ADW\CommonBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController.
 *
 * @author Artur Vesker
 *
 * @Route("/ailove/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="ailove.admin.user.list")
     * @Method("GET")
     * @Template("ADWAiloveBundle:Admin/User:list.html.twig")
     * @Security("has_role('ROLE_AILOVE_USER_SHOW')")
     */
    public function listAction(Request $request)
    {
        $users = $this->get('ailove.user_repository')->findByEmailOrPhoneOrSocial($request->query->get('search'));

        $pagination = $this->get('knp_paginator')->paginate($users, $request->query->get('page', 1), 50);

        return [
            'base_template' => $this->getParameter('sonata.admin.configuration.templates')['layout'],
            'admin_pool' => $this->get('sonata.admin.pool'),
            'usersPagination' => $pagination,
        ];
    }

    /**
     * @Route("/{id}", name="adw.ailove.user.show")
     * @Method("GET")
     * @Template("ADWAiloveBundle:Admin/User:show.html.twig")
     */
    public function showAction($id)
    {
        $user = $this->get('ailove.user_repository')->find($id);
        $pets = $this->get('ailove.pet_repository_factory')->createForUser($user)->findAll();
        $promos = $this->get('ailove.promo_repository')->findAll();
        $userPromos = $this->get('ailove.promo_repository')->findByUser($user);
        $userPrizes = $this->get('ailove.prize_repository')->findByUser($user);

        $replaceSelectorForm = $this->createPostForm(new ReplaceSelectorType(), ['user' => $id])
            ->add('user', 'hidden');

        $userInvitationCodes = $this->get('ailove.invitation_repository_factory')->create($user)->findAll();

        return [
            'base_template' => $this->getParameter('sonata.admin.configuration.templates')['layout'],
            'admin_pool' => $this->get('sonata.admin.pool'),
            'user' => $user,
            'pets' => $pets,
            'promos' => $promos,
            'sales_district' => AiloveUtils::getSalesDistrictForUser(
                $this->get('ailove.sales_district_repository'),
                $this->get('ailove.fias_repository'),
                $user
            ),
            'userPromos' => $userPromos,
            'userPrizes' => $userPrizes,
            'replaceSelectorForm' => $replaceSelectorForm->createView(),
            'userInvitationCodes' => $userInvitationCodes
        ];
    }


}
