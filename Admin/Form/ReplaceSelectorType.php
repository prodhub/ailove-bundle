<?php

namespace ADW\AiloveBundle\Admin\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ReplaceSelectorType
 *
 * @author Artur Vesker
 */
class ReplaceSelectorType extends AbstractType
{

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'number')
            ->add('promo', 'ailove_promo', ['required' => true])
            ->add('old_selector', 'text')
            ->add('new_selector', 'text')
        ;
    }

}