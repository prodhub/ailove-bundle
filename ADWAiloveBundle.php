<?php

namespace ADW\AiloveBundle;

use ADW\AiloveBundle\DependencyInjection\ContainerBuilder\PrizeLimitationStrategyCompilerPass;
use ADW\AiloveBundle\DependencyInjection\Security\ContactPasswordFactory;
use ADW\AiloveBundle\DependencyInjection\Security\CookieTokenRememberFactory;
use ADW\AiloveBundle\DependencyInjection\Security\QueryTokenFactory;
use ADW\AiloveBundle\DependencyInjection\Security\SocialFactory;
use ADW\AiloveBundle\DependencyInjection\Security\HeaderTokenFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class ADWAiloveBundle.
 *
 * @author Artur Vesker
 */
class ADWAiloveBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $extension = $container->getExtension('security');
        $container->addCompilerPass(new PrizeLimitationStrategyCompilerPass());
        $extension->addSecurityListenerFactory(new ContactPasswordFactory());
        $extension->addSecurityListenerFactory(new SocialFactory());
        $extension->addSecurityListenerFactory(new HeaderTokenFactory());
        $extension->addSecurityListenerFactory(new CookieTokenRememberFactory());
        $extension->addSecurityListenerFactory(new QueryTokenFactory());
    }
}
