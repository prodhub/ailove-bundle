<?php

namespace ADW\AiloveBundle\Media\Provider;

use ADW\AiloveBundle\Model\UploadImage;
use ADW\AiloveBundle\RestClient\Method\Upload\UploadImageMethodDescription;
use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Imagine\Image\ImagineInterface;
use Sonata\MediaBundle\CDN\CDNInterface;
use Sonata\MediaBundle\Generator\GeneratorInterface;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\MediaBundle\Model\MediaInterface;
use Sonata\MediaBundle\Provider\BaseProvider;
use Sonata\MediaBundle\Provider\FileProvider;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Provider\ImageProvider;
use Sonata\MediaBundle\Thumbnail\ThumbnailInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ADW\RestClientBundle\Client\Client;

/**
 * Class AiloveMediaProvider
 *
 * @package ADW\AiloveBundle\Media\Provider
 * @author Artur Vesker
 */
class AiloveMediaProvider extends ImageProvider
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * AiloveMediaProvider constructor.
     *
     * @param string $name
     * @param Filesystem $filesystem
     * @param CDNInterface $cdn
     * @param GeneratorInterface $pathGenerator
     * @param ThumbnailInterface $thumbnail
     * @param array $allowedExtensions
     * @param array $allowedMimeTypes
     * @param ImagineInterface $adapter
     * @param MetadataBuilderInterface $metadata
     * @param Client $client
     */
    public function __construct(
        $name,
        Filesystem $filesystem,
        CDNInterface $cdn,
        GeneratorInterface $pathGenerator,
        ThumbnailInterface $thumbnail,
        array $allowedExtensions,
        array $allowedMimeTypes,
        ImagineInterface $adapter,
        MetadataBuilderInterface $metadata,
        Client $client
    ) {
        parent::__construct(
            $name,
            $filesystem,
            $cdn,
            $pathGenerator,
            $thumbnail,
            $allowedExtensions,
            $allowedMimeTypes,
            $adapter,
            $metadata
        );

        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function prePersist(MediaInterface $media)
    {
        parent::prePersist($media);
        $this->uploadToAilove($media);
    }

    /**
     * @inheritdoc
     */
    public function preUpdate(MediaInterface $media)
    {
        parent::preUpdate($media);
        $this->uploadToAilove($media);
    }

    /**
     * @param MediaInterface $media
     * @return UploadImage
     */
    public function uploadToAilove(MediaInterface $media)
    {
        $tmpPath = sprintf("%s/%s.%s",sys_get_temp_dir(), uniqid('ailove_image'), $media->getExtension());

        $file = $media->getBinaryContent();

        if ($file instanceof File) {
            $file = file_get_contents($file->getRealPath());
        }

        file_put_contents($tmpPath, $file);

        $result = $this->client->request(new UploadImageMethodDescription(), [
            'upload_type' => $media->getContext(),
            'image' => new File($tmpPath)
        ]);

        $media->setMetadataValue('ailove_image', $result->getImage());
        $media->setMetadataValue('ailove_image_url', $result->getImageUrl());

        @unlink($tmpPath); //pohui

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function generatePublicUrl(MediaInterface $media, $format)
    {
        return $media->getMetadataValue('ailove_image_url');
    }

    /**
     * @inheritdoc
     */
    public function generatePrivateUrl(MediaInterface $media, $format)
    {
        return $media->getMetadataValue('ailove_image');
    }


}
