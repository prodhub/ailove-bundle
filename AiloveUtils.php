<?php

namespace ADW\AiloveBundle;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\SalesDistrictRepository;
use ADW\AiloveFiasBundle\Repository\FIASRepository;

/**
 * Class AiloveUtils.
 *
 * @author Artur Vesker
 * @deprecated
 */
class AiloveUtils
{
    const DEFAULT_CITY = 'Москва';
    const DEFAULT_REGION = '0c5b2444-70a0-4932-980c-b4dc0d3f02b5';
    const DEFAULT_DISTRICT = 'center';

    public static function getSalesDistrictForUser(SalesDistrictRepository $salesDistrictRepository, FIASRepository $FIASObjectsRepository, User $user)
    {
        $contacts = $user->getContacts();

        if ($contacts = $contacts->last()) {
            if (!$city = $contacts->getCity()) {
                return self::DEFAULT_DISTRICT;
            }
        } else {
            return self::DEFAULT_DISTRICT;
        }

        $addresses = $FIASObjectsRepository->searchByCity($city);

        if (count($addresses) == 0) {
            return self::DEFAULT_DISTRICT;
        }

        $region = $addresses[0]->getRegion();

        $districts = $salesDistrictRepository->findByRegion($region->getId());

        if (count($districts) == 0) {
            return self::DEFAULT_DISTRICT;
        }

        return $districts[0]->getSystemName();
    }
}
