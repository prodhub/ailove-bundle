<?php

namespace ADW\AiloveBundle\Utils;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Tmp\ReplaceTransactionMethodDescription;
use ADW\RestClientBundle\Client\Client;

/**
 * Class AiloveUtils
 *
 * @author Artur Vesker
 */
class AiloveUtils
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * AiloveUtils constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param User $user
     * @param $promo
     * @param $oldSelector
     * @param $newSelector
     * @internal
     * @return mixed
     */
    public function replaceSelector(User $user, $promo, $oldSelector, $newSelector)
    {
        return $this->client->request(new ReplaceTransactionMethodDescription(), [
            'customer' => $user->getId(),
            'promo' => $promo,
            'old_selector' => $oldSelector,
            'new_selector' => $newSelector
        ]);
    }

}
