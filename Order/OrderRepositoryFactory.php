<?php

namespace ADW\AiloveBundle\Order;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use ADW\AiloveBundle\Repository\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class OrderRepositoryFactory
 *
 * @package ADW\AiloveBundle\Order
 * @author Artur Vesker
 */
class OrderRepositoryFactory
{

    /**
     * @var PrizeRepository
     */
    protected $prizeRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * OrderRepositoryFactory constructor.
     *
     * @param Client $client
     * @param LoggerInterface $logger
     * @param PrizeRepository $prizeRepository
     * @param UserRepository $userRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Client $client, LoggerInterface $logger, PrizeRepository $prizeRepository, UserRepository $userRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->userRepository = $userRepository;
        $this->prizeRepository = $prizeRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
        $this->client = $client;
    }

    /**
     * @param User $user
     * @return OrderRepository
     */
    public function create(User $user)
    {
        return new OrderRepository($this->client, $user, $this->logger, $this->prizeRepository, $this->userRepository, $this->eventDispatcher);
    }

}
