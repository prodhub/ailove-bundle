<?php

namespace ADW\AiloveBundle\Order;

use ADW\AiloveBundle\Model\Contact;

/**
 * Class Order
 *
 * @package ADW\AiloveBundle\Repository
 * @author Artur Vesker
 */
class Order
{

    /**
     * @var string
     */
    protected $object;

    /**
     * @var Contact
     */
    protected $deliveryAddress;

    /**
     * @var string
     */
    protected $comment;

    /**
     * Order constructor.
     *
     * @param string $object
     * @param Contact $deliveryAddress
     * @param string $comment
     */
    public function __construct($object, Contact $deliveryAddress, $comment = null)
    {
        $this->object = $object;
        $this->deliveryAddress = $deliveryAddress;
        $this->comment = $comment;
    }

    /**
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return Contact
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

}