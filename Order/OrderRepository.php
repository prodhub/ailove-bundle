<?php

namespace ADW\AiloveBundle\Order;

use ADW\AiloveBundle\Event\OrderEvent;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use ADW\AiloveBundle\Repository\UserRelatedAbstractRepository;
use ADW\AiloveBundle\Repository\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class OrderRepository
 *
 * @package ADW\AiloveBundle\Repository
 * @author Artur Vesker
 */
class OrderRepository extends UserRelatedAbstractRepository
{

    /**
     * @var PrizeRepository
     */
    protected $prizeRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * OrderRepository constructor.
     *
     * @param Client $client
     * @param User $user
     * @param LoggerInterface $logger
     * @param PrizeRepository $prizeRepository
     * @param UserRepository $userRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(Client $client, User $user, LoggerInterface $logger, PrizeRepository $prizeRepository, UserRepository $userRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->userRepository = $userRepository;
        $this->prizeRepository = $prizeRepository;
        $this->eventDispatcher = $eventDispatcher;
        parent::__construct($client, $user, $logger);
    }

    /**
     * @param Order $order
     * @return \ADW\AiloveBundle\Model\PrizeWinner
     */
    public function add(Order $order)
    {
        $user = $this->getUser();
        $user->getContacts()->save($order->getDeliveryAddress());
        $this->userRepository->save($user);

        $result = $this->prizeRepository->add($user, $order->getObject());

        $this->eventDispatcher->dispatch(OrderEvent::NAME, new OrderEvent($order, $user, $result));

        return $result;
    }

}
