<?php

namespace ADW\AiloveBundle\Action;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Exception\ActionException;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\UserAction;
use ADW\AiloveBundle\RestClient\Method\User\Action\DoActionMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Action\GetUserActionsListMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Action\GetUserPermissionsMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class ActionsManager.
 *
 * @author Artur Vesker
 */
class ActionsManager
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $partner;

    protected $allowedActions;

    /**
     * ActionsManager constructor.
     *
     * @param Client $client
     * @param Logger $logger
     * @param $partner
     * @param array $allowedActions
     */
    public function __construct(Client $client, Logger $logger, $partner, array $allowedActions = [])
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->partner = $partner;
        $this->allowedActions = $allowedActions;
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function getUserPermissions(User $user)
    {
        return $this->client->request(new GetUserPermissionsMethodDescription(), ['uid' => (int) $user->getId()]);
    }

    /**
     * @param User $user
     * @param $action
     * @param null       $promo
     * @param array|null $selector
     *
     * @return UserAction
     */
    public function doAction(User $user, $action, $promo = null, array $selector = null)
    {
        if (!in_array($action, $this->allowedActions)) {
            throw new ActionException($action, 'Действие недоступно');
        }

        $result = $this->client->request(new DoActionMethodDescription(), [
            'uid' => (int) $user->getId(),
            'action' => $action,
            'promo' => $promo,
            'selector' => $selector,
        ]);

        $this->logger->info("Created Action #{$result->getId()}");

        return $result;
    }

    /**
     * @param User $user
     *
     * @return LazyCollection|UserAction[]
     */
    public function getUserActions(User $user)
    {
        return $this->client->request(new GetUserActionsListMethodDescription(), ['uid' => (int) $user->getId()]);
    }

    /**
     * @param User $user
     * @param $permission
     *
     * @return bool
     */
    public function hasPermission(User $user, $permission)
    {
        $permissions = $this->getUserPermissions($user);

        return in_array($this->normalizeName($permission), $permissions['permissions']);
    }

    /**
     * @param string $name
     * @return string
     */
    public function normalizeName($name)
    {
        return $this->partner . '.' . $name;
    }
}
