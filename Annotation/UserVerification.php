<?php

namespace ADW\AiloveBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * Class UserVerification
 *
 * @package ADW\AiloveBundle\Annotation
 * @author Artur Vesker
 *
 * @Annotation
 */
class UserVerification extends ConfigurationAnnotation
{

    /**
     * @var bool
     */
    protected $emailVerification;

    /**
     * @var bool
     */
    protected $phoneVerification;

    /**
     * @return boolean
     */
    public function isEmailVerification()
    {
        return $this->emailVerification;
    }

    /**
     * @param boolean $emailVerification
     * @return self
     */
    public function setEmailVerification($emailVerification)
    {
        $this->emailVerification = $emailVerification;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPhoneVerification()
    {
        return $this->phoneVerification;
    }

    /**
     * @param boolean $phoneVerification
     * @return self
     */
    public function setPhoneVerification($phoneVerification)
    {
        $this->phoneVerification = $phoneVerification;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAliasName()
    {
        return 'ailove_user_verified';
    }

    /**
     * @inheritdoc
     */
    public function allowArray()
    {
        return false;
    }


}