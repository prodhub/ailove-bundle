<?php

namespace ADW\AiloveBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * Class PromoMember
 *
 * @package ADW\AiloveBundle\Annotation
 * @author Artur Vesker
 *
 * @Annotation
 */
class PromoMember extends ConfigurationAnnotation
{

    /**
     * @var array|string
     */
    protected $promo;

    /**
     * @inheritdoc
     */
    public function getAliasName()
    {
        return 'ailove_promo_member';
    }

    /**
     * @inheritdoc
     */
    public function allowArray()
    {
        return true;
    }

    /**
     * @return array|string
     */
    public function getValue()
    {
        return $this->promo;
    }

    /**
     * @param array|string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->promo = $value;

        return $this;
    }

    /**
     * @return array|string
     */
    public function getPromo()
    {
        return $this->promo;
    }

}