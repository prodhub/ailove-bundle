<?php


namespace ADW\AiloveBundle\Promo;

use ADW\AiloveBundle\Model\Promo;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\Promo\X5Qestion;
use ADW\AiloveBundle\RestClient\Method\Tmp\X5PromoQuestionMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GetUserOrdersReceiptsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\PatchUserOrdersReceiptsMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\UserOrdersReceiptsAnswerMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GetUserOrdersReceiptsStatusMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\RestClientBundle\Client\Client;

/**
 * Class X5PromoManager.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class X5PromoManager
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Client $client
     * @param Logger $logger
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param $id
     *
     * @return X5Qestion
     */
    public function getQuestion($id)
    {
        $result = $this->client->request(new X5PromoQuestionMethodDescription(), [
            'question_id' => $id
        ]);
        return $result;
    }

    /**
     * @param User $user
     * @param $promo
     */
    public function getUserOrdersReceipt(User $user, $promo)
    {
        $result = $this->client->request(new GetUserOrdersReceiptsMethodDescription(), [
            'uid' => $user->getId(),
            'promo' => $promo,
        ]);
        return $result;
    }

    /**
     * @param User $user
     * @param $promo
     */
    public function getUserOrdersReceiptStatus(User $user, $promo)
    {
        $result = $this->client->request(new GetUserOrdersReceiptsStatusMethodDescription(), [
            'uid' => $user->getId(),
            'promo' => $promo,
        ]);
        return $result;
    }


    /**
     * @param User $user
     * @param $id
     */
    public function setUserOrdersReceiptAnswer(User $user, $id)
    {
        $result = $this->client->request(new UserOrdersReceiptsAnswerMethodDescription(), [
            'uid' => $user->getId(),
            'answer' => $id,
        ]);
        return $result;
    }

    /**
     * @param User $user
     * @param $id
     * @param $prizeUserId
     */
    public function setUserOrdersReceipt(User $user, $id, $prizeUserId)
    {
        $result = $this->client->request(new PatchUserOrdersReceiptsMethodDescription(), [
            'uid' => $user->getId(),
            'receipt_id' => $id,
            'user' => $user->getId(),
            'prize' => $prizeUserId,
        ]);
        return $result;
    }


}
