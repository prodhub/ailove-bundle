<?php

namespace ADW\AiloveBundle\Promo;

use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Promo\Prize\Limiter\PrizeLimitationStrategyInterface;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GivePrizeFelixMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\GivePrizeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\ListUserPrizesMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class PrizeFelixManager.
 *
 * @author Anton Prokhorov
 */
class PrizeFelixManager
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Client $client
     * @param Logger $logger
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param User $user
     * @param $prize
     *
     * @return PrizeWinner
     */
    public function givePrize(User $user, $promo, $prize)
    {
        $result = $this->client->request(new GivePrizeFelixMethodDescription(), [
            'uid' => $user->getId(),
            'promo' => $promo,
            'prize' => $prize,
        ]);

        $this->logger->info("Give felix prize \"{$prize}\" #{$result->getId()}");

        return $result;
    }

    /**
     * @param User $user
     * @param $name
     * @return bool
     */
    public function has(User $user, $name)
    {
        foreach ($this->findAll($user) as $prize) {
            if ($prize->getPrize()->getSystemName() == $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @return LazyCollection|PrizeWinner[]
     */
    public function findAll(User $user)
    {
        return $this->client->request(new ListUserPrizesMethodDescription(), ['uid' => $user->getId()]);
    }
}
