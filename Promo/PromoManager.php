<?php

namespace ADW\AiloveBundle\Promo;

use ADW\AiloveBundle\Exception\PromoNotAvailableException;
use ADW\AiloveBundle\Model\Promo;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PromoRepository;
use ADW\AiloveBundle\RestClient\Method\User\Promo\AcceptPromoMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\Promo\ActivateCodeMethodDescription;
use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\RestClientBundle\Client\Client;

/**
 * Class PromoManager.
 *
 * @author Artur Vesker
 */
class PromoManager
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var PromoRepository
     */
    protected $promoRepository;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param Client $client
     * @param PromoRepository $promoRepository
     * @param array $config
     */
    public function __construct(Client $client, PromoRepository $promoRepository, array $config = [])
    {
        $this->client = $client;
        $this->promoRepository = $promoRepository;
        $this->config = $config;
    }

    /**
     * @param User $user
     * @param $promo
     *
     * @return self
     */
    public function join(User $user, $promo)
    {
        $this->client->request(new AcceptPromoMethodDescription(), [
            'agreed_terms' => true,
            'uid' => $user->getId(),
            'promo' => $promo,
        ]);

        return $this;
    }

    /**
     * @param User $user
     * @param $code
     *
     * @return bool
     */
    public function checkCode(User $user, $code)
    {
        try {
            return $this->assertCode($user, $code);
        } catch (ValidationViolationException $e) {
            return false;
        }
    }

    /**
     * @param User $user
     * @param $code
     *
     * @return bool
     */
    public function assertCode(User $user, $code)
    {
        $this->client->request(new ActivateCodeMethodDescription(), [
            'uid' => $user->getId(),
            'code' => $code,
            'check_only' => true,
        ]);

        return true;
    }

    /**
     * @param User $user
     * @param $code
     * @return User\Promo\UserPromoCode
     */
    public function getCode(User $user, $code)
    {
        return $this->client->request(new ActivateCodeMethodDescription(), [
            'uid' => $user->getId(),
            'code' => $code,
            'check_only' => true,
        ]);
    }

    /**
     * @param User $user
     * @param $code
     *
     * @return User\Promo\UserPromoCode
     */
    public function activateCode(User $user, $code)
    {
        return $this->client->request(new ActivateCodeMethodDescription(), [
            'uid' => $user->getId(),
            'code' => (string)$code,
        ]);
    }

    /**
     * @param User         $user
     * @param string|Promo $promo
     */
    public function isMemberOfPromo(User $user, $promo)
    {
        dump($this->promoRepository->findByUser($user));
        die();
    }

    /**
     * @param User             $user
     * @param string[]|Promo[] $promos
     *
     * @return bool
     */
    public function isMemberOfAtLeastOnePromo(User $user, array $promos)
    {
        foreach ($this->promoRepository->findByUser($user) as $userPromo) {
            foreach ($promos as $promo) {
                if ((string) $promo === (string) $userPromo) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param User             $user
     * @param string[]|Promo[] $promos
     */
    public function isMemberOfPromos(User $user, array $promos)
    {
        dump($this->promoRepository->findByUser($user));
        die();
    }

}
