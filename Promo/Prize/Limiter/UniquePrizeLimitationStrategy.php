<?php

namespace ADW\AiloveBundle\Promo\Prize\Limiter;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;

/**
 * Class UniquePrizeLimitationStrategy
 *
 * @author Artur Vesker
 */
class UniquePrizeLimitationStrategy
{

    /**
     * @var PrizeRepository
     */
    protected $prizeRepository;

    /**
     * UniquePrizeLimitStrategy constructor.
     *
     * @param PrizeRepository $prizeRepository
     */
    public function __construct(PrizeRepository $prizeRepository)
    {
        $this->prizeRepository = $prizeRepository;
    }

    /**
     * @inheritdoc
     */
    public function isAvailable(User $user, $prize)
    {
        foreach ($this->prizeRepository->findByUser($user) as $prizeWinner) {
            if ($prizeWinner->getPrize()->getSystemName() == $prize) {
                return false;
            }
        }

        return true;
    }

}