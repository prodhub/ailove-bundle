<?php

namespace ADW\AiloveBundle\Promo\Prize\Limiter;

use ADW\AiloveBundle\Model\Prize;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class DateIntervalPrizeLimitationStrategy
 *
 * @author Artur Vesker
 */
class DateIntervalPrizeLimitationStrategy
{

    const MODE_SAME_PRIZE = 'same_prize';
    const MODE_ALL = 'all';
    const MODE_SAME_PROMO = 'same_promo';

    /**
     * @param User $user
     * @param Prize $prize
     * @param $prizes
     * @param ExecutionContextInterface $context
     * @param array $options
     */
    public function __invoke(User $user, Prize $prize, $prizes, ExecutionContextInterface $context, array $options = [])
    {
        $prizeName = $prize->getSystemName();

        if (count($prizes) < 1) {
            return;
        }

        if ($options['mode'] == self::MODE_ALL) {

        }

        if ($options['mode'] == self::MODE_SAME_PRIZE) {

            /**
             * @var PrizeWinner $prizeWinner
             */
            foreach ($prizes as $prizeWinner) {
                if ($prizeWinner->getPrize()->getSystemName() == $prizeName) {
                    $this->assertInteval($prizeWinner, $options['interval'], $context);

                    return;
                }
            }

            return;
        }

        if ($options['mode'] == self::MODE_SAME_PROMO) {

            /**
             * @var PrizeWinner $prizeWinner
             */
            foreach ($prizes as $prizeWinner) {
                if ($prizeWinner->getPrize()->getPromoName() == $options['promo']) {
                    $this->assertInteval($prizeWinner, $options['interval'], $context);

                    return;
                }
            }

            return;
        }

        throw new \InvalidArgumentException('Mode ' . $options['mode'] . ' not defined');
    }

    /**
     * @param PrizeWinner $prize
     * @param $interval
     * @param ExecutionContextInterface $context
     */
    private function assertInteval(PrizeWinner $prize, $interval, ExecutionContextInterface $context)
    {
        if (time() - $prize->getWonAt()->getTimestamp() < $interval) {
            $context->addViolation('Приз был выдан недавно');
        }
    }


}