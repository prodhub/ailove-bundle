<?php

namespace ADW\AiloveBundle\Promo\Prize\Limiter;

use ADW\AiloveBundle\Model\Prize;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class CountPrizeLimitationStrategy
 *
 * @author Artur Vesker
 */
class CountPrizeLimitationStrategy
{

    const MODE_SAME_PRIZE = 'same_prize';
    const MODE_ALL = 'all';
    const MODE_SAME_PROMO = 'same_promo';

    /**
     * @param User $user
     * @param Prize $prize
     * @param $prizes
     * @param ExecutionContextInterface $context
     * @param array $options
     */
    public function __invoke(User $user, Prize $prize, $prizes, ExecutionContextInterface $context, array $options = [])
    {
        $prizeName = $prize->getSystemName();

        if (count($prizes) < $options['count']) {
            return;
        }

        if ($options['mode'] == self::MODE_ALL) {
            $this->assertCount(count($prizes), $options['count'], $context);

            return;
        }

        if ($options['mode'] == self::MODE_SAME_PRIZE) {

            $count = 0;

            /**
             * @var PrizeWinner $prizeWinner
             */
            foreach ($prizes as $prizeWinner) {
                if ($prizeWinner->getPrize()->getSystemName() == $prizeName) {
                    $count++;
                }
            }

            $this->assertCount($count, $options['count'], $context);

            return;
        }

        if ($options['mode'] == self::MODE_SAME_PROMO) {

            $count = 0;

            /**
             * @var PrizeWinner $prizeWinner
             */
            foreach ($prizes as $prizeWinner) {
                if ($prizeWinner->getPrize()->getPromoName() == $options['promo']) {
                    $count++;
                }

                $this->assertCount($count, $options['count'], $context);
                return;
            }

            return;
        }

        throw new \InvalidArgumentException('Mode ' . $options['mode'] . ' not defined');
    }

    /**
     * @param $actualCount
     * @param $availableCount
     * @param ExecutionContextInterface $context
     */
    private function assertCount($actualCount, $availableCount, ExecutionContextInterface $context)
    {
        if ($actualCount > $availableCount) {
            $context->addViolation('Достигнуто ограничение по количеству призов');
        }
    }
}