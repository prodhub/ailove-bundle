<?php

namespace ADW\AiloveBundle\Serializer;

use ADW\AiloveBundle\CRM\Security\Token;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * Class TokenHandler.
 *
 * @author Artur Vesker
 */
class TokenHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $formats = ['json'];
        $collectionTypes = [
            'Token',
            'ADW\AiloveBundle\CRM\Security\Token',
        ];

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = array(
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                );
            }
        }

        return $methods;
    }

    public function deserializeTokenFromJson(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        return new Token($data['token']['key'], $data['token']['expire']);
    }
}
