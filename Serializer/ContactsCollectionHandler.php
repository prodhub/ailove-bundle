<?php

namespace ADW\AiloveBundle\Serializer;

use ADW\AiloveBundle\Collection\ContactsCollection;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;

/**
 * Class ContactsCollectionHandler
 *
 * @package ADW\AiloveBundle\Serializer
 * @author Artur Vesker
 */
class ContactsCollectionHandler extends ArrayCollectionHandler
{

    /**
     * @inheritdoc
     */
    public static function getSubscribingMethods()
    {
        $methods = array();
        $formats = array('json', 'ailove_json');
        $collectionTypes = array(
            'ContactsCollection',
        );

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = array(
                    'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'serializeCollection',
                );

                $methods[] = array(
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'deserializeCollection',
                );
            }
        }

        return $methods;
    }

    /**
     * @inheritdoc
     */
    public function deserializeCollection(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $type['name'] = 'array';

        return new ContactsCollection($visitor->visitArray($data, $type, $context));
    }

}