<?php

namespace ADW\AiloveBundle\Serializer;

use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\DateHandler as BaseDateHandler;

/**
 * Class DateHandler
 *
 * @package ADW\AiloveBundle\Serializer
 * @author Artur Vesker
 */
class DateHandler extends BaseDateHandler
{

    /**
     * @inheritdoc
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'type' => 'DateTime',
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'ailove_json',
                'method' => 'deserializeDateTimeFromJson'
            ],
            [
                'type' => 'DateTime',
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'ailove_json',
                'method' => 'serializeDateTime'
            ]
        ];
    }

}