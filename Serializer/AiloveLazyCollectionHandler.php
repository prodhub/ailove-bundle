<?php

namespace ADW\AiloveBundle\Serializer;

use ADW\AiloveBundle\AiloveSerializer;
use ADW\AiloveBundle\Collection\CachedPageLoader;
use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Collection\PageLoader;
use ADW\AiloveBundle\Collection\Paginator;
use ADW\AiloveBundle\Exception\InvalidResponseException;
use GuzzleHttp\Client;
use JMS\Serializer\Context;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\VisitorInterface;

/**
 * Class AiloveLazyCollectionHandler.
 *
 * @author Artur Vesker
 */
class AiloveLazyCollectionHandler implements SubscribingHandlerInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var AiloveSerializer
     */
    protected $serializer;


    public function __construct(AiloveSerializer $serializer, Client $client)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'type' => 'AiloveLazyCollection',
                'format' => 'json',
                'method' => 'deserialize',
            ],
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'type' => 'ADW\AiloveBundle\Collection\LazyCollection',
                'format' => 'ailove_json',
                'method' => 'serialize',
            ],
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param $data
     * @param array   $type
     * @param Context $context
     *
     * @return mixed
     */
    public function deserialize(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        if (!array_key_exists('results', $data)) {
            throw new InvalidResponseException('Invalid CRM pagination: missing results field. It\'s PAYAL\'NIK time!');
        }

        $loader = new PageLoader(
            $this->client,
            $this->serializer,
            $context->attributes->get('_request')->get(),
            $this->getDomainType($type),
            $context->getFormat(),
            $this->serializer->fillContext($context, DeserializationContext::create())

        );
        
        $loader = new CachedPageLoader($loader, [$this->calculateCurrentPage($data['next'], $data['previous']) => $data['results']]);

        return new LazyCollection($data['count'], new Paginator($loader));
    }

    /**
     * @param $type
     *
     * @return array
     */
    private function getDomainType($type)
    {
        if (count($type['params']) == 0) {
            return ['name' => 'array', 'params' => []];
        }

        return $type['params'][0]['name'];
    }

    /**
     * @param $next
     * @param $prev
     * @return int
     */
    private function calculateCurrentPage($next, $prev)
    {
        if (!$prev) {
            return 1;
        }

        $parsed = parse_url($next);
        parse_str($parsed['query'], $parsed);

        return $parsed['page'] - 1;
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param LazyCollection $collection
     * @param $type
     * @param SerializationContext $context
     * @return mixed
     */
    public function serialize(JsonSerializationVisitor $visitor, LazyCollection $collection, $type, SerializationContext $context)
    {
        return $context->accept(['elements' => iterator_to_array($collection)]);
    }
}
