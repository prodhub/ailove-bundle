<?php

namespace ADW\AiloveBundle\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * Class AiloveDataHandler.
 *
 * @author Artur Vesker
 */
class AiloveDataHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $formats = ['json', 'ailove_json'];
        $collectionTypes = [
            'AiloveData',
        ];

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = array(
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'deserialize',
                );
            }
        }

        return $methods;
    }

    /**
     * @param VisitorInterface $visitor
     * @param $data
     * @param array   $type
     * @param Context $context
     *
     * @return mixed
     */
    public function deserialize(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $type = $context->attributes->get('_ailove_type')->get();

        return $context->accept($data, $type);
    }
}
