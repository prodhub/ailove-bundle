<?php

namespace ADW\AiloveBundle\Serializer;

use ADW\AiloveBundle\Model\UploadImage;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * Class UploadedImageSubscriber.
 *
 * @author Artur Vesker
 */
class UploadedImageSubscriber implements SubscribingHandlerInterface
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @param string $host
     */
    public function __construct($host)
    {
        $this->host = $host;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $formats = ['json'];
        $collectionTypes = [
            'ADW\AiloveBundle\Model\UploadImage',
        ];

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = array(
                    'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'deserializeUploadImageDataFromJson',
                );
            }
        }

        return $methods;
    }

    /**
     * {@inheritdoc}
     */
    public function deserializeUploadImageDataFromJson(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        if (is_string($data)) {
            if (strpos($data, '://')) {
                $url = $data;
                $path = substr($data, strpos($data, '/data/') + 6);
            } else {
                $url = 'http://'.$this->host.'/data/'.$data;
                $path  = $data;
            }

            return new UploadImage($path, $url);
        }

        if (is_array($data)) {
            return new UploadImage($data['image'], $data['image_url']);
        }
    }
}
