<?php

namespace ADW\AiloveBundle\Serializer;

use ADW\AiloveBundle\CRM\Security\Token;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * Class UserHandler.
 *
 * @author Artur Vesker
 */
class UserHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'type' => 'AiloveUser',
                'format' => 'json',
                'method' => 'deserializeAiloveUserFromJson'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param $data
     * @param array $type
     * @param Context $context
     * @return mixed
     */
    public function deserializeAiloveUserFromJson(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $type['name'] = $this->discriminateClass($data, $type);
        return $context->accept($data, $type);
    }

    /**
     * @param $data
     * @param $type
     * @return string
     */
    public function discriminateClass($data, $type)
    {
        $type = $data['type']['system_name'];

        switch ($type) {
            case 'vet':
                return 'ADW\AiloveBundle\Model\Vet';
        }

        return 'ADW\AiloveBundle\Model\User';
    }
}
