<?php

namespace ADW\AiloveBundle\Serializer;

use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class VerificationUserSubscriber
 *
 * @package ADW\AiloveBundle\Serializer
 * @author Artur Vesker
 */
class VerificationUserSubscriber implements EventSubscriberInterface
{

    /**
     * @var bool
     */
    protected $sendEmail = true;

    /**
     * @var bool
     */
    protected $sendSms = true;

    /**
     * VerificationUserSubscriber constructor.
     *
     * @param bool $sendEmail
     * @param bool $sendSms
     */
    public function __construct($sendEmail = true, $sendSms = true)
    {
        $this->sendEmail = $sendEmail;
        $this->sendSms = $sendSms;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event' => Events::POST_SERIALIZE,
                'class' => 'ADW\AiloveBundle\Model\User',
                'format' => 'ailove_json',
                'method' => 'setUpSendVerificationMessages'
            ]
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function setUpSendVerificationMessages(ObjectEvent $event)
    {
        $context = $event->getContext();
        $visitor = $context->getVisitor();

        if (!$visitor instanceof JsonSerializationVisitor) {
            return;
        }

        if (!$groups = $context->attributes->get('groups')->getOrElse(false)) {
            return;
        }

        if (!in_array('create', $groups)) {
            return;
        }

        $visitor->addData('send_sms', $this->sendSms);
        $visitor->addData('send_email', $this->sendEmail);
    }

}