<?php

namespace ADW\AiloveBundle\Exception;

use Exception;

/**
 * Class PromoNotAvailableException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class PromoNotAvailableException extends \RuntimeException
{

    /**
     * @var string
     */
    protected $promo;

    /**
     * @param string $promo
     * @param string $message
     * @param Exception $previous
     */
    public function __construct($promo, $message = 'Промо недоступно', Exception $previous = null)
    {
        $this->promo = $promo;
        parent::__construct($message, 0, $previous);
    }

    /**
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

}