<?php

namespace ADW\AiloveBundle\Exception;

use ADW\AiloveBundle\CRM\Description\DescriptionInterface;
use ADW\AiloveBundle\Exception\Handler\ValidationErrorHandler;
use ADW\AiloveBundle\Model\ValidationError;
use ADW\CommonBundle\Exception\ValidationViolationException;
use ADW\CommonBundle\Validation\CompositeViolation;
use GuzzleHttp\Exception\RequestException;
use JMS\Serializer\Metadata\PropertyMetadata;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\Serializer;

/**
 * Class ExceptionFactory.
 *
 * @author Artur Vesker
 */
class ExceptionFactory
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var \Metadata\MetadataFactoryInterface
     */
    protected $metadataFactory;

    /**
     * @var SerializedNameAnnotationStrategy
     */
    protected $namingStrategy;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
        $this->metadataFactory = $serializer->getMetadataFactory();
        $this->namingStrategy = new SerializedNameAnnotationStrategy(new CamelCaseNamingStrategy());
    }

    public function create(RequestException $e, DescriptionInterface $description, $body)
    {
        $handler = new ValidationErrorHandler($this->serializer);

        $handler->handle($e, $description, $body);

        if (!$response = $e->getResponse()) {
            throw $e;
        }

        $metadata = null;

        $properties = [];

        if ($response->getStatusCode() == 400) {
            $exceptionData = json_decode((string) $response->getBody(), true)['data'];

            if ($exceptionData['error_code'] == 'validation_error') {
                $message = $exceptionData['error_message'];

                unset($exceptionData['status_code']);
                unset($exceptionData['error_message']);
                unset($exceptionData['error_code']);

                throw new ValidationViolationException(
                    $this->createViolation($exceptionData, $description->getModel()),
                    $message
                );
            }
        }

        return $e;
    }

    private function addErrors($data, &$errors)
    {
        foreach ($data as $name => $field) {
            if ($this->isAssoc($field)) {
                $this->addErrors($field, $errors);

                continue;
            }

            $errors[] = new ValidationError($name, $name, $field);
        }
    }

    private function isAssoc($arr)
    {
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public function fillViolation(CompositeViolation $violation, $errors)
    {
        if ($this->isAssoc($errors)) {
            foreach ($errors as $name => $data) {
                $violation->addChild($this->fillViolation(new CompositeViolation($name, [], $violation), $data));
            }
        } else {
            $violation->setMessages($errors);
        }

        return $violation;
    }

    public function handleErrors($errors, CompositeViolation $parent = null, PropertyMetadata $propertyMetadata = null, $meyname = null)
    {
        if (!$this->isAssoc($errors)) {
            return new CompositeViolation($propertyMetadata ? $propertyMetadata->name : $meyname, $errors, $parent);
        }

        $violation = new CompositeViolation($propertyMetadata ? $propertyMetadata->name : $meyname, [], $parent);

        foreach ($errors as $name => $data) {
            $metadata = null;

            if ($propertyMetadata) {
                if (class_exists($propertyMetadata->type['name'])) {
                    $metadata = $this->metadataFactory->getMetadataForClass($propertyMetadata->type['name']);
                }
            }

            $violation->addChild(
                $this->handleErrors(
                    $data,
                    $violation,
                    $metadata ? $this->findProperty($name, $metadata->propertyMetadata) : null,
                    $metadata ? null : $name
                )
            );
        }

        return $violation;
    }

    /**
     * @param $name
     * @param PropertyMetadata[] $properties
     *
     * @return PropertyMetadata
     */
    public function findProperty($name, $properties)
    {
        foreach ($properties as $property) {
            if ($this->namingStrategy->translateName($property) == $name) {
                return $property;
            }
        }

        return;
    }

    public function createViolation($errors, $model)
    {
        if (!$this->isAssoc($errors)) {
            return new CompositeViolation(null, $errors);
        }

        $metadata = null;

        if (class_exists($model)) {
            $metadata = $this->metadataFactory->getMetadataForClass($model);
        }

        $violation = new CompositeViolation(null, []);

        foreach ($errors as $name => $data) {
            $violation->addChild(
                $this->handleErrors(
                    $data,
                    $violation,
                    $metadata ? $this->findProperty($name, $metadata->propertyMetadata) : null,
                    $name
                )
            );
        }

        return $violation;
    }
}
