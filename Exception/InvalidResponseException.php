<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class InvalidResponseException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class InvalidResponseException extends \RuntimeException
{
    
}