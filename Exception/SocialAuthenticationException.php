<?php

namespace ADW\AiloveBundle\Exception;

use Exception;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class SocialAuthenticationException.
 *
 * @author Artur Vesker
 */
class SocialAuthenticationException extends AuthenticationException
{
    /**
     * @var UserResponseInterface
     */
    protected $userResponse;

    public function __construct(UserResponseInterface $userResponse, $message = '', $code = 0, Exception $previous = null)
    {
        $this->userResponse = $userResponse;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return UserResponseInterface
     */
    public function getUserResponse()
    {
        return $this->userResponse;
    }
}
