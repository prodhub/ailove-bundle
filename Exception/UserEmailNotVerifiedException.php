<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class UserEmailNotVerifiedException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class UserEmailNotVerifiedException extends UserNotVerifiedException
{

}