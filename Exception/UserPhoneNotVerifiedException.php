<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class UserPhoneNotVerifiedException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class UserPhoneNotVerifiedException extends UserNotVerifiedException
{

}