<?php
/**
 * @author Artur Vesker (arturvesker@gmail.com)
 */
namespace ADW\AiloveBundle\Exception;

use ADW\CommonBundle\Exception\ValidationViolationException;

/**
 * Class UserAlreadyExistsException
 */
class UserAlreadyExistsException extends ValidationViolationException
{

}