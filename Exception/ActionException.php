<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class ActionException
 *
 * @author Artur Vesker
 */
class ActionException extends \RuntimeException
{

    /**
     * @var string
     */
    protected $action;

    /**
     * @param string $action
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($action, $message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

}