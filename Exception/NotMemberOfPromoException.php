<?php

namespace ADW\AiloveBundle\Exception;

use ADW\AiloveBundle\Model\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class NotMemberOfPromoException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class NotMemberOfPromoException extends AccessDeniedException
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var string|array
     */
    protected $promo;

    /**
     * @param User $user
     * @param string|array $promo
     * @param string $message
     */
    public function __construct(User $user, $promo, $message = 'Not member of promo')
    {
        $this->user = $user;
        $this->promo = $promo;
        parent::__construct($message);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

}