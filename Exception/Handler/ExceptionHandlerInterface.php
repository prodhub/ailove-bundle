<?php

namespace ADW\AiloveBundle\Exception\Handler;

use GuzzleHttp\Exception\RequestException;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Interface ExceptionHandlerInterface.
 */
interface ExceptionHandlerInterface
{
    public function handle(RequestException $exception, MethodDescriptionInterface $methodDescriptionInterface, array $options);
}
