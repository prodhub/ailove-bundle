<?php

namespace ADW\AiloveBundle\Exception;

use ADW\AiloveBundle\Model\ValidationError;

/**
 * Class ValidationErrorException.
 *
 * @author  Artur Vesker
 */
class ValidationErrorException extends \ErrorException
{
    /**
     * @var ValidationError[]
     */
    protected $invalidFields;

    /**
     * @param ValidationError[] $invalidFields
     */
    public function __construct(array $invalidFields)
    {
        $this->invalidFields = $invalidFields;

        $message = implode("\r\n", $invalidFields);

        parent::__construct($message, 400);
    }

    /**
     * @return \ADW\AiloveBundle\Model\ValidationError[]
     */
    public function getErrors()
    {
        return $this->invalidFields;
    }
}
