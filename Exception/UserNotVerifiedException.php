<?php

namespace ADW\AiloveBundle\Exception;

use ADW\AiloveBundle\Model\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserNotVerifiedException
 *
 * @package ADW\AiloveBundle\Exception
 * @author Artur Vesker
 */
class UserNotVerifiedException extends AccessDeniedException
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     * @param string $message
     */
    public function __construct(User $user, $message = 'User not verified')
    {
        $this->user = $user;
        parent::__construct($message);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}