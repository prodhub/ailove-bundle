<?php

namespace ADW\AiloveBundle\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class InvalidUserTokenException
 *
 * @author Artur Vesker
 */
class InvalidUserTokenException extends AuthenticationException
{

}