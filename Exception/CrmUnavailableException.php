<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class CrmUnavailableException.
 *
 * @author Artur Vesker
 */
class CrmUnavailableException extends \RuntimeException
{
}
