<?php

namespace ADW\AiloveBundle\Exception;

/**
 * Class ReadOnlyException.
 *
 * @author Artur Vesker
 */
class ReadOnlyException extends \RuntimeException
{
}
