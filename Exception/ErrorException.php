<?php

namespace ADW\AiloveBundle\Exception;

use Exception;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class ErrorException.
 *
 * @author  Artur Vesker
 */
class ErrorException extends \Exception
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("error_code")
     */
    protected $errorCode;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\SerializedName("status_code")
     */
    protected $code;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("error_message")
     */
    protected $message;

    public function __construct($message = '', $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }
}
