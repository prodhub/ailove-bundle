<?php

namespace ADW\AiloveBundle\Twig\Extension;

use ADW\AiloveBundle\Action\ActionsManager;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Promo\PrizeManager;
use ADW\AiloveBundle\Subscription\SubscribeManager;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AiloveTwigExtension.
 *
 * @author Artur Vesker
 */
class AiloveTwigExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $schema;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $partner;

    /**
     * @var ActionsManager
     */
    protected $actionsManager;

    /**
     * @var PrizeManager
     */
    protected $prizeManager;

    /**
     * @var SubscribeManager
     */
    protected $subscriptionManager;

    /**
     * @param $host
     * @param $schema
     * @param $key
     * @param $partner
     */
    public function __construct($host, $schema, $key, $partner, ActionsManager $actionsManager, PrizeManager $prizeManager, SubscribeManager $subscriptionManager)
    {
        $this->host = $host;
        $this->schema = $schema;
        $this->key = $key;
        $this->partner = $partner;
        $this->actionsManager = $actionsManager;
        $this->prizeManager = $prizeManager;
        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('ailove_customer_link', function ($id) {
                return "{$this->schema}://{$this->host}/admin/customers/customer/{$id}/";
            }),
            new \Twig_SimpleFunction('ailove_social_auth_link', function ($network, $partner = null) {
                return "{$this->schema}://{$this->host}/social/login/{$network}/?partner_id=" . ($partner ?: $this->partner);
            }),
            new \Twig_SimpleFunction('ailove_has_permission', function ($user, $permission) {
                return $this->actionsManager->hasPermission($user, $permission);
            }),
            new \Twig_SimpleFunction('ailove_has_prize', function ($user, $prize) {
                return $this->prizeManager->has($user, $prize);
            }),
            new \Twig_SimpleFunction('ailove_is_subscribed', function ($user, $subscription) {
                return $this->subscriptionManager->isSubscribed($user, $subscription);
            }),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'ailove';
    }
}
