<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class OnlySystemNameForAiloveTrait
 *
 * @package ADW\AiloveBundle\Model
 * @author Artur Vesker
 */
trait OnlySystemNameForAiloveTrait
{

    /**
     * @return string
     */
    abstract function getSystemName();

    /**
     * @param JsonSerializationVisitor $visitor
     * @return string
     * @internal
     *
     * @Serialized\HandlerCallback(direction="serialization", format="ailove_json")
     */
    public function toJson(JsonSerializationVisitor $visitor)
    {
        return $this->getSystemName();
    }

}