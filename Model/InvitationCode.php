<?php

namespace ADW\AiloveBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class InvitationCode.
 *
 * @author Artur Vesker
 *
 * //TODO: USER
 */
class InvitationCode
{
    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     * @Serialized\SerializedName("is_complete")
     */
    protected $complete;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $code;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $program;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $inviter;

    /**
     * @var ArrayCollection
     *
     * @Serialized\Type("ArrayCollection")
     */
    protected $additional_data;

    public function __construct()
    {
        $this->additional_data = new ArrayCollection();
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        return $this->complete;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @return string
     */
    public function getInviter()
    {
        return $this->inviter;
    }

    /**
     * @return ArrayCollection
     */
    public function getAdditionalData()
    {
        return $this->additional_data;
    }
}
