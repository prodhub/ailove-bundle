<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Class UserType.
 *
 * @author Artur Vesker
 */
class UserType implements RoleInterface
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @param string $name
     * @param string $systemName
     */
    public function __construct($name, $systemName)
    {
        $this->name = $name;
        $this->systemName = $systemName;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRole()
    {
        return 'ROLE_'.strtoupper($this->getSystemName());
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getSystemName();
    }

    use OnlySystemNameForAiloveTrait;
}
