<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SalesDistrict.
 *
 * @author Artur Vesker
 */
class SalesDistrict
{
    /**
     * @Serialized\Type("array")
     *
     * @var array
     */
    protected $salesDistrict;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->salesDistrict['name'];
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->salesDistrict['system_name'];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->salesDistrict['email'];
    }

    use OnlySystemNameForAiloveTrait;
}
