<?php


namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ConfirmKey.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class ConfirmKey
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $confirm_key;

    /**
     * @return string
     */
    public function getConfirmKey()
    {
        return $this->confirm_key;
    }


}