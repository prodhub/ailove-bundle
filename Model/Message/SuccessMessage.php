<?php

namespace ADW\AiloveBundle\Model\Message;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SuccessMessage.
 *
 * @author  Artur Vesker
 */
class SuccessMessage
{
    /**
     * Сообщение об успехе.
     *
     * @var string
     *
     * @Serialized\SerializedName("success")
     * @Serialized\Type("string")
     */
    protected $message;

    /**
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->message;
    }
}
