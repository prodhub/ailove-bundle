<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Prize.
 *
 * @author Artur Vesker
 */
class Prize
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $amountLeft;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $promoName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $description;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $amount;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $action_perform;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmountLeft()
    {
        return $this->amountLeft;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function getActionPerform()
    {
        return $this->action_perform;
    }

    /**
     * @return string
     */
    public function getPromoName()
    {
        return $this->promoName;
    }

    /**
     * @param string $promoName
     * @return self
     */
    public function setPromoName($promoName)
    {
        $this->promoName = $promoName;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;
}
