<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class VetData.
 *
 * @author Artur Vesker
 */
class VetData
{
    /**
     * @var int
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    public $graduationYear;

    /**
     * @var Institution
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Institution")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    public $institution;

    /**
     * @var Clinic
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Clinic")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    public $clinic;

}
