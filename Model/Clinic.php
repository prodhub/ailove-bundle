<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Clinic.
 *
 * @author Artur Vesker
 */
class Clinic
{
    /**
     * @var Country
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Country")
     */
    protected $country;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $region;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $district;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $cityType;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "short"})
     */
    protected $city;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $street;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $streetType;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $house;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $building;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $construction;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $apartment;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $zipcode;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "short"})
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "short"})
     */
    protected $systemName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $code;

    /**
     * @var string
     *
     * @Serialized\Type("boolean")
     */
    protected $isPerfect;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $phone;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $email;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $url;

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     *
     * @return self
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     *
     * @return self
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * @return string
     */
    public function getCityType()
    {
        return $this->cityType;
    }

    /**
     * @param string $cityType
     *
     * @return self
     */
    public function setCityType($cityType)
    {
        $this->cityType = $cityType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetType()
    {
        return $this->streetType;
    }

    /**
     * @param string $streetType
     *
     * @return self
     */
    public function setStreetType($streetType)
    {
        $this->streetType = $streetType;

        return $this;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string $house
     *
     * @return self
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param string $building
     *
     * @return self
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @return string
     */
    public function getConstruction()
    {
        return $this->construction;
    }

    /**
     * @param string $construction
     *
     * @return self
     */
    public function setConstruction($construction)
    {
        $this->construction = $construction;

        return $this;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param string $apartment
     *
     * @return self
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     *
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsPerfect()
    {
        return $this->isPerfect;
    }

    /**
     * @param string $isPerfect
     *
     * @return self
     */
    public function setIsPerfect($isPerfect)
    {
        $this->isPerfect = $isPerfect;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;

}
