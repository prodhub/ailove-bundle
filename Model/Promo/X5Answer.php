<?php


namespace ADW\AiloveBundle\Model\Promo;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class X5Answer.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class X5Answer
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $text;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     */
    protected $right;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return X5Answer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return X5Answer
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRight()
    {
        return $this->right;
    }

    /**
     * @param boolean $right
     * @return X5Answer
     */
    public function setRight($right)
    {
        $this->right = $right;
        return $this;
    }


}