<?php


namespace ADW\AiloveBundle\Model\Promo;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Question.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class Question
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $question;

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return Question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }



}