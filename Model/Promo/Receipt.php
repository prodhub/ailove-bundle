<?php


namespace ADW\AiloveBundle\Model\Promo;

use JMS\Serializer\Annotation as Serialized;


/**
 * Class Receipt
 * Project ailove-bundle.
 * @package ADW\AiloveBundle\Model\Promo
 * @author Anton Prokhorov
 */

class Receipt
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $promo;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $cleared_at;

    /**
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param string $promo
     * @return Receipt
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Receipt
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getClearedAt()
    {
        return $this->cleared_at;
    }

    /**
     * @param string $cleared_at
     * @return Receipt
     */
    public function setClearedAt($cleared_at)
    {
        $this->cleared_at = $cleared_at;
        return $this;
    }


}