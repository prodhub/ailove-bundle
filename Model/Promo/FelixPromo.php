<?php

namespace ADW\AiloveBundle\Model\Promo;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class FelixPromo
 *
 * @package ADW\AiloveBundle\Model\Promo
 * @author Anton Prokhorov
 */
class FelixPromo
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $total_left_code;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $total_left_code_today;

    /**
     * @return int
     */
    public function getTotalLeftCode()
    {
        return $this->total_left_code;
    }

    /**
     * @return int
     */
    public function getTotalLeftCodeToday()
    {
        return $this->total_left_code_today;
    }



}