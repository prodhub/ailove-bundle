<?php

namespace ADW\AiloveBundle\Model\Promo;

use ADW\AiloveBundle\Model\Product;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class PromoCode
 *
 * @package ADW\AiloveBundle\Model\Promo
 * @author Artur Vesker
 */
class PromoCode
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $codeType;

    /**
     * @var string
     *
     * @Serialized\Type("float")
     */
    protected $points;

    /**
     * @var Product
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Product")
     */
    protected $product;

    /**
     * @return string
     */
    public function getCodeType()
    {
        return $this->codeType;
    }

    /**
     * @return string
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

}