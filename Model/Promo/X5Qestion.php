<?php


namespace ADW\AiloveBundle\Model\Promo;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class X5Qestion.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class X5Qestion
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $text;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $hint;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $number;

    /**
     * @var \ADW\AiloveBundle\Model\Promo\X5Answer[]
     *
     * @Serialized\Type("array<ADW\AiloveBundle\Model\Promo\X5Answer>")
     */
    protected $answers;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return X5Qestion
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * @param string $hint
     * @return X5Qestion
     */
    public function setHint($hint)
    {
        $this->hint = $hint;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return X5Qestion
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return X5Answer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param X5Answer[] $answers
     * @return X5Qestion
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }





//    /**
//     * @return OrderItem[]
//     */
//    public function getItems()
//    {
//        return $this->items;
//    }
//
//    /**
//     * @param OrderItem[] $items
//     * @return Order
//     */
//    public function setItems(array $items)
//    {
//        $this->items = $items;
//        return $this;
//    }


}