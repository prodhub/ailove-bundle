<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Contact.
 *
 * @author Artur Vesker
 */
class Contact
{
    const TYPE_GENERAL = 'general';
    const TYPE_DELIVERY = 'delivery';
    const TYPE_BILLING = 'billing';

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\Groups({"load", "update"})
     */
    protected $id;

    /**
     * @var \ADW\AiloveBundle\Model\Country
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Country")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $country;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $region;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $city;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $street;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $streetType;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $house;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $building;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $construction;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $apartment;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $zipcode;

    /**
     * general/delivery/billing.
     *
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("contact_type")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $type;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("phone_mobile")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $mobilePhone;

    /**
     * Create contact instance for delivery.
     *
     * @return Contact
     */
    public static function createDeliveryContact()
    {
        return new self(self::TYPE_DELIVERY);
    }

    /**
     * @param string $type
     */
    public function __construct($type = self::TYPE_GENERAL)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return (string) $this->city;
    }

    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return (string) $this->region;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return (string) $this->street;
    }

    /**
     * @param Country $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param string $region
     * @return self
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @param string $street
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @param string $house
     *
     * @return self
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * @param string $building
     *
     * @return self
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * @param string $construction
     *
     * @return self
     */
    public function setConstruction($construction)
    {
        $this->construction = $construction;

        return $this;
    }

    /**
     * @param string $apartment
     *
     * @return self
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * @param string $zipcode
     *
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipcode;
    }

    /**
     * @return string
     */
    public function getConstruction()
    {
        return $this->construction;
    }

    /**
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param string $mobilePhone
     *
     * @return self
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreetType()
    {
        return $this->streetType;
    }

    /**
     * @param string $streetType
     * @return self
     */
    public function setStreetType($streetType)
    {
        $this->streetType = $streetType;

        return $this;
    }

}
