<?php

namespace ADW\AiloveBundle\Model\User\Invitation;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class InvitationCode.
 *
 * @author Artur Vesker
 */
class InvitationCode
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $program;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     */
    protected $isActive;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $code;

    /*
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    //protected $createdAt;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $countActivated;

    /**
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getCountActivated()
    {
        return $this->countActivated;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->code;
    }
}
