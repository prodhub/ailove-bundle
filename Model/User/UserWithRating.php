<?php

namespace ADW\AiloveBundle\Model\User;

use ADW\AiloveBundle\Model\User;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class UserWithRating
 *
 * @package ADW\AiloveBundle\Model\User
 * @author Artur Vesker
 */
class UserWithRating extends User
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\Groups({"Default", "load"})
     */
    protected $balancePoints;

    /**
     * @return int
     */
    public function getBalancePoints()
    {
        return $this->balancePoints;
    }

}