<?php

namespace ADW\AiloveBundle\Model\User\Promo;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class UserPromoCode
 *
 * @package ADW\AiloveBundle\Model\User\Promo
 * @author Artur Vesker
 */
class UserPromoCode
{

    /**
     * @var integer
     *
     * @Serialized\Type("integer")
     */
    protected $customer;

    /**
     * @var \ADW\AiloveBundle\Model\Promo\PromoCode
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Promo\PromoCode")
     */
    protected $code;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    protected $activatedAt;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     */
    protected $checkOnly;

    /**
     * @return \ADW\AiloveBundle\Model\Promo\PromoCode
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return \DateTime
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * @return boolean
     */
    public function isCheckOnly()
    {
        return $this->checkOnly;
    }

    /**
     * @return int
     */
    public function getCustomer()
    {
        return $this->customer;
    }

}