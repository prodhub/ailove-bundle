<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class UserPromo.
 *
 * @author Artur Vesker
 */
class UserPromo
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $promo;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     */
    protected $agreedTerms;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    protected $createdAt;

    /**
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @return bool
     */
    public function isAgreedTerms()
    {
        return $this->agreedTerms;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->promo;
    }
}
