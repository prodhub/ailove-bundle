<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Order.
 *
 * @author Anton Prokhorov
 */
class Order
{
    /**
     * @var \ADW\AiloveBundle\Model\OrderItem[]
     *
     * @Serialized\Type("array<ADW\AiloveBundle\Model\OrderItem>")
     */
    protected $items;

    /**
     * @var int
     * @Serialized\Type("integer")
     * @Serialized\SerializedName("contact")
     */
    protected $contact;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("order_type")
     */
    protected $orderType;

    /**
     * @return OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param OrderItem[] $items
     * @return Order
     */
    public function setItems(array $items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @param OrderItem[] $item
     * @return Order
     */
    public function addItem($item)
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param int $contact
     * @return Order
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param string $orderType
     * @return Order
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
        return $this;
    }

}
