<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class PrizeWinner.
 *
 * @author Artur Vesker
 */
class PrizeWinner
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $user;

    /**
     * @var Prize
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Prize")
     */
    protected $prize;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    protected $won_at;


    /**
     * @var Payload
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Payload")
     */
    protected $payload;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Prize
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * @return \DateTime
     */
    public function getWonAt()
    {
        return $this->won_at;
    }

    /**
     * @return Payload
     */
    public function getPayload()
    {
        return $this->payload;
    }

}
