<?php

namespace ADW\AiloveBundle\Model\Pet;

use ADW\AiloveBundle\Model\OnlySystemNameForAiloveTrait;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class Size.
 *
 * @author Artur Vesker
 */
class Size
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $hint;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * @param string $hint
     *
     * @return self
     */
    public function setHint($hint)
    {
        $this->hint = $hint;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;
}
