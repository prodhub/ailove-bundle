<?php

namespace ADW\AiloveBundle\Model\Pet;

use ADW\AiloveBundle\Model\AdditionDataEntry;
use ADW\AiloveBundle\Model\SimpleSystemObject;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialized;
use ADW\AiloveBundle\Model\UploadImage;

/**
 * Class Pet.
 *
 * @author Artur Vesker
 */
class Pet
{
    const TYPE_CAT = 'cat';
    const TYPE_DOG = 'dog';

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var UploadImage
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\UploadImage")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $avatar;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $type;

    /**
     * @var Breed
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Pet\Breed")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $breed;

    /**
     * @var Need
     *
     * @Serialized\Type("array<ADW\AiloveBundle\Model\Pet\Need>")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $needs;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $gender;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     * @Serialized\ReadOnly()
     */
    protected $created;

    /**
     * @var Size
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Pet\Size")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $size;

    /**
     * @var \ADW\AiloveBundle\Model\AdditionDataEntry[]
     *
     * @Serialized\Type("array<ADW\AiloveBundle\Model\AdditionDataEntry>")
     * @Serialized\SerializedName("addition_data")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $additionalData;

    /**
     * @var PetAge
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Pet\PetAge")
     * @Serialized\Groups({"create", "Default"})
     */
    protected $age;

    /**
     * Create cat instance.
     *
     * @return Pet
     */
    public static function createCat()
    {
        return new self(self::TYPE_CAT);
    }

    /**
     * Create dog instance.
     *
     * @return Pet
     */
    public static function createDog()
    {
        return new self(self::TYPE_DOG);
    }

    /**
     * @param null $type
     */
    public function __construct($type = null)
    {
        $this->type = $type;
        $this->additionalData = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return UploadImage
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param UploadImage $avatar
     *
     * @return self
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Breed
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * @param Breed $breed
     *
     * @return self
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;

        return $this;
    }

    /**
     * @return Need[]
     */
    public function getNeeds()
    {
        return $this->needs;
    }

    /**
     * @param Need[] $needs
     *
     * @return self
     */
    public function setNeeds($needs)
    {
        $this->needs = $needs;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Size $size
     *
     * @return self
     */
    public function setSize(Size $size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return ArrayCollection|AdditionDataEntry[]
     */
    public function getAdditionalData()
    {
        return $this->additionalData;
    }

    /**
     * Здесь и далее я передаю привет айлову,
     * у которых возраст в запросе в одном формате, а в ответе - в другому
     *
     * @return integer|null
     *
     * @Serialized\VirtualProperty()
     * @Serialized\SerializedName("year")
     * @Serialized\Groups({"create", "Default"})
     */
    public function getYear()
    {
        if (!$this->age) {
            return null;
        }

        return $this->age->getYear();
    }

    /**
     * @return integer|null
     *
     * @Serialized\VirtualProperty()
     * @Serialized\SerializedName("month")
     * @Serialized\Groups({"create", "Default"})
     */
    public function getMonth()
    {
        if (!$this->age) {
            return null;
        }

        return $this->age->getMonth();
    }

    /**
     * @param integer $month
     * @return $this
     */
    public function setMonth($month)
    {
        if ($month > 11) {
            $this->setYear((int)floor($month / 12));
            $this->setMonth($month % 12);
            return $this;
        }

        if (!$this->age) {
            $this->age = new PetAge(0, $month);

            return $this;
        }

        $this->age->setMonth($month);

        return $this;
    }

    /**
     * @param integer $year
     * @return $this
     */
    public function setYear($year)
    {
        if (!$this->age) {
            $this->age = new PetAge($year, 0);

            return $this;
        }

        $this->age->setYear($year);

        return $this;
    }

    /**
     * @param ArrayCollection $additionalData
     * @return self
     */
    public function setAdditionalData($additionalData)
    {
        $this->additionalData = $additionalData;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function addDiscriminator($value)
    {
        $current = null;

        foreach ($this->additionalData as $data) {
            if ($data->getField()->getSystemName() == 'discriminator') {
                $current = $data;
                break;
            }
        }

        if (!$current) {
            $current = new AdditionDataEntry(new SimpleSystemObject('discriminator', 'discriminator'));
            $this->additionalData[] = $current;
        }

        $current->addValue($value);

        return $this;
    }

    /**
     * @param \DateTime $day
     * @return $this
     */
    public function setBirthDay(\DateTime $day)
    {
        $interval = $day->diff(new \DateTime());

        $this->setMonth($interval->m + (12 * $interval->y));

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthDay()
    {
        if (!$month = $this->getMonth()) {
            $month = 0;
        }

        if (!$year = $this->getYear()) {
            $year = 0;
        }

        if (($month === 0) && ($year === 0)) {
            return null;
        }

        $now = new \DateTime();

        return $now->sub(new \DateInterval("P{$year}Y{$month}M"));
    }

    /**
     * @Serialized\VirtualProperty()
     * @Serialized\SerializedName("birth_day")
     */
    public function getBirthDayAsString()
    {
        if (!$birthDay = $this->getBirthDay()) {
            return null;
        }

        return $birthDay->format('m/Y');
    }

}
