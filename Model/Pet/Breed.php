<?php

namespace ADW\AiloveBundle\Model\Pet;

use ADW\AiloveBundle\Model\OnlySystemNameForAiloveTrait;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class Breed.
 *
 * @author Artur Vesker
 */
class Breed
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var array
     *
     * @Serialized\Type("array")
     */
    protected $altNames;

    /**
     * @var Size
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Pet\Size")
     */
    protected $size;

    /**
     * @param $systemName
     */
    public function __construct($systemName)
    {
        $this->systemName = $systemName;
    }

    /**
     * @return array
     */
    public function getAltNames()
    {
        return $this->altNames;
    }

    /**
     * @param array $altNames
     *
     * @return self
     */
    public function setAltNames($altNames)
    {
        $this->altNames = $altNames;

        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Size $size
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;

    public function __toString()
    {
        return $this->name;
    }


}
