<?php

namespace ADW\AiloveBundle\Model\Pet;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class PetAge.
 *
 * @author Artur Vesker
 */
class PetAge
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $year;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $month;

    /**
     * PetAge constructor.
     *
     * @param int $year
     * @param int $month
     */
    public function __construct($year = 0, $month = 0)
    {
        $this->year = (int)$year;
        $this->month = (int)$month;
    }

    /**
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param int $month
     *
     * @return self
     */
    public function setMonth($month)
    {
        $this->month = (int)$month;

        return $this;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     *
     * @return self
     */
    public function setYear($year)
    {
        $this->year = (int)$year;

        return $this;
    }

}
