<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class UserPetsTrait.
 *
 * @author Artur Vesker
 */
trait UserPetsTrait
{
    /**
     * @var array
     */
    protected $pets;

    /**
     * Closure for pets lazy loading.
     *
     * @var \Closure
     *
     * @Serialized\Exclude()
     */
    private $getPetsClosure;

    /**
     * @param callable $getPetsClosure
     */
    public function setGetPetsClosure(\Closure $getPetsClosure)
    {
        $this->getPetsClosure = $getPetsClosure;
    }

    /**
     * @return array
     */
    public function getPets()
    {
        if ($this->pets) {
            return $this->pets;
        }

        $closure = $this->getPetsClosure;

        if (!$closure) {
            return;
        }

        return $this->pets = $closure();
    }
}
