<?php


namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class PhoneCheck.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class PhoneCheck
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $hashkey;


    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Exclude()
     */
    protected $token;

    /**
     * @return string
     */
    public function getHashkey()
    {
        return $this->hashkey;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }



}