<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Captcha.
 *
 * @author Artur Vesker
 */
class Captcha
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("hashkey")
     */
    protected $hashKey;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("captcha")
     */
    protected $image;

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getHashKey()
    {
        return $this->hashKey;
    }

    /**
     * @param string $hashKey
     *
     * @return self
     */
    public function setHashKey($hashKey)
    {
        $this->hashKey = $hashKey;

        return $this;
    }
}
