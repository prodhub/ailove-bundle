<?php

namespace ADW\AiloveBundle\Model;

/**
 * Class Region.
 *
 * @author Artur Vesker
 */
class Region
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $formalName;

    public function __construct($id, $formalName)
    {
        $this->id = $id;
        $this->formalName = $formalName;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFormalName()
    {
        return $this->formalName;
    }
}
