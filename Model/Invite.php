<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Invite.
 *
 * @author Artur Vesker
 */
class Invite
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Serialized\Type("string")
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Serialized\Type("string")
     */
    protected $phone;
}
