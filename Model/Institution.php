<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Institution.
 *
 * @author Artur Vesker
 */
class Institution
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $altName;

    /**
     * @param null $name
     * @param null $systemName
     * @param null $altName
     */
    public function __construct($name = null, $systemName = null, $altName = null)
    {
        $this->name = $name;
        $this->systemName = $systemName;
        $this->altName = $altName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAltName()
    {
        return $this->altName;
    }

    /**
     * @param string $altName
     *
     * @return self
     */
    public function setAltName($altName)
    {
        $this->altName = $altName;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;

}
