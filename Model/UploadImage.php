<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class UploadImage.
 *
 * @author Artur Vesker
 */
class UploadImage
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $image;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $image_url;

    /**
     * @param string $image
     * @param string $image_url
     */
    public function __construct($image, $image_url)
    {
        $this->image = $image;
        $this->image_url = $image_url;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param JsonSerializationVisitor $visitor
     *
     * @return string
     *
     * @Serialized\HandlerCallback(direction="serialization", format="ailove_json")
     */
    public function toJson(JsonSerializationVisitor $visitor)
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->image;
    }
}
