<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Payload.
 *
 * @author Anton Prokhorov
 */
class Payload
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("promo-code")
     */
    protected $promoCode;

    /**
     * @return string
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }


}
