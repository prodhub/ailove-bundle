<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class OrderItem.
 *
 * @author Anton Prokhorov
 */
class OrderItem
{
    /**
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Item")
     *
    **/
    protected $item;

    /**
     * @var int
     * @Serialized\Type("integer")
     */
    protected $pet;

    /**
     * OrderItem constructor.
     * @param $item
     * @param int $pet
     */
    public function __construct($item, $pet = null)
    {
        $this->item = $item;
        $this->pet = $pet;
    }


    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     * @return OrderItem
     */
    public function setItem($item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @return int
     */
    public function getPet()
    {
        return $this->pet;
    }

    /**
     * @param int $pet
     * @return OrderItem
     */
    public function setPet($pet)
    {
        $this->pet = $pet;
        return $this;
    }


}
