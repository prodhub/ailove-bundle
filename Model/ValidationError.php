<?php

namespace ADW\AiloveBundle\Model;

/**
 * Class ValidationError.
 *
 * @author Artur Vesker
 */
class ValidationError implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @var string
     */
    protected $serializedFieldName;

    /**
     * @var array
     */
    protected $messages;

    /**
     * @var ValidationError
     */
    protected $children;

    public function __construct($field, $serializedFieldName, array $messages)
    {
        $this->field = $field;
        $this->serializedFieldName = $serializedFieldName;
        $this->messages = $messages;
    }

    /**
     * @return string
     */
    public function getPropertyName()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getSerializedName()
    {
        return $this->serializedFieldName;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->field.': '.implode(', ', $this->messages);
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            $this->field => $this->messages,
        ];
    }
}
