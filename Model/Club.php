<?php

namespace ADW\AiloveBundle\Model;

/**
 * Class Club.
 *
 * @author Artur Vesker
 */
class Club
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $systemName;

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
