<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Country.
 *
 * @author Artur Vesker
 */
class Country
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $systemName;

    public function __construct($id, $name, $systemName)
    {
        $this->id = $id;
        $this->name = $name;
        $this->systemName = $systemName;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     *
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;
}
