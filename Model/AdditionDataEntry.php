<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class AdditionDataEntry
 *
 * @package ADW\AiloveBundle\Model
 * @author Artur Vesker
 */
class AdditionDataEntry
{

    /**
     * @var SimpleSystemObject
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\SimpleSystemObject")
     * @Serialized\Groups({"create"})
     */
    protected $field;

    /**
     * @var SimpleSystemObject[]
     *
     * @Serialized\Type("array<ADW\AiloveBundle\Model\SimpleSystemObject>")
     * @Serialized\Groups({"create"})
     */
    protected $values;

    /**
     * @param SimpleSystemObject $field
     * @param array $values
     */
    public function __construct(SimpleSystemObject $field = null, array $values = [])
    {
        $this->field = $field;
        $this->values = $values;
    }


    /**
     * @return SimpleSystemObject
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param SimpleSystemObject $field
     * @return self
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return SimpleSystemObject[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param SimpleSystemObject[] $values
     * @return self
     */
    public function setValues($values)
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function addValue($value)
    {
        if (!$value instanceof SimpleSystemObject) {
            $value = new SimpleSystemObject($value, $value);
        }

        $this->values[] = $value;

        return $this;
    }


}