<?php

namespace ADW\AiloveBundle\Model\Mailing;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Mail.
 *
 * @author Artur Vesker
 */
class Mail
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $title;

    /**
     * @var array
     *
     * @Serialized\Type("array<integer>")
     */
    protected $users;

    /**
     * @var Template
     */
    protected $template;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     */
    protected $sendDate;
}
