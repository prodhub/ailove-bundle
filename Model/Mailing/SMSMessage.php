<?php

namespace ADW\AiloveBundle\Model\Mailing;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SMSMessage.
 *
 * @author Artur Vesker
 */
class SMSMessage
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $phone;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $message;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $sender;

    /**
     * @param $phone
     * @param $message
     * @param string $sender
     */
    public function __construct($phone, $message, $sender = 'ProPlan')
    {
        $this->phone = $phone;
        $this->message = $message;
        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }
}
