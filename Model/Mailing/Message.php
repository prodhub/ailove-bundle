<?php

namespace ADW\AiloveBundle\Model\Mailing;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Message.
 *
 * @author Artur Vesker
 */
class Message
{
    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $email;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $subject;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $bodyHtml;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $bodyText;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $template;


    /**
     * @var array
     *
     * @Serialized\Type("array")
     */
    protected $context;

    public function __construct($email, $subject = null, $htmlBody = null, $textBody = null, $template = null, $context = null)
    {
        $this->email = $email;
        $this->subject = $subject;
        $this->bodyHtml = $htmlBody;
        $this->bodyText = $textBody;
        $this->template = $template;
        $this->context = $context;
    }

    /**
     * @param $email
     * @param $templateName
     * @return Message
     */
    public static function fromTemplate($email, $templateName)
    {
        return new self($email, null, null, null, $templateName, null);
    }

    /**
     * @param $email
     * @param $templateName
     * @param $context
     * @return Message
     */
    public static function fromTemplateWithContext($email, $templateName, $context)
    {
        return new self($email, null, null, null, $templateName, $context);
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->bodyText;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param array $context
     * @return Message
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

}
