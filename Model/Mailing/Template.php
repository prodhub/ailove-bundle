<?php

namespace ADW\AiloveBundle\Model\Mailing;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Template.
 *
 * @author Artur Vesker
 */
class Template
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("integer")
     */
    protected $systemName;

    /**
     * @var int
     */
    protected $author;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * @var string
     */
    protected $senderEmail;
}
