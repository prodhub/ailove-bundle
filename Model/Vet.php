<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Vet
 *
 * @author Artur Vesker
 */
class Vet extends User
{

    const PRACTICE_SURGERY = 'surgery';
    const PRACTICE_THERAPY = 'therapy';
    const PRACTICE_OPHTHALMOLOGY = 'ophthalmology';
    const PRACTICE_TRAUMATOLOGY = 'traumatology';
    const PRACTICE_ENDOCRINOLOGY = 'endocrinology';
    const PRACTICE_DENTISTRY = 'dentistry';
    const PRACTICE_GENERAL = 'general_practice';

    /**
     * @var VetData
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\VetData")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    protected $vet;

    /**
     * Create Vet
     */
    public function __construct()
    {
        parent::__construct(self::TYPE_VET);
        $this->vet = new VetData();
    }

    /**
     * @return int
     */
    public function getGraduationYear()
    {
        return $this->vet->graduationYear;
    }

    /**
     * @param int $graduationYear
     *
     * @return self
     */
    public function setGraduationYear($graduationYear)
    {
        $this->vet->graduationYear = $graduationYear;

        return $this;
    }

    /**
     * @return Institution
     */
    public function getInstitution()
    {
        return $this->vet->institution;
    }

    /**
     * @param Institution $institution
     *
     * @return self
     */
    public function setInstitution(Institution $institution)
    {
        $this->vet->institution = $institution;

        return $this;
    }

    /**
     * @return Institution
     */
    public function getClinic()
    {
        return $this->vet->clinic;
    }

    /**
     * @param Clinic $clinic
     *
     * @return self
     */
    public function setClinic(Clinic $clinic)
    {
        $this->vet->clinic = $clinic;

        return $this;
    }

    /**
     * @Serialized\PostDeserialize()
     */
    public function onPostDeserialize()
    {
        if (!$this->vet) {
            $this->vet = new VetData();
        }
    }
}