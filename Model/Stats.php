<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Stats.
 *
 * @author Artur Vesker
 */
class Stats
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $balance;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\SerializedName("place_league_table")
     */
    protected $ratingPosition;

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return int
     */
    public function getRatingPosition()
    {
        return $this->ratingPosition;
    }
}
