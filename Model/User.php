<?php

namespace ADW\AiloveBundle\Model;

use ADW\AiloveBundle\Collection\ContactsCollection;
use ADW\AiloveBundle\Model\Pet\Pet;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class AiloveUser.
 *
 * @author Artur Vesker
 */
class User
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    const TYPE_OWNER = 'owner';
    const TYPE_VET = 'vet';

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\Groups({"Default", "load", "short"})
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load", "short"})
     */
    protected $email;

    /**
     * @var string
     *
     * @Serialized\Groups({"Default", "create", "load", "update", "short"})
     * @Serialized\Type("string")
     */
    protected $phone;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load"})
     */
    protected $facebookId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load"})
     */
    protected $vkontakteId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load"})
     */
    protected $googleId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load"})
     */
    protected $twitterId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load"})
     */
    protected $odnoklassnikiId;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("firstname")
     * @Serialized\Groups({"Default", "create", "load", "update", "short"})
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("lastname")
     * @Serialized\Groups({"Default", "create", "load", "update", "short"})
     */
    protected $lastName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("middlename")
     * @Serialized\Groups({"Default", "create", "load", "update", "short"})
     * @Serialized\Accessor(getter="getMiddleName", setter="setMiddleName")
     */
    protected $middleName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "create", "load", "update", "short"})
     */
    protected $gender;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime<'Y-m-d'>")
     * @Serialized\SerializedName("birthday")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    protected $birthDay;

    /**
     * @var UserType
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\UserType")
     * @Serialized\Groups({"Default", "create", "load", "update"})
     */
    protected $type;

    /**
     * @var string
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\UploadImage")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $avatar;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     * @Serialized\Groups({"Default", "load"})
     */
    protected $registerDate;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     * @Serialized\Groups({"Default", "load"})
     */
    protected $lastLogin;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     * @Serialized\Groups({"Default", "load"})
     */
    protected $age;

    /**
     * @var ContactsCollection
     *
     * @Serialized\Type("ContactsCollection<ADW\AiloveBundle\Model\Contact>")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $contacts;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime")
     * @Serialized\Groups({"load"})
     */
    protected $emailChanged;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     * @Serialized\Groups({"load"})
     */
    protected $fullAnyAddress;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     * @Serialized\Groups({"create", "load"})
     */
    protected $processDataPermission;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $sendEmailPermission;

    /**
     * @var bool
     *
     * @Serialized\Type("boolean")
     * @Serialized\Groups({"create", "load", "update"})
     */
    protected $sendSmsPermission;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "load", "short"})
     */
    protected $emailVerified;

    /**
     * @var bool
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"Default", "load", "short"})
     */
    protected $phoneVerified;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create"})
     */
    protected $password1;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"create"})
     */
    protected $password2;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\Groups({"load", "update"})
     */
    protected $version;

    /**
     * @var Pet[]
     * @internal
     *
     * @Serialized\Exclude()
     */
    protected $_pets = [];


    /**
     * @var string
     *
     * @Serialized\Groups({"create"})
     * @Serialized\SerializedName("phone_confirm_key")
     */
    protected $phoneconfirmkey;

    /**
     * Create Vet.
     *
     * @return \ADW\AiloveBundle\Model\User
     */
    public static function createVet()
    {
        return new self(self::TYPE_VET);
    }

    /**
     * Create Owner.
     *
     * @param array $pets
     * @return User
     */
    public static function createOwner($pets = [])
    {
        return new self(self::TYPE_OWNER, $pets);
    }

    /**
     * User constructor.
     *
     * @param string $type
     * @param array $pets
     */
    public function __construct($type = self::TYPE_OWNER, $pets = [])
    {
        $this->type = new UserType($type, $type);
        $this->pets = $pets;
        $this->contacts = new ContactsCollection();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     *
     * @return self
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->middleName . ' ' . $this->lastName;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * @param \DateTime $birthDay
     *
     * @return self
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * @return UserType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param UserType|string $type
     *
     * @return self
     */
    public function setType($type)
    {
        if (is_string($type)) {
            $type = new UserType($type, $type);
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @param string $password2
     *
     * @return self
     */
    public function setPassword2($password2)
    {
        $this->password2 = $password2;

        return $this;
    }

    /**
     * @param string $password1
     *
     * @return self
     */
    public function setPassword1($password1)
    {
        $this->password1 = $password1;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     *
     * @return self
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return ContactsCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }


    public function getMainContacts()
    {
        return count($this->contacts) > 0 ? $this->contacts[0] : null;
    }

    /**
     * @return bool
     */
    public function hasProcessDataPermission()
    {
        return $this->processDataPermission;
    }

    /**
     * @return bool
     */
    public function hasSendEmailPermission()
    {
        return $this->sendEmailPermission;
    }

    /**
     * @return bool
     */
    public function hasSendSmsPermission()
    {
        return $this->sendSmsPermission;
    }

    /**
     * @param bool $processDataPermission
     *
     * @return self
     */
    public function setProcessDataPermission($processDataPermission)
    {
        $this->processDataPermission = $processDataPermission;

        return $this;
    }

    /**
     * @param bool $sendEmailPermission
     *
     * @return self
     */
    public function setSendEmailPermission($sendEmailPermission)
    {
        $this->sendEmailPermission = $sendEmailPermission;

        return $this;
    }

    /**
     * @param bool $sendSmsPermission
     *
     * @return self
     */
    public function setSendSmsPermission($sendSmsPermission)
    {
        $this->sendSmsPermission = $sendSmsPermission;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword1()
    {
        return $this->password1;
    }

    /**
     * @return string
     */
    public function getPassword2()
    {
        return $this->password2;
    }


    /**
     * @deprecated
     */
    public function setVet(VetData $vetData)
    {
        $this->vet = $vetData;
    }

    public function setPlainPassword($password)
    {
        $this->password1 = $password;
        $this->password2 = $password;

        return $this;
    }

    /**
     * @param $phone
     *
     * @return $this
     */
    public function addAnotherPhone($phone)
    {
        $contact = new Contact();
        $contact->setMobilePhone($phone);

        if (!$this->contacts) {
            $this->contacts = new ContactsCollection();
        }

        $this->contacts->add($contact);

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     * @return self
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * @param string $vkontakteId
     * @return self
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     * @return self
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitterId;
    }

    /**
     * @param string $twitterId
     * @return self
     */
    public function setTwitterId($twitterId)
    {
        $this->twitterId = $twitterId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOdnoklassnikiId()
    {
        return $this->odnoklassnikiId;
    }

    /**
     * @param string $odnoklassnikiId
     * @return self
     */
    public function setOdnoklassnikiId($odnoklassnikiId)
    {
        $this->odnoklassnikiId = $odnoklassnikiId;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isEmailVerified()
    {
        return $this->emailVerified === 'verified';
    }

    /**
     * @return boolean
     */
    public function isPhoneVerified()
    {
        return $this->phoneVerified === 'verified';
    }

    /**
     * @param ContactsCollection $contacts
     * @return $this
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return \ADW\AiloveBundle\Model\Contact|null
     */
    public function getDeliveryAddress()
    {
        return $this->getContacts()->getDeliveryContact();
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function setDeliveryAddress(Contact $contact)
    {
        $contact->setType(Contact::TYPE_DELIVERY);
        $this->getContacts()->save($contact);

        return $this;
    }

    /**
     * @return \ADW\AiloveBundle\Model\Contact|null
     * @throws \Exception
     */
    public function getMainAddress()
    {
        return $this->getContacts()->getMainContact();
    }

    /**
     * @param Contact $contact
     * @return $this
     * @throws \Exception
     */
    public function setMainAddress(Contact $contact)
    {
        $this->getContacts()->save($contact);

        return $this;
    }

    /**
     * @param Pet[] $pets
     * @return self
     * @internal ONLY FOR ATOMICITY IN REGISTRATION
     */
    public function setPets(array $pets)
    {
        $this->_pets = $pets;

        return $this;
    }

    /**
     * @internal
     * @return Pet[]
     * @Serialized\SerializedName("cats")
     * @Serialized\VirtualProperty()
     * @Serialized\Type("array<ADW\AiloveBundle\Model\Pet\Pet>")
     * @Serialized\Groups({"create"})
     */
    public function getCats()
    {
        $cats = [];

        foreach ($this->_pets as $pet) {
            if ($pet->getType() == Pet::TYPE_CAT) {
                $cats[] = $pet;
            }
        }

        return $cats;
    }

    /**
     * @internal
     * @return Pet[]
     * @Serialized\SerializedName("dogs")
     * @Serialized\VirtualProperty()
     * @Serialized\Type("array<ADW\AiloveBundle\Model\Pet\Pet>")
     * @Serialized\Groups({"create"})
     */
    public function getDogs()
    {
        $dogs = [];

        foreach ($this->_pets as $pet) {
            if ($pet->getType() == Pet::TYPE_DOG) {
                $dogs[] = $pet;
            }
        }

        return $dogs;
    }

    /**
     * @return string
     */
    public function getPhoneconfirmkey()
    {
        return $this->phoneconfirmkey;
    }

    /**
     * @param string $phoneconfirmkey
     * @return User
     */
    public function setPhoneconfirmkey($phoneconfirmkey)
    {
        $this->phoneconfirmkey = $phoneconfirmkey;
        return $this;
    }


}
