<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class SimpleSystemObject
 *
 * @package ADW\AiloveBundle\Model
 * @author Artur Vesker
 */
class SimpleSystemObject
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @param null|string $name
     * @param null|string $systemName
     */
    public function __construct($name = null, $systemName = null)
    {
        $this->name = $name;
        $this->systemName = $systemName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @param string $systemName
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    use OnlySystemNameForAiloveTrait;

}