<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Promo.
 *
 * @author Artur Vesker
 */
class Promo
{
    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var Action
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Action")
     */
    protected $actionType;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $environment;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $territory;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime<'Y-m-d'>")
     */
    protected $dateBegin;

    /**
     * @var \DateTime
     *
     * @Serialized\Type("DateTime<'Y-m-d'>")
     */
    protected $dateEnd;

    /**
     * @var string
     *
     * @Serialized\Type("integer")
     */
    protected $brand;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * @return string
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @return Action
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        $now = time();

        return ($now > $this->dateBegin->getTimestamp()) && ($now < $this->dateEnd->getTimestamp());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->systemName;
    }

    use OnlySystemNameForAiloveTrait;
}
