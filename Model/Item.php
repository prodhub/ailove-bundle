<?php

namespace ADW\AiloveBundle\Model;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class Item.
 *
 * @author Anton Prokhorov
 */
class Item
{

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $id;


    /**
     * @var \ADW\AiloveBundle\Model\Product
     *
     * @Serialized\Type("ADW\AiloveBundle\Model\Product")
     */

    protected $product;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $amountLeft;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Serialized\Type("string")
     */
    protected $systemName;

    /**
     * @var int
     *
     * @Serialized\Type("integer")
     */
    protected $amount;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmountLeft()
    {
        return $this->amountLeft;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return self
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @param string $systemName
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;
        return $this;
    }

    use OnlySystemNameForAiloveTrait;
}
