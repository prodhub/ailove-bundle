<?php

namespace ADW\AiloveBundle\RestClient;

/**
 * Interface CustomResponseHandlerInterface
 *
 * @package ADW\AiloveBundle\RestClient
 */
interface CustomResponseHandlerInterface
{

    public function getCustomHandler();

}