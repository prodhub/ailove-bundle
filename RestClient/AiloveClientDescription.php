<?php

namespace ADW\AiloveBundle\RestClient;

use ADW\AiloveBundle\Exception\CrmUnavailableException;
use ADW\AiloveBundle\Exception\Handler\ExceptionHandlerInterface;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use GuzzleHttp\Exception\ServerException;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ADW\RestClientBundle\Client\ClientDescriptionInterface;
use ADW\RestClientBundle\Event\ExceptionEvent;
use ADW\RestClientBundle\Event\RequestEvent;

/**
 * Class AiloveClientDescription.
 *
 * @author Artur Vesker
 */
class AiloveClientDescription implements ClientDescriptionInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var ExceptionHandlerInterface[]
     */
    protected $exceptionHandlers = [];

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $schema;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param Serializer            $serializer
     * @param $host
     * @param $key
     * @param $schema
     */
    public function __construct(TokenStorageInterface $tokenStorage, Serializer $serializer, $host, $key, $schema)
    {
        $this->tokenStorage = $tokenStorage;
        $this->serializer = $serializer;
        $this->host = $host;
        $this->key = $key;
        $this->schema = $schema;
    }

    /**
     * @param ExceptionHandlerInterface $exceptionHandler
     */
    public function addExceptionHandler(ExceptionHandlerInterface $exceptionHandler)
    {
        $this->exceptionHandlers[] = $exceptionHandler;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => [$this, 'setHeaders'],
            ],
            [
                'event' => ExceptionEvent::NAME,
                'listener' => function (ExceptionEvent $event) {
                    $exception = $event->getException();

                    foreach ($this->exceptionHandlers as $handler) {
                        $handler->handle($exception, $event->getMethodDescription(), $event->getOptions());
                    }

                    if ($exception instanceof ServerException) {
                        throw new CrmUnavailableException($exception->getMessage(), $exception->getCode());
                    }

                    throw $exception;
                },
            ],
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function setHeaders(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($token = $this->tokenStorage->getToken()) {
            if ($token instanceof AiloveToken) {
                $request = $request->withHeader('Authorization', 'Token '.$token->getKey());
            }
        }

        $request = $request
            ->withHeader('Accept-Language', 'ru')
            ->withHeader('X-Partner-Key', $this->key);

        $descripion = $event->getMethodDescription();

        /*
         * У айлова не настроена политика кеширования, потому настраиваем ее сами. Этот заголовок будет обработан CacheCrutch мидлваром
         */
        if ($descripion instanceof CachingDescriptionInterface) {
            $request = $request->withHeader('Cache-Control', 'max-age='.$descripion->getCacheTtl());
        }

        $event->setRequest($request);
    }

    /**
     * @return callable
     */
    public function getSerializer()
    {
        return function ($data, $format, array $context) {

            $jmsContext = SerializationContext::create();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('ailove', true);

            return $this->serializer->serialize($data, 'ailove_json', $jmsContext);
        };
    }

    /**
     * @return callable
     */
    public function getDeserializer()
    {
        return function ($description, $data, $format, $model, array $context) {

            if ($description instanceof CustomResponseHandlerInterface) {
                $handler = $description->getCustomHandler();

                return $handler($this->serializer, $description, $data, $format, $model, $context);
            }

            $jmsContext = DeserializationContext::create();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('_context', clone $jmsContext);

            return $this->serializer->fromArray(json_decode($data, true)['data'], $model, $jmsContext);
        };
    }

}
