<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListUserTypesMethodDescription.
 *
 * @author Artur Vesker
 */
class ListUserTypesMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/types/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\UserType>';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_WEEK;
    }
}
