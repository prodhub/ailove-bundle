<?php


namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GetUserOrdersReceiptsMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class GetUserOrdersReceiptsMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
//        return 'ADW\AiloveBundle\Model\Promo\Question';
        return 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'promo' => 'string'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/orders/receipts';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'promo' => $options['promo']
        ];
    }

}