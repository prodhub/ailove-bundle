<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Option;

/**
 * Class ActivateCodeMethodDescription.
 *
 * @author Artur Vesker
 */
class ActivateCodeMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'code' => ['string', 'null'],
            'selector' => new Option('string'),
            'check_only' => new Option('bool', false, false),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/promos/codes/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\User\Promo\UserPromoCode';
    }


}
