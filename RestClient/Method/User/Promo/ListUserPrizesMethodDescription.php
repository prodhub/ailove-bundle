<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListUserPrizesMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\User\Promo
 * @author Artur Vesker
 */
class ListUserPrizesMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'uid' => 'int'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/user/{uid}/promos/prizes/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\PrizeWinner>';
    }


}