<?php


namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class UserOrdersReceiptsMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class UserOrdersReceiptsMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Promo\Question';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'receipt' => 'ADW\AiloveBundle\Model\Promo\Receipt'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/orders/receipts';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'promo' => $options['receipt']->getPromo(),
            'name' => $options['receipt']->getName(),
            'cleared_at' => $options['receipt']->getClearedAt()
        ];
    }

}