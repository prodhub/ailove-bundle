<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GetPrizeFelixMethodDescription.
 *
 * @author Anton Prokhorov
 */
class GetPrizeFelixMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Promo\FelixPromo';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'promo' => 'string'
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return [
            'promo' => $options['promo']
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/tmp/felixpromo01stats/';
    }


}
