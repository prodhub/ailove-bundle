<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class UserOrdersMethodDescription.
 *
 * @author Anton Prokhorov
 */
class UserOrdersMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Order';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'order' => 'ADW\AiloveBundle\Model\Order'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/orders/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'items' => $options['order']->getItems(),
            'contact' => $options['order']->getContact(),
            'order_type' => $options['order']->getOrderType()
        ];
    }
}
