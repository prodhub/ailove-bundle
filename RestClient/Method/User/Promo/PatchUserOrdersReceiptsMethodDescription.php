<?php


namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class PatchUserOrdersReceiptsMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class PatchUserOrdersReceiptsMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
//        return 'ADW\AiloveBundle\Model\Promo\Question';
        return 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'receipt_id' => 'integer',
            'user' => 'integer',
            'prize' => 'integer',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'PATCH';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/orders/receipts/{receipt_id}';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'user' => $options['user'],
            'prize' => $options['prize']
        ];
    }

}