<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GivePrizeFelixMethodDescription.
 *
 * @author Anton Prokhorov
 */
class GivePrizeFelixMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\PrizeWinner';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'promo' => 'string',
            'prize' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/tmp/felixpromo01prizes/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return ['user'=> $options['uid'], 'promo' => $options['promo'],'prize' => $options['prize']];
    }
}
