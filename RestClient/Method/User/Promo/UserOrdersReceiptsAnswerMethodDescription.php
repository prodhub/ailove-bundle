<?php


namespace ADW\AiloveBundle\RestClient\Method\User\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class UserOrdersReceiptsAnswerMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class UserOrdersReceiptsAnswerMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
//        return 'ADW\AiloveBundle\Model\Promo\Question';
        return 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'answer' => 'integer'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/orders/receipts/answer';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'answer' => $options['answer']
        ];
    }

}