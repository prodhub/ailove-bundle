<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Action;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class DoActionMethodDescription.
 *
 * @author Artur Vesker
 */
class DoActionMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'action' => 'string',
            'promo' => ['string', 'null'],
            'selector' => ['array', 'null'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/actions/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return array_filter([
            'action' => $options['action'],
            'promo' => $options['promo'],
            'selector' => $options['selector'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\UserAction';
    }
}
