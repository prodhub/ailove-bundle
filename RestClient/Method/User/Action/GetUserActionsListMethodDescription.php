<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Action;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GetUserActionsListMethodDescription.
 *
 * @author Artur Vesker
 */
class GetUserActionsListMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/actions/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\UserAction>';
    }
}
