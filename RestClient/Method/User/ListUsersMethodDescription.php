<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Option;

/**
 * Class ListUsersMethodDescription.
 *
 * @author Artur Vesker
 */
class ListUsersMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'search' => new Option(['string', 'null'], false, null),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\User>';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataContext()
    {
        return ['groups' => ['load']];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/';
    }

}
