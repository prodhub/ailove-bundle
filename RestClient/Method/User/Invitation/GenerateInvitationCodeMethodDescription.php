<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Invitation;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Option;

/**
 * Class GenerateInvitationCodeDescription.
 *
 * @author Artur Vesker
 */
class GenerateInvitationCodeMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\User\Invitation\InvitationCode';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'program' => 'string',
            'additional' => new Option('array', false, []),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/invitations/invites/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        $result = [
            'program' => $options['program'],
        ];

        if ($options['additional']) {
            $result['additional_data'] = json_encode($options['additional']);
        }

        return $result;
    }
}
