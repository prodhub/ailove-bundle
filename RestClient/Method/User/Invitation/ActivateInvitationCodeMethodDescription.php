<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Invitation;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Option;

/**
 * Class ActivateInvitationCodeMethodDescription.
 *
 * @author Artur Vesker
 */
class ActivateInvitationCodeMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\InvitationCode';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'code' => 'string',
            'check_only' => new Option('bool'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/invitations/codes/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }
}
