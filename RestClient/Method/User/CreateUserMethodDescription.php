<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class CreateUserMethodDescription.
 *
 * @author Artur Vesker
 */
class CreateUserMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'user' => 'ADW\AiloveBundle\Model\User',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options['user'];
    }

    /**
     * @return array
     */
    public function getRequestDataContext()
    {
        return ['groups' => ['create']];
    }

    /**
     * @return string
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\User';
    }

    /**
     * @return array
     */
    public function getResponseDataContext()
    {
        return ['groups' => ['load']];
    }
}
