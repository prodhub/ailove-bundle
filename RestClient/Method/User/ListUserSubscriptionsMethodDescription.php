<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListUserSubscriptionsMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\User
 * @author Artur Vesker
 */
class ListUserSubscriptionsMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'uid' => 'int'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/user/{uid}/mailer/subscriptions/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'array';
    }

}