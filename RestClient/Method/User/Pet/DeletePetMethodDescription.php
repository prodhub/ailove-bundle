<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Pet;

use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class DeletePetMethodDescription.
 *
 * @author Artur Vesker
 */
class DeletePetMethodDescription implements MethodDescriptionInterface
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'id' => 'int',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'DELETE';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/user/{uid}/pets/{id}';
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataFormat()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getRequestDataFormat()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getRequestDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [];
    }

}
