<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Pet;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class PutPetMethodDescription.
 *
 * @author Artur Vesker
 */
class PutPetMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'id' => 'integer',
            'pet' => 'ADW\AiloveBundle\Model\Pet\Pet',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Pet\Pet';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'PUT';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/user/{uid}/pets/{id}';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options['pet'];
    }
}
