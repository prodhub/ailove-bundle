<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Pet;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class CreatePetMethodDescription.
 *
 * @author Artur Vesker
 */
class CreatePetMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Pet\Pet';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'integer',
            'pet' => 'ADW\AiloveBundle\Model\Pet\Pet',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/pets/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options['pet'];
    }
}
