<?php

namespace ADW\AiloveBundle\RestClient\Method\User\Pet;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GetPetMethodDescription.
 *
 * @author Artur Vesker
 */
class GetPetMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'id' => 'int',
            'uid' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Pet\Pet';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/pets/{id}';
    }
}
