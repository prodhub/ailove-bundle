<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class PutUserMethodDescription.
 *
 * @author Artur Vesker
 */
class PutUserMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return [
            'user' => 'ADW\AiloveBundle\Model\User',
            'id' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{id}';
    }

    /**
     * {@inheritdoc}
     */
    public function getData(array $options)
    {
        return $options['user'];
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'id' => 'int',
            'user' => 'ADW\AiloveBundle\Model\User',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'PUT';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options['user'];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataContext()
    {
        return ['groups' => ['update']];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveUser';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataContext()
    {
        return ['groups' => ['load']];
    }
}
