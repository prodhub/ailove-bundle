<?php

namespace ADW\AiloveBundle\RestClient\Method\User;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class DeleteUserMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\User
 * @author Artur Vesker
 */
class DeleteUserMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'id' => 'integer'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{id}';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'DELETE';
    }

}