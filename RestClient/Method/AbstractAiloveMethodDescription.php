<?php

namespace ADW\AiloveBundle\RestClient\Method;

use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class AbstractAiloveMethodDescription.
 *
 * @author Artur Vesker
 */
abstract class AbstractAiloveMethodDescription implements MethodDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_JSON;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataContext()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_JSON;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'array';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataContext()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [];
    }


}
