<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ChangeEmailMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\Auth
 * @author Artur Vesker
 */
class ChangeEmailMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'email' => 'string'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/auth/email/change/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Message\SuccessMessage';
    }

}