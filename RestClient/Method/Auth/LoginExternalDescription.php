<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use Symfony\Component\OptionsResolver\OptionsResolver;
use ADW\RestClientBundle\Description\AbstractPostMethodDescription;

/**
 * Class LoginExternalDescription.
 *
 * @author Artur Vesker
 */
class LoginExternalDescription extends AbstractPostMethodDescription
{
    const PROVIDER_ODNOKLASSNIKI = 'ok';
    const PROVIDER_VKONTAKTE = 'vk';
    const PROVIDER_FACEBOOK = 'fb';
    const PROVIDER_TWITTER = 'tw';
    const PROVIDER_GOOGLE = 'gp';

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return [
            'provider' => 'string',
            'id' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setAllowedValues('provider', [
                self::PROVIDER_ODNOKLASSNIKI,
                self::PROVIDER_VKONTAKTE,
                self::PROVIDER_FACEBOOK,
                self::PROVIDER_TWITTER,
                self::PROVIDER_GOOGLE,
            ]);
        parent::configureOptions($resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function getResource(array $options)
    {
        return '/api/auth/login-external/';
    }

    /**
     * {@inheritdoc}
     */
    public function getBody(array $options)
    {
        return [
            'social_type' => $options['provider'],
            'social_id' => $options['id'],
        ];
    }
}
