<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Event\RequestEvent;

/**
 * Class GetCurrentUserMethodDescription.
 *
 * @author Artur Vesker
 */
class GetCurrentUserMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'key' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/me/';
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => function (RequestEvent $event) {
                    $key = $event->getOptions()['key'];

                    $event->setRequest($event->getRequest()->withHeader('Authorization', "Token {$key}"));
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveUser';
    }
}
