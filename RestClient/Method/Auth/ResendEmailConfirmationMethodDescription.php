<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ResendEmailConfirmationMethodDescription.
 *
 * @author Artur Vesker
 */
class ResendEmailConfirmationMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/email/change/resend/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return [
            'user' => $options['uid'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Message\SuccessMessage';
    }
}
