<?php


namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\Model\ConfirmKey;

/**
 * Class CheckConfirmPhoneMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class CheckConfirmPhoneMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/phone/check-confirm/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'hashkey' => 'string',
            'token' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return ConfirmKey::class;
    }
}