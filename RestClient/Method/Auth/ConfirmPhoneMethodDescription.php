<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ConfirmPhoneMethodDescription.
 *
 * @author Artur Vesker
 */
class ConfirmPhoneMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/phone/confirm/';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'key' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Message\SuccessMessage';
    }
}
