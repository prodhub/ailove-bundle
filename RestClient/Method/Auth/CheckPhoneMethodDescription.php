<?php


namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\Model\PhoneCheck;


/**
 * Class CheckPhoneMethodDescription.
 * Project pyaterochka-backend.
 * @author Anton Prokhorov
 */

class CheckPhoneMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/phone/check/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'phone' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return PhoneCheck::class;
    }

}