<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ConfirmResetPasswordMethodDescription.
 *
 * @author Artur Vesker
 */
class ConfirmResetPasswordMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'uid' => 'string',
            'token' => 'string',
            'new_password1' => 'string',
            'new_password2' => 'string',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/auth/password/reset/confirm/';
    }

    /**
     * @inheritdoc
     */
    public function getModel()
    {
        return 'ADW\AiloveBundle\Model\Message\SuccessMessage';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options;
    }
}
