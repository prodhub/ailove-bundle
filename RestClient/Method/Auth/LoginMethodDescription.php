<?php

namespace ADW\AiloveBundle\RestClient\Method\Auth;

use ADW\AiloveBundle\RestClient\CustomResponseHandlerInterface;
use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Serializer;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;
use ADW\RestClientBundle\Event\RequestEvent;

/**
 * Class LoginMethodDescription.
 *
 * @author Artur Vesker
 */
class LoginMethodDescription extends AbstractAiloveMethodDescription implements CustomResponseHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'username' => 'string',
            'password' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/auth/login/';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveUser';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => function(RequestEvent $event) {
                    $event->setRequest($event->getRequest()->withoutHeader('Authorization'));
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getCustomHandler()
    {
        return function (Serializer $serializer, MethodDescriptionInterface $description, $data, $format, $model, array $context) {
            $data = json_decode($data, true)['data'];

            $token =  new AiloveToken($data['token']['key'], $data['token']['expire']);

            return [
                'token' => $token,
                'user' => $serializer->fromArray($data, 'ADW\AiloveBundle\Model\User', DeserializationContext::create()->setGroups('load'))
            ];
        };
    }

}
