<?php

namespace ADW\AiloveBundle\RestClient\Method\Captcha;

use Symfony\Component\OptionsResolver\OptionsResolver;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;
use ADW\RestClientBundle\MethodContext;

/**
 * Class GetCaptchaMethodDescription.
 *
 * @author Artur Vesker
 */
class GetCaptchaMethodDescription implements MethodDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function configureMethod(MethodContext $context, array $options)
    {
        $context
            ->setResponseDataModel('ADW\AiloveBundle\CRM\Response<ADW\AiloveBundle\Model\Captcha>')
            ->getRequestBuilder()
                ->setMethod('GET')
                ->setResource('/api/captcha/captcha/');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        // TODO: Implement configureOptions() method.
    }
}
