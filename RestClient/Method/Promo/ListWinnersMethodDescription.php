<?php

namespace ADW\AiloveBundle\RestClient\Method\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListWinnersMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\Promo
 * @author Artur Vesker
 */
class ListWinnersMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'promo_name' => ['null', 'string'],
            'prize_name' => ['null', 'string'],
            'won_lte' => ['null', 'string'],
            'won_gte' => ['null', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/promo/winners/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        return $options;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\PrizeWinner>';
    }

}