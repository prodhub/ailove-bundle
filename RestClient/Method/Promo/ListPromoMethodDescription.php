<?php

namespace ADW\AiloveBundle\RestClient\Method\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListPromoMethodDescription.
 *
 * @author Artur Vesker
 */
class ListPromoMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/promo/';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Promo>';
    }

}
