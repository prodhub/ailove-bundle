<?php

namespace ADW\AiloveBundle\RestClient\Method\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListPrizesMethodDescription.
 *
 * @author Artur Vesker
 */
class ListPrizesMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/promo/prizes/';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Prize>';
    }

}
