<?php

namespace ADW\AiloveBundle\RestClient\Method\Promo;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ListItemMethodDescription.
 *
 * @author Anton Prokhorov
 */
class ListItemMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/tmp/orders/items/';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Item>';
    }

}
