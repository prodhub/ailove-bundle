<?php

namespace ADW\AiloveBundle\RestClient\Method\Upload;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class UploadImageMethodDescription.
 *
 * @author Artur Vesker
 */
class UploadImageMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'image' => 'Symfony\Component\HttpFoundation\File\File',
            'upload_type' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/upload/images/';
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\UploadImage';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_MULTIPART_FORM_DATA;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }
}
