<?php

namespace ADW\AiloveBundle\RestClient\Method\PointSystem;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\RestClientBundle\Option;

/**
 * Class GetBalanceMethodDescription.
 *
 * @author Artur Vesker
 */
class GetBalanceMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Stats';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'uid' => 'int',
            'promo' => new Option('string'),
            'selector' => new Option('string'),
            'with_system' => new Option('bool'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/user/{uid}/pointsystem/balance/';
    }
}
