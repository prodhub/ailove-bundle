<?php

namespace ADW\AiloveBundle\RestClient\Method\PointSystem;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class GetRatingMethodDescription
 *
 * @package ADW\AiloveBundle\RestClient\Method\PointSystem
 * @author Artur Vesker
 */
class GetRatingMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'promo' => ['null', 'string'],
            'selector' => ['null', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/pointsystem/rating/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\User\UserWithRating>';
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataContext()
    {
        return ['groups' => ['load']];
    }

    /**
     * @inheritdoc
     */
    public function getQuery(array $options)
    {
        return $options;
    }


}