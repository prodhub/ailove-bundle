<?php

namespace ADW\AiloveBundle\RestClient\Method\Subscribe;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class SubscribeMethodDescription.
 *
 * @author Artur Vesker
 */
class SubscribeMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'email' => 'string',
            'subscription' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/mailings/mail/lists/subscribe/';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options;
    }
}
