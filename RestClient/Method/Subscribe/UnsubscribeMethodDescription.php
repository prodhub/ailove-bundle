<?php

namespace ADW\AiloveBundle\RestClient\Method\Subscribe;

use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class UnsubscribeMethodDescription.
 *
 * @author Artur Vesker
 */
class UnsubscribeMethodDescription implements MethodDescriptionInterface
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'email' => 'string',
            'subscription' => 'int',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/mailings/mail/lists/unsubscribe/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'DELETE';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_JSON;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataFormat()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataModel()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getResponseDataContext()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getSubscribedEvents()
    {
        return [];
    }

}
