<?php

namespace ADW\AiloveBundle\RestClient\Method\FIAS;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class FIASSearchMethodDescription.
 *
 * @author Artur Vesker
 */
class FIASSearchMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'query' => 'string',
            'level' => 'int',
            'limit' => 'int',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/fias/search/';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return [
            'q' => $options['query'],
            'level' => $options['level'],
            'limit' => $options['limit'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveFiasBundle\FIASAddress>';
    }
}
