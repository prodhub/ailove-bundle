<?php

namespace ADW\AiloveBundle\RestClient\Method\Mailings;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class SendEmailMethodDescription.
 *
 * @author Artur Vesker
 */
class SendEmailMethodDescription extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'message' => 'ADW\AiloveBundle\Model\Mailing\Message'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/mailings/mail/send/';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(array $options)
    {
        return $options['message'];
    }
}
