<?php

namespace ADW\AiloveBundle\RestClient\Method\Collection\Clinic;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListClinicsMethodDescription.
 *
 * @author Artur Vesker
 */
class ListClinicsMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/collection/clinics/';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Clinic>';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_DAY;
    }
}
