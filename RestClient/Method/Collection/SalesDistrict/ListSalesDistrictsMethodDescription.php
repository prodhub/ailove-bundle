<?php

namespace ADW\AiloveBundle\RestClient\Method\Collection\SalesDistrict;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListSalesDistrictsMethodDescription.
 *
 * @author Artur Vesker
 */
class ListSalesDistrictsMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'search' => 'string',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/collection/salesdistrict/';
    }

    /**
     * {@inheritdoc}
     */
    public function getQuery(array $options)
    {
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_DAY;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\SalesDistrict>';
    }
}
