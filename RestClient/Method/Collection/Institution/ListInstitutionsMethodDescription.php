<?php

namespace ADW\AiloveBundle\RestClient\Method\Collection\Institution;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListInstitutionsMethodDescription.
 *
 * @author Artur Vesker
 */
class ListInstitutionsMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/collection/institutions/';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_DAY;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Institution>';
    }
}
