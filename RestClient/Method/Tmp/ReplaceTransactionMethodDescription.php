<?php

namespace ADW\AiloveBundle\RestClient\Method\Tmp;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;

/**
 * Class ReplaceTransactionMethodDescription
 *
 * @author Artur Vesker
 */
class ReplaceTransactionMethodDescription extends AbstractAiloveMethodDescription
{

    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'customer' => 'integer',
            'promo' => 'string',
            'new_selector' => 'string',
            'old_selector' => 'string'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getResource()
    {
        return '/api/tmp/transaction/replace/';
    }

    /**
     * @inheritdoc
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @inheritdoc
     */
    public function getRequestData(array $options)
    {
        return $options;
    }

}