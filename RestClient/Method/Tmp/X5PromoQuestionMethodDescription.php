<?php


namespace ADW\AiloveBundle\RestClient\Method\Tmp;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\Model\Promo\X5Qestion;

/**
 * Class X5PromoQuestionMethodDescription.
 * Project ailove-bundle.
 * @author Anton Prokhorov
 */

class X5PromoQuestionMethodDescription  extends AbstractAiloveMethodDescription
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'ADW\AiloveBundle\Model\Promo\X5Qestion';
    }

    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return [
            'question_id' => 'integer'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/tmp/x5promo/{question_id}';
    }

}