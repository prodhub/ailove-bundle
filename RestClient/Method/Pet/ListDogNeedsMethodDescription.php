<?php

namespace ADW\AiloveBundle\RestClient\Method\Pet;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListDogNeedsMethodDescription.
 *
 * @author Artur Vesker
 */
class ListDogNeedsMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Pet\Need>';
    }

    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/pet/dog/needs/';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_DAY;
    }
}
