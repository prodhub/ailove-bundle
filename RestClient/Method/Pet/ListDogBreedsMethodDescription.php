<?php

namespace ADW\AiloveBundle\RestClient\Method\Pet;

use ADW\AiloveBundle\RestClient\Method\AbstractAiloveMethodDescription;
use ADW\AiloveBundle\RestClient\CachingDescriptionInterface;

/**
 * Class ListDogBreedsMethodDescription.
 *
 * @author Artur Vesker
 */
class ListDogBreedsMethodDescription extends AbstractAiloveMethodDescription implements CachingDescriptionInterface
{
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return 'GET';
    }

    /**
     * {@inheritdoc}
     */
    public function getResponseDataModel()
    {
        return 'AiloveLazyCollection<ADW\AiloveBundle\Model\Pet\Breed>';
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return '/api/pet/dog/breeds/';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl()
    {
        return CachingDescriptionInterface::TTL_DAY;
    }
}
