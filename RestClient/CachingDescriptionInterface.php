<?php

namespace ADW\AiloveBundle\RestClient;

/**
 * Interface CachingDescriptionInterface.
 *
 * @author Artur Vesker
 */
interface CachingDescriptionInterface
{
    const TTL_DAY = 86400;
    const TTL_HOUR = 3600;
    const TTL_WEEK = 604800;

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl();
}
