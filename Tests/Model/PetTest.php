<?php

namespace ADW\AiloveBundle\Tests\Model;

use ADW\AiloveBundle\Model\Pet\Pet;

/**
 * Class PetTest
 *
 * @author Artur Vesker
 */
class PetTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Birthday to age conversion
     */
    public function testBirthDayToAgeConversion()
    {
        $pet = Pet::createDog();

        $day = new \DateTime();
        $day->modify('- 16 months');
        $pet->setBirthDay($day);
        $this->assertEquals(4, $pet->getMonth());
        $this->assertEquals(1, $pet->getYear());

        $convertedDay = $pet->getBirthDay();

        $now = new \DateTime();

        $interval = $now->diff($convertedDay);
        $this->assertEquals(16, $interval->m + (12 * $interval->y));
    }

}