<?php

namespace ADW\AiloveBundle\Tests;

use ADW\AiloveBundle\Security\AiloveSecurityUtils;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\BrowserKit\Cookie;

/**
 * Class AiloveTestAuthenticationTrait
 *
 * @author Artur Vesker
 */
trait AiloveTestAuthenticationTrait
{

    /**
     * @param Client $client
     * @param $token
     */
    public function authenticateViaToken(Client $client, $token)
    {
        $cookie = new Cookie('TOKEN', $token);

/*        $client->getContainer()->get('security.token_storage')->setToken(AiloveSecurityUtils::extractToken($token));*/

        $client->getCookieJar()->set($cookie);
    }

}