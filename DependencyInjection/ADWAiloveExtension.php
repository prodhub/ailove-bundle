<?php

namespace ADW\AiloveBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use ADW\GuzzleBundle\DependencyInjection\GuzzleDependencyInjectionTrait;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ADWAiloveExtension extends Extension
{

    use GuzzleDependencyInjectionTrait;

    const GUZZLE_ID = 'ailove.guzzle';
    const CLIENT_ID = 'ailove_client';

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        //set config
        foreach ($config as $key => $value) {
            $container->setParameter('ailove.config.' . $key, $value);
        }

        $config['guzzle_options'] = [
            'headers' => [
                'User-Agent' => 'ADW'
            ]
        ];

        //set guzzle
        if ($config['guzzle']) {
            $container->setAlias(self::GUZZLE_ID, $config['guzzle']);
        } else {
            $stack = [];

            //configure cache
            if ($config['cache']) {
                $this->buildCacheHandler($container, $config['cache']);
                $stack[] = new Reference('ailove.cache_middleware');
                $stack[] = new Reference('ailove.cache_crutch_middleware');
            }

            $logger = $this->buildLogger($container, 'ailove', 'ailove', '>>> {request} <<< {response}');

            $guzzle = $this->buildClient($container, 'ailove', null, $stack, $config['guzzle_options'], true, $logger);
            $container->setAlias(self::GUZZLE_ID, $guzzle);
        }

        $container->setDefinition(self::CLIENT_ID, new Definition('ADW\RestClientBundle\Client\Client', [
            new Reference(self::GUZZLE_ID),
            new Reference('ailove.client_description')
        ]));

        $container->setParameter('ailove.config.user_decorator_class', $config['user_decorator_class']);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $loader->load('services.yml');
        $loader->load('form.yml');
        $loader->load('security.yml');
        $loader->load('serializer.yml');
        $loader->load('repository.yml');
    }

    /**
     * @param ContainerBuilder $container
     * @param $cacheProvider
     */
    protected function buildCacheHandler(ContainerBuilder $container, $cacheProvider)
    {
        $container->setDefinition('ailove.cache_storage', new Definition('Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage', [new Reference('doctrine_cache.providers.' . $cacheProvider)]));
        $container->setDefinition('ailove.cache_strategy', new Definition('Kevinrob\GuzzleCache\Strategy\PrivateCacheStrategy', [new Reference('ailove.cache_storage')]));
        $container->setDefinition('ailove.cache_middleware', new Definition('Kevinrob\GuzzleCache\CacheMiddleware', [new Reference('ailove.cache_strategy')]));
        $container->setDefinition('ailove.cache_crutch_middleware', new Definition('ADW\AiloveBundle\Middleware\CacheCrutch'));
    }

}
