<?php

namespace ADW\AiloveBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('adw_ailove');

        $rootNode
            ->normalizeKeys(false)
            ->children()
                ->scalarNode('guzzle')->defaultNull()->end()
                ->arrayNode('guzzle_options')
                    ->normalizeKeys(false)
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('social')
                    ->children()
                        ->scalarNode('register_route')->isRequired()->end()
                        ->scalarNode('add_social_network_route')->defaultFalse()->end()
                ->end()
                ->end()
                ->arrayNode('pet')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('repository_class')->defaultValue('ADW\AiloveBundle\Repository\PetRepository')->end()
                    ->end()
                ->end()
                ->scalarNode('host')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('schema')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('cache')->defaultNull()->end()
                ->scalarNode('partner')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('partner_key')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('send_email_verification')->defaultTrue()->end()
                ->scalarNode('send_sms_verification')->defaultTrue()->end()
                ->scalarNode('base_path')->end()
                ->scalarNode('user_decorator_class')->isRequired()->end()
                ->scalarNode('media_class')->defaultValue('Application\Sonata\MediaBundle\Entity\Media')->end()
                ->arrayNode('promo')
                    ->normalizeKeys(false)
                    ->prototype('array')
                        ->children()
                            ->arrayNode('join_via_code')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('actions')
                    ->normalizeKeys(false)
                    ->prototype('scalar')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
