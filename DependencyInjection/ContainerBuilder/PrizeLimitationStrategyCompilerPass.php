<?php

namespace ADW\AiloveBundle\DependencyInjection\ContainerBuilder;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class PrizeLimitationStrategyCompilerPass
 *
 * @package ADW\AiloveBundle\DependencyInjection\ContainerBuilder
 * @author Artur Vesker
 */
class PrizeLimitationStrategyCompilerPass implements CompilerPassInterface
{

    const TAG = 'ailove.prize_limitation_strategy';

    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $strategies = $container->findTaggedServiceIds(self::TAG);

        $references = [];

        foreach ($strategies as $id => $strategyTags) {
            foreach ($strategyTags as $tag) {
                $references[$tag['alias']] = new Reference($id);
            }
        }

        $container->getDefinition('ailove.validator.unique.prize_validator')->addArgument($references);
    }



}