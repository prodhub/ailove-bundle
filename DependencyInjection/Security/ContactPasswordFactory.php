<?php

namespace ADW\AiloveBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ContactPasswordFactory.
 *
 * @author Artur Vesker
 */
class ContactPasswordFactory extends AbstractFactory
{

    /**
     * {@inheritdoc}
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $providerId = 'ailove.security.contact_password_authentication_provider.'.$id;
        $provider = new DefinitionDecorator('ailove.security.contact_password_authentication_provider');
        $provider->addArgument($id);

        $container
            ->setDefinition($providerId, $provider);

        return $providerId;
    }

    /**
     * {@inheritdoc}
     */
    public function getListenerId()
    {
        return 'ailove.security.contact_password_authentication_listener';
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'form';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'ailove_form';
    }

    /**
     * {@inheritdoc}
     */
    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
        $entryPointId = 'security.authentication.form_entry_point.'.$id;
        $container
            ->setDefinition($entryPointId, new DefinitionDecorator('security.authentication.form_entry_point'))
            ->addArgument(new Reference('security.http_utils'))
            ->addArgument($config['login_path'])
            ->addArgument($config['use_forward'])
        ;

        return $entryPointId;
    }
}
