<?php

namespace ADW\AiloveBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

/**
 * Class QueryTokenFactory.
 *
 * @author Artur Vesker
 */
class QueryTokenFactory implements SecurityFactoryInterface
{

    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $template = 'ailove.security.query_token_authentication_listener';
        $listenerDefinition = new DefinitionDecorator($template);
        $listenerDefinition
            ->addArgument($config);
        $listenerId = $template . '.' . $id;
        $container->setDefinition($listenerId, $listenerDefinition);


        $template = 'ailove.security.token_authentication_provider';
        $providerDefinition = new DefinitionDecorator($template);
        $providerDefinition
            ->addArgument($config);
        $providerId = $template . '.' . $id;
        $container->setDefinition($providerId, $providerDefinition);

        return [$providerId, $listenerId, $defaultEntryPoint];
    }

    public function addConfiguration(NodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode('name')->defaultValue('_ailove_token')->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'ailove.security.token_authentication_provider.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('ailove.security.token_authentication_provider'));

        return $provider;
    }


    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'http';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'ailove_token_query';
    }
}
