<?php

namespace ADW\AiloveBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

/**
 * Class SocialFactory.
 *
 * @author Artur Vesker
 */
class SocialFactory extends AbstractFactory
{
    /**
     * @inheritdoc
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'ailove.security.token_authentication_provider.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('ailove.security.token_authentication_provider'));

        return $provider;
    }

    /**
     * {@inheritdoc}
     */
    protected function getListenerId()
    {
        return 'ailove.security.social_authentication_listener';
    }

    /**
     * @inheritdoc
     */
    public function getPosition()
    {
        return 'http';
    }

    /**
     * @inheritdoc
     */
    public function getKey()
    {
        return 'ailove_social';
    }
}
