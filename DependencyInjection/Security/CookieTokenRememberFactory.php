<?php

namespace ADW\AiloveBundle\DependencyInjection\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\RememberMeFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class CookieTokenRememberFactory.
 *
 * @author Artur Vesker
 */
class CookieTokenRememberFactory implements SecurityFactoryInterface
{

    protected $options = [
        'name' => 'TOKEN',
        'lifetime' => 31536000,
        'path' => '/',
        'domain' => null,
        'secure' => false,
        'httponly' => true,
        'always_remember_me' => false,
        'remember_me_parameter' => '_ailove_token',
    ];

    /**
     * @inheritdoc
     */
    public function addConfiguration(NodeDefinition $node)
    {
        $builder = $node->children();

        $builder
            ->scalarNode('secret')->isRequired()->cannotBeEmpty()->end()
            ->scalarNode('catch_exceptions')->defaultFalse()->end()
        ;

        foreach ($this->options as $name => $value) {
            if (is_bool($value)) {
                $builder->booleanNode($name)->defaultValue($value);
            } else {
                $builder->scalarNode($name)->defaultValue($value);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        // authentication provider
        $authProviderId = 'ailove.security.token_authentication_provider.'.$id;

        $container
            ->setDefinition($authProviderId, new DefinitionDecorator('ailove.security.token_authentication_provider'))
            ->replaceArgument(0, new Reference('security.user_checker.'.$id))
            ->addArgument(new Reference($userProvider))
            ->addArgument($config['secret'])
            ->addArgument($id)
        ;

        $templateId = 'ailove.security.remember_me_services';
        $rememberMeServicesId = $templateId.'.'.$id;

        if ($container->hasDefinition('security.logout_listener.'.$id)) {
            $container
                ->getDefinition('security.logout_listener.'.$id)
                ->addMethodCall('addHandler', array(new Reference($rememberMeServicesId)))
            ;
        }

        $rememberMeServices = $container->setDefinition($rememberMeServicesId, new DefinitionDecorator($templateId));
        $rememberMeServices->replaceArgument(1, $config['secret']);
        $rememberMeServices->replaceArgument(2, $id);


        //$this->options['always_remember_me'] = $this->options['always_save_token'];
        // remember-me options
        $rememberMeServices->replaceArgument(3, array_intersect_key($config, $this->options));

        // attach to remember-me aware listeners
        $userProviders = array();
        foreach ($container->findTaggedServiceIds('security.remember_me_aware') as $serviceId => $attributes) {
            foreach ($attributes as $attribute) {
                if (!isset($attribute['id']) || $attribute['id'] !== $id) {
                    continue;
                }

                if (!isset($attribute['provider'])) {
                    throw new \RuntimeException('Each "security.remember_me_aware" tag must have a provider attribute.');
                }

                $userProviders[] = new Reference($attribute['provider']);

                $container
                    ->getDefinition($serviceId)
                    ->addMethodCall('setRememberMeServices', array(new Reference($rememberMeServicesId)))
                ;
            }
        }

        if (count($userProviders) === 0) {
            throw new \RuntimeException('You must configure at least one remember-me aware listener (such as form-login) for each firewall that has remember-me enabled.');
        }

        $rememberMeServices->replaceArgument(0, $userProviders);

        // remember-me listener
        $listenerId = 'security.authentication.listener.rememberme.'.$id;
        $listener = $container->setDefinition($listenerId, new DefinitionDecorator('security.authentication.listener.rememberme'));
        $listener->replaceArgument(1, new Reference($rememberMeServicesId));
        $listener->replaceArgument(5, $config['catch_exceptions']);

        return [$authProviderId, $listenerId, $defaultEntryPoint];
    }

    /**
     * {@inheritdoc}
     */
    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'ailove.security.token_authentication_provider.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('ailove.security.token_authentication_provider'));

        return $provider;
    }

    /**
     * {@inheritdoc}
     */
    protected function getListenerId()
    {
        return 'ailove.security.cookie_token_authentication_listener';
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return 'remember_me';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey()
    {
        return 'ailove_token_cookie';
    }

}
