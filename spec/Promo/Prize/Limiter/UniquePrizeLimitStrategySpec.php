<?php

namespace spec\ADW\AiloveBundle\Promo\Prize\Limiter;

use ADW\AiloveBundle\Model\Prize;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class UniquePrizeLimitStrategySpec
 *
 * @author Artur Vesker
 */
class UniquePrizeLimitStrategySpec extends ObjectBehavior
{

    function let(PrizeRepository $prizeRepository, PrizeWinner $prizeWinner, Prize $prize)
    {
        $prize->getSystemName()->willReturn('test');
        $prizeWinner->getPrize()->willReturn($prize);
        $prizeRepository->findByUser(Argument::any())->willReturn([$prizeWinner]);

        $this->beConstructedWith($prizeRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ADW\AiloveBundle\Promo\Prize\Limiter\UniquePrizeLimitStrategy');
    }

    function it_is_check_availabity_correct(User $user)
    {
        $this->isAvailable($user, 'test')->shouldBe(false);
        $this->isAvailable($user, 'test2')->shouldBe(true);
    }
}
