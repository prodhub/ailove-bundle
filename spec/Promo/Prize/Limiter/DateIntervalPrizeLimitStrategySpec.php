<?php

namespace spec\ADW\AiloveBundle\Promo\Prize\Limiter;

use ADW\AiloveBundle\Model\Prize;
use ADW\AiloveBundle\Model\PrizeWinner;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PrizeRepository;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class DateIntervalPrizeLimitStrategySpec
 *
 * @author Artur Vesker
 */
class DateIntervalPrizeLimitStrategySpec extends ObjectBehavior
{

    function it_is_initializable(PrizeRepository $prizeRepository, \DateInterval $interval)
    {
        $this->beConstructedWith($prizeRepository, $interval);
        $this->shouldHaveType('ADW\AiloveBundle\Promo\Prize\Limiter\DateIntervalPrizeLimitStrategy');
    }

    function it_is_check_availabity_correct_for_any_prizes_if_interval_is_reached(PrizeRepository $prizeRepository, User $user, PrizeWinner $prizeWinner, Prize $prize, \DateTime $dateTime)
    {
        $dateTime->getTimestamp()->willReturn(time() - 2000);
        $prizeWinner->getWonAt()->willReturn($dateTime);
        $prizeRepository->findByUser(Argument::any())->willReturn([$prizeWinner]);

        $this->beConstructedWith($prizeRepository, 1000);

        $this->isAvailable($user, 'test')->shouldBe(true);
    }

    function it_is_check_availabity_correct_for_any_prizes_if_interval_is_not_reached(PrizeRepository $prizeRepository, User $user, PrizeWinner $prizeWinner, Prize $prize, \DateTime $dateTime)
    {
        $dateTime->getTimestamp()->willReturn(time() - 2000);
        $prizeWinner->getWonAt()->willReturn($dateTime);
        $prizeRepository->findByUser(Argument::any())->willReturn([$prizeWinner]);

        $this->beConstructedWith($prizeRepository, 2001);

        $this->isAvailable($user, 'test')->shouldBe(false);
    }

    function it_is_check_availabity_correct_for_any_prizes_if_user_not_has_prizes(PrizeRepository $prizeRepository, User $user, PrizeWinner $prizeWinner, Prize $prize, \DateTime $dateTime)
    {
        $prizeRepository->findByUser(Argument::any())->willReturn([]);

        $this->beConstructedWith($prizeRepository, 2001);

        $this->isAvailable($user, 'test')->shouldBe(true);
    }

    function it_is_check_availabity_correct_for_same_prizes_if_user_not_has_same_prizes(PrizeRepository $prizeRepository, User $user, PrizeWinner $prizeWinner, Prize $prize, \DateTime $dateTime)
    {
        $dateTime->getTimestamp()->willReturn(time() - 2000);
        $prize->getSystemName()->willReturn('test2');
        $prizeWinner->getPrize()->willReturn($prize);
        $prizeWinner->getWonAt()->willReturn($dateTime);
        $prizeRepository->findByUser(Argument::any())->willReturn([$prizeWinner]);

        $this->beConstructedWith($prizeRepository, 2001, true);

        $this->isAvailable($user, 'test')->shouldBe(true);
    }

    function it_is_check_availabity_correct_for_same_prizes_if_interval_is_reached(
        PrizeRepository $prizeRepository, User $user, PrizeWinner $prizeWinner, Prize $prize, \DateTime $dateTime,
        \DateTime $dateTime2, PrizeWinner $prizeWinner2, Prize $prize2
    )
    {
        $dateTime->getTimestamp()->willReturn(time() - 2000);
        $prize->getSystemName()->willReturn('test');
        $prizeWinner->getPrize()->willReturn($prize);
        $prizeWinner->getWonAt()->willReturn($dateTime);

        $dateTime2->getTimestamp()->willReturn(time() - 5000);
        $prize2->getSystemName()->willReturn('test2');
        $prizeWinner2->getPrize()->willReturn($prize2);
        $prizeWinner2->getWonAt()->willReturn($dateTime2);


        $prizeRepository->findByUser(Argument::any())->willReturn([$prizeWinner, $prizeWinner2]);

        $this->beConstructedWith($prizeRepository, 2001, true);

        $this->isAvailable($user, 'test2')->shouldBe(true);
    }

}
