<?php

namespace spec\ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\UserAction;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ADW\RestClientBundle\Client\Client as RestClient;

/**
 * Class ActionRepositorySpec
 *
 * @author Artur Vesker
 */
class ActionRepositorySpec extends ObjectBehavior
{

    public function let(RestClient $client, User $user, UserAction $userAction)
    {
        $client
            ->request(
                Argument::type('ADW\AiloveBundle\RestClient\Method\User\Action\DoActionMethodDescription'),
                Argument::any()
            )
            ->willReturn($userAction);

        $userAction->getAction()->willReturn('test_action');
        $user->getId()->willReturn(10);
        $this->beConstructedWith($client, $user);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ADW\AiloveBundle\Repository\ActionRepository');
    }

    function it_return_user_action_instance_when_save()
    {
        $this->save('test_action')->shouldHaveType('ADW\AiloveBundle\Model\UserAction');
    }


}
