<?php

namespace spec\ADW\AiloveBundle\Repository;

use ADW\AiloveBundle\Model\Clinic;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ADW\RestClientBundle\Client\Client;

/**
 * Class ClinicRepositorySpec
 *
 * @author Artur Vesker
 */
class ClinicRepositorySpec extends ObjectBehavior
{

    function let(Client $client, Clinic $clinic1, Clinic $clinic2, Clinic $clinic3, Clinic $clinic4)
    {
        $clinic1->getCity()->willReturn('moscow');
        $clinic2->getCity()->willReturn('moscow');
        $clinic3->getCity()->willReturn('spb');
        $clinic4->getCity()->willReturn('voronezh');

        $client
            ->request(Argument::type('ADW\AiloveBundle\RestClient\Method\Collection\Clinic\ListClinicsMethodDescription'))
            ->willReturn([$clinic1, $clinic2, $clinic3, $clinic4])
        ;

        $this->beConstructedWith($client);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('ADW\AiloveBundle\Repository\ClinicRepository');
    }

    function it_is_return_collection_when_find_all()
    {
        $this->findAll()->shouldHaveCount(4);

        for ($i = 0; $i < 4; $i++) {
            $this->findAll()[$i]->shouldBeAnInstanceOf('ADW\AiloveBundle\Model\Clinic');
        }
    }

    function it_is_applies_filter_when_find_by_city()
    {
        $this->findByCity('moscow')->shouldHaveCount(2);

        for ($i = 0; $i < 2; $i++) {
            $this->findAll()[$i]->shouldBeAnInstanceOf('ADW\AiloveBundle\Model\Clinic');
        }
    }
}
