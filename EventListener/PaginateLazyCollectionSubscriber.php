<?php

namespace ADW\AiloveBundle\EventListener;

use ADW\AiloveBundle\Collection\LazyCollection;
use Knp\Component\Pager\Event\ItemsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PaginateLazyCollectionSubscriber.
 *
 * @author Artur Vesker
 */
class PaginateLazyCollectionSubscriber implements  EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public function items(ItemsEvent $event)
    {
        if (!$event->target instanceof LazyCollection) {
            return;
        }

        $collection = $event->target;

        $event->count = $collection->count();
        $event->items = $collection->slice($event->getOffset(), $event->getLimit());

        $event->stopPropagation();
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'knp_pager.items' => ['items', 1],
        ];
    }
}
