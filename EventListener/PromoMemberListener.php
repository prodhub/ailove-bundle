<?php

namespace ADW\AiloveBundle\EventListener;

use ADW\AiloveBundle\Annotation\PromoMember;
use ADW\AiloveBundle\Exception\NotMemberOfPromoException;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\PromoRepository;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class PromoMemberListener
 *
 * @package ADW\AiloveBundle\EventListener
 * @author Artur Vesker
 */
class PromoMemberListener
{

    /**
     * @var PromoRepository
     */
    protected $promoRepository;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    public function __construct(PromoRepository $promoRepository, TokenStorageInterface $tokenStorage)
    {
        $this->promoRepository = $promoRepository;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @param FilterControllerEvent $event
     */
    public function onController(FilterControllerEvent $event)
    {
        if (!$promoMemberAnnotations = $event->getRequest()->attributes->get('_ailove_promo_member')) {
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            throw new AccessDeniedException('User must be authenticated');
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedException('Allowed for only ailove users');
        }

        foreach ($promoMemberAnnotations as $promoMember) {
            $this->handleAnnotation($user, $promoMember);
        }
    }

    /**
     * @param User $user
     * @param PromoMember $promoMember
     */
    private function handleAnnotation(User $user, PromoMember $promoMember)
    {
        $requested = $promoMember->getPromo();

        if (is_array($requested)) {
            if (count($this->promoRepository->findByUserAndNames($user, $requested)) < 1) {
                throw new NotMemberOfPromoException($user, $requested, 'User must be member least one of ' . implode(', ', $requested) . ' promo');
            }

            return;
        }

        if (!$promo = $this->promoRepository->findOneByUserAndName($user, $promoMember->getPromo())) {
            throw new NotMemberOfPromoException($user, $requested, 'User must be member of promo ' . $promoMember->getPromo());
        }
    }

}