<?php

namespace ADW\AiloveBundle\EventListener;

use ADW\AiloveBundle\Annotation\UserVerification;
use ADW\AiloveBundle\Exception\UserEmailNotVerifiedException;
use ADW\AiloveBundle\Exception\UserPhoneNotVerifiedException;
use ADW\AiloveBundle\Model\User;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserVerifiedListener
 *
 * @package ADW\AiloveBundle\EventListener
 * @author Artur Vesker
 */
class UserVerifiedListener
{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @param FilterControllerEvent $event
     */
    public function onController(FilterControllerEvent $event)
    {
        if (!$verifiedAnnotation = $event->getRequest()->attributes->get('_ailove_user_verified')) {
            return;
        }

        if (!$token = $this->tokenStorage->getToken()) {
            throw new AccessDeniedException('User must be authenticated');
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedException('Allowed for only ailove users');
        }

        $this->handleAnnotation($user, $verifiedAnnotation);
    }

    /**
     * @param User $user
     * @param UserVerification $verifiedAnnotation
     */
    private function handleAnnotation(User $user, UserVerification $verifiedAnnotation)
    {
        if ($verifiedAnnotation->isEmailVerification() && (!$user->isEmailVerified())) {
            throw new UserEmailNotVerifiedException($user);
        }

        if ($verifiedAnnotation->isPhoneVerification() && (!$user->isPhoneVerified())) {
            throw new UserPhoneNotVerifiedException($user);
        }
    }

}