<?php

namespace ADW\AiloveBundle\EventListener\Doctrine;

use ADW\AiloveBundle\Repository\UserRepository;
use ADW\AiloveBundle\Security\User\UserDecoratorInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class UserDecoratorSubscriber.
 *
 * @author Artur Vesker
 */
class UserDecoratorSubscriber implements EventSubscriber
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var string
     */
    protected $decoratorClass;

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postLoad',
        ];
    }

    /**
     * @param UserRepository $userRepository
     * @param $decoratorClass
     */
    public function __construct(UserRepository $userRepository, $decoratorClass)
    {
        $this->userRepository = $userRepository;
        $this->decoratorClass = $decoratorClass;
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $event
     */
    public function postLoad(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        if (!(($entity instanceof UserDecoratorInterface) && (ClassUtils::getClass($entity) === $this->decoratorClass))) {
            return;
        }

        $entity->decorate($this->userRepository->find($entity->getId()));
    }
}
