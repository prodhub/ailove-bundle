<?php

namespace ADW\AiloveBundle\EventListener;

use ADW\AiloveBundle\Event\UserDecoratedEvent;
use ADW\AiloveBundle\Repository\PetRepositoryFactory;
use ADW\AiloveBundle\Security\User\AbstractUserDecorator;

/**
 * Class PostUserDecorationListener.
 *
 * @author Artur Vesker
 */
class PostUserDecorationListener
{
    /**
     * @var \ADW\AiloveBundle\Repository\PetRepositoryFactory
     */
    protected $petRepositoryFactory;

    /**
     * @param \ADW\AiloveBundle\Repository\PetRepositoryFactory $petRepositoryFactory
     */
    public function __construct(PetRepositoryFactory $petRepositoryFactory)
    {
        $this->petRepositoryFactory = $petRepositoryFactory;
    }

    /**
     * @param UserDecoratedEvent $event
     */
    public function onDecorate(UserDecoratedEvent $event)
    {
        $decorator = $event->getDecoratedUser();

        if (!$decorator instanceof AbstractUserDecorator) {
            return;
        }

        $decorator->setPetRepositoryLoader(function ($user) {
            return $this->petRepositoryFactory->createForUser($user);
        });
    }
}
