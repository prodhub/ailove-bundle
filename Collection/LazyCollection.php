<?php

namespace ADW\AiloveBundle\Collection;

use ADW\AiloveBundle\Exception\ReadOnlyException;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class LazyCollection.
 *
 * @author Artur Vesker
 */
class LazyCollection implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var int
     *
     * @Serialized\Exclude()
     */
    protected $totalCount;

    /**
     * @var Paginator
     *
     * @Serialized\Exclude()
     */
    protected $paginator;

    /**
     * @var int
     *
     * @Serialized\Exclude()
     */
    protected $position = 0;

    public function __construct($totalCount, Paginator $paginator)
    {
        $this->totalCount = $totalCount;
        $this->paginator = $paginator;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->offsetGet($this->position);
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->offsetExists($this->position);
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $offset < $this->totalCount;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new \InvalidArgumentException('Undefined Offset '.$offset);
        }

        return $this->paginator->getOffset($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        throw new ReadOnlyException('This collection is read-only');
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        throw new ReadOnlyException('This collection is read-only');
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->totalCount;
    }

    public function slice($offset, $count)
    {
        if ($offset > $this->totalCount) {
            return [];
        }

        $slice = [];

        $lastOffset = $offset + $count;

        for ($i = $offset; ($i < $lastOffset && $i < $this->totalCount); ++$i) {
            $slice[] = $this->offsetGet($i);
        }

        return $slice;
    }

    /**
     * @return mixed|null
     */
    public function getFirstOrNull()
    {
        return $this->totalCount > 0 ? $this->offsetGet(0) : null;
    }
}
