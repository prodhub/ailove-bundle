<?php

namespace ADW\AiloveBundle\Collection;

use ADW\AiloveBundle\AiloveSerializer;
use ADW\AiloveBundle\Exception\InvalidResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Uri;
use JMS\Serializer\DeserializationContext;
use Psr\Http\Message\RequestInterface;

/**
 * Class PageLoader.
 *
 * @author Artur Vesker
 */
class PageLoader
{

    /**
     * @var AiloveSerializer
     */
    protected $serializer;

    /**
     * @var RequestInterface
     */
    protected $baseRequest;

    /**
     * @var string
     */
    protected $domainType;

    /**
     * @var string
     */
    protected $format;

    /**
     * @var DeserializationContext
     */
    protected $context;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzle;

    /**
     * PageLoader constructor.
     * @param Client $guzzle
     * @param AiloveSerializer $serializer
     * @param RequestInterface $request
     * @param $domainType
     * @param $format
     * @param DeserializationContext|null $context
     */
    public function __construct(
        Client $guzzle,
        AiloveSerializer $serializer,
        RequestInterface $request,
        $domainType,
        $format,
        DeserializationContext $context = null
    )
    {
        $this->baseRequest = $request;
        $this->domainType = $domainType;
        $this->format = $format;
        $this->guzzle = $guzzle;
        $this->serializer = $serializer;
        $this->context = $context;
    }

    /**
     * @param $page
     * @return array
     */
    public function __invoke($page)
    {
        $request = $this->createRequest($page);
        $response = $this->guzzle->send($request);

        return $this->deserialize($response->getBody());
    }

    /**
     * @param $body
     * @return array
     */
    public function deserialize($body)
    {
        $decoded = $this->serializer->decode($body);

        if (!array_key_exists('results', $decoded)) {
            throw new InvalidResponseException('Invalid CRM pagination: missing results field. It\'s PAYAL\'NIK time!');
        }

        return $this->normalize($decoded['results']);
    }

    /**
     * @param $decoded
     * @return array
     */
    public function normalize($decoded)
    {
        return $this->serializer->normalize($decoded, $this->formatType(), $this->serializer->fillContext($this->context, DeserializationContext::create()));
    }

    /**
     * @param $page
     * @return Request
     */
    protected function createRequest($page)
    {
        return $this->baseRequest->withUri(Uri::withQueryValue($this->baseRequest->getUri(), 'page', $page));
    }

    /**
     * @return string
     */
    protected function formatType()
    {
        return 'array<' . $this->domainType . '>';
    }
}
