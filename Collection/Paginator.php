<?php

namespace ADW\AiloveBundle\Collection;

/**
 * Class Paginator.
 *
 * @author Artur Vesker
 */
class Paginator
{
    const MAX = 50;

    /**
     * @var array
     */
    protected $pages = [];

    /**
     * @var callable
     */
    protected $loader;

    public function __construct(callable $loader)
    {
        $this->loader = $loader;
    }

    /**
     * @param $page
     *
     * @return array
     */
    private function loadPage($page)
    {
        $loader = $this->loader;
        $this->pages[$page] = $loader($page);

        return $this->pages[$page];
    }

    /**
     * @param $offset
     *
     * @return array
     */
    public function getPageContainsOffset($offset)
    {
        return $this->getPage($this->getPageIndexByOffset($offset));
    }

    private function getPageIndexByOffset($offset)
    {
        return (int) ceil(++$offset / self::MAX);
    }

    /**
     * @param $page
     *
     * @return array
     */
    public function getPage($page)
    {
        if (isset($this->pages[$page])) {
            return $this->pages[$page];
        }

        return $this->loadPage($page);
    }

    /**
     * @param $offset
     *
     * @return mixed
     */
    public function getOffset($offset)
    {
        $page = $this->getPageIndexByOffset($offset);

        return $this->getPage($page)[($offset - ($page - 1) * self::MAX)];
    }
}
