<?php

namespace ADW\AiloveBundle\Collection;

use ADW\AiloveBundle\Model\Contact;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ContactsCollection
 *
 * @package ADW\AiloveBundle\Collection
 * @author Artur Vesker
 */
class ContactsCollection extends ArrayCollection
{

    /**
     * @return Contact
     */
    public function getMainContact()
    {
        /**
         * @var Contact[] $this
         */
        foreach ($this as $contact) {
            if ($contact->getType() == Contact::TYPE_GENERAL) {
                return $contact;
            }
        }

        return null;
    }

    /**
     * @return Contact|null
     */
    public function getDeliveryContact()
    {
        /**
         * @var Contact[] $this
         */
        foreach ($this as $contact) {
            if ($contact->getType() == Contact::TYPE_DELIVERY) {
                return $contact;
            }
        }

        return null;
    }

    /**
     * @param $id
     * @return Contact|null
     */
    public function find($id)
    {
        /**
         * @var Contact $element
         */
        foreach ($this as $element) {
            if ($element->getId() == $id) {
                return $element;
            }
        }

        return null;
    }

    /**
     * @param Contact $contact
     * @return $this
     */
    public function save(Contact $contact)
    {
        if ($id = $contact->getId()) {
            if ($existingContactWithThisId = $this->indexOf($id)) {
                $this->set($existingContactWithThisId, $contact);

                return $this;
            }
        }

        $this->add($contact);

        return $this;
    }
}