<?php

namespace ADW\AiloveBundle\Collection;

/**
 * Class CachedPageLoader
 *
 * @package ADW\AiloveBundle\Collection
 * @author Artur Vesker
 */
class CachedPageLoader
{

    /**
     * @var PageLoader
     */
    protected $delegate;

    /**
     * @var array
     */
    protected $requested;

    /**
     * @var array
     */
    protected $normalized;

    /**
     * CachedPageLoader constructor.
     * @param PageLoader $delegate
     * @param array $requested
     * @param array $normalized
     */
    public function __construct(PageLoader $delegate, array $requested = [], array $normalized = [])
    {
        $this->delegate = $delegate;
        $this->requested = $requested;
        $this->normalized = $normalized;
    }

    /**
     * @param $page
     * @return array
     */
    public function __invoke($page)
    {
        if (array_key_exists($page, $this->normalized)) {
            return $this->normalized[$page];
        }
        
        if (array_key_exists($page, $this->requested)) {
            return $this->normalized[$page] = $this->delegate->normalize($this->requested[$page]);
        }

        return $this->normalized[$page] = call_user_func($this->delegate, $page);
    }


}