<?php

namespace ADW\AiloveBundle\Validator\Constraints;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Promo\PromoManager;
use ADW\CommonBundle\Exception\ValidationViolationException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class PromoCodeValidator
 *
 * @package ADW\AiloveBundle\Validator\Constraints
 * @author Artur Vesker
 */
class PromoCodeValidator extends ConstraintValidator
{


    /**
     * @var PromoManager
     */
    protected $promoManager;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param PromoManager $promoManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(PromoManager $promoManager, TokenStorageInterface $tokenStorage)
    {
        $this->promoManager = $promoManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$token = $this->tokenStorage->getToken()) {
            throw new \InvalidArgumentException('User must be authenticated');
        }

        if (!$token->getUser() instanceof User) {
            throw new \InvalidArgumentException('User must be instance of ADW\AiloveBundle\Model\User');
        }

        try {
            $code = $this->promoManager->getCode($token->getUser(), $value);
        } catch (ValidationViolationException $e) {
            $this->context->addViolation($e->getConstraintViolationList()->get(0)->getMessage());
            return;
        }

        if ($constraint->type) {
            if (is_string($constraint->type)) {
                $constraint->type = [$constraint->type];
            }
            if (!in_array($code->getCode()->getCodeType(), $constraint->type)) {
                $this->context->addViolation('Неверный тип кода');
            }
        }

        if ($constraint->products) {
            if (!in_array($code->getCode()->getProduct()->getSystemName(), $constraint->products)) {
                $this->context->addViolation('Нельзя использовать код другой категории');
            }
        }
    }

}