<?php

namespace ADW\AiloveBundle\Validator\Constraints;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Promo\Prize\Limiter\PrizeLimitationStrategyInterface;
use ADW\AiloveBundle\Repository\PrizeRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Class PrizeValidator
 *
 * @package ADW\AiloveBundle\Validator\Constraints
 * @author Artur Vesker
 */
class PrizeValidator extends ConstraintValidator
{

    /**
     * @var PrizeRepository
     */
    protected $prizeRepository;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var array
     */
    protected $limitationStrategies;

    /**
     * @param PrizeRepository $prizeRepository
     * @param TokenStorageInterface $tokenStorage
     * @param array $limitationStrategies
     */
    public function __construct(PrizeRepository $prizeRepository, TokenStorageInterface $tokenStorage, array $limitationStrategies)
    {
        $this->prizeRepository = $prizeRepository;
        $this->tokenStorage = $tokenStorage;
        $this->limitationStrategies = $limitationStrategies;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$token = $this->tokenStorage->getToken()) {
            throw new \InvalidArgumentException('User must be authenticated');
        }

        $user = $token->getUser();

        if (!$user instanceof User) {
            throw new \InvalidArgumentException('User must be instance of ADW\AiloveBundle\Model\User');
        }

        $prizes = $this->prizeRepository->findAll();

        $prize = $this->find($prizes, $value);

        if (!$prize) {
            $this->context->addViolation('Нет такого приза');
            return;
        }

        if (!($prize->getAmountLeft() > 0)) {
            $this->context->addViolation('Призы закончились');
            return;
        }

        $prizes = null;

        foreach ($constraint->extraLimitations as $limitation => $options) {
            if (!isset($this->limitationStrategies[$limitation])) {
                throw new InvalidArgumentException('Undefined limitation');
            }

            if (!$prizes) {
                $prizes = $this->prizeRepository->findByUser($user);
            }

            $this->limitationStrategies[$limitation]($user, $prize, $prizes, $this->context, $options);
        }

        return;
    }

    /**
     * @param $prizes
     * @param $systemName
     * @return null
     */
    protected function find($prizes, $systemName)
    {
        foreach ($prizes as $prize) {
            if ($prize->getSystemName() == $systemName) {
                return $prize;
            }
        }

        return null;
    }

}