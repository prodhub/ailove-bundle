<?php

namespace ADW\AiloveBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Prize
 *
 * @package ADW\AiloveBundle\Validator\Constraints
 * @author Artur Vesker
 */
class Prize extends Constraint
{

    /**
     * @var array
     */
    public $extraLimitations;

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'prize_validator';
    }


}