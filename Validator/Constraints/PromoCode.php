<?php

namespace ADW\AiloveBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class PromoCode
 *
 * @package ADW\AiloveBundle\Validator\Constraints
 * @author Artur Vesker
 *
 * @Annotation
 */
class PromoCode extends Constraint
{

    /**
     * @var null|string|array
     */
    public $type;

    /**
     * @var array
     */
    public $products = null;

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'promo_code_validator';
    }


}