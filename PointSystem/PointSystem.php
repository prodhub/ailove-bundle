<?php

namespace ADW\AiloveBundle\PointSystem;

use ADW\AiloveBundle\AiloveUtils;
use ADW\AiloveBundle\Collection\LazyCollection;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\FIASObjectsRepository;
use ADW\AiloveBundle\Repository\SalesDistrictRepository;
use ADW\AiloveBundle\RestClient\Method\PointSystem\GetBalanceMethodDescription;
use ADW\AiloveBundle\RestClient\Method\PointSystem\GetRatingMethodDescription;
use ADW\PromoBundle\PromoInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class PointSystem.
 *
 * @author Artur Vesker
 */
class PointSystem
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var \ADW\AiloveBundle\Repository\SalesDistrictRepository
     */
    protected $salesDistrictRepository;

    /**
     * @var FIASObjectsRepository
     */
    protected $FIASObjectRepository;

    /**
     * @param Client                  $client
     * @param SalesDistrictRepository $salesDistrictRepository
     * @param FIASObjectsRepository   $FIASObjectsRepository
     */
    public function __construct(Client $client, SalesDistrictRepository $salesDistrictRepository, FIASObjectsRepository $FIASObjectsRepository)
    {
        $this->client = $client;
        $this->salesDistrictRepository = $salesDistrictRepository;
        $this->FIASObjectRepository = $FIASObjectsRepository;
    }

    public function getStats(User $user, $promo = null, $selector = null)
    {
        return $this->client->request(new GetBalanceMethodDescription(), [
            'uid' => $user->getId(),
            'promo' => $promo,
            'selector' => $selector,
        ]);
    }


    /**
     * @param $promo
     * @param $selector
     * @return LazyCollection|User\UserWithRating[]
     */
    public function getRating($promo, $selector)
    {
        return $this->client->request(new GetRatingMethodDescription(), ['promo' => $promo, 'selector' => $selector]);
    }

    /**
     * @param \ADW\AiloveBundle\Model\User    $user
     * @param \ADW\PromoBundle\PromoInterface $promo
     *
     * @return \ADW\AiloveBundle\Model\Stats
     */
    public function getStatsInSalesDistrict(User $user, PromoInterface $promo = null)
    {
        return $this->getStats(
            $user,
            $promo ? $promo->getId() : null,
            $promo->getId().'-'.AiloveUtils::getSalesDistrictForUser($this->salesDistrictRepository, $this->FIASObjectRepository, $user)
        );
    }
}
