<?php

namespace ADW\AiloveBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;
use ADW\AiloveBundle\RestClient\Method\Auth\AddSocialMethodDescription;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class SocialAuthController
 *
 * @package ADW\AiloveBundle\Controller
 * @author Artur Vesker
 *
 * @Route("/social-auth")
 */
class SocialAuthController extends Controller
{

    private $social = array(
        'odnoklassniki' => 'ok',
        'vkontakte' => 'vk',
        'facebook' => 'fb',
        'twitter' => 'tw',
        'google' => 'gp'
    );

    /**
     * @Route("/")
     * @Method("GET")
     */
    public function handleAction(Request $request)
    {
        $authStatus = $request->get('auth_status');


        $user = $this->getUser();

        if (!$authStatus) {
            throw new BadRequestHttpException('Missed auth status');
        }

        if ($authStatus === 'error') {
            throw new BadRequestHttpException($request->get('message'));
        }

        if ($authStatus == 'register') {
            if ($user) {
                return $this->handleAttachSocialNetwork($request);
            }
            return $this->handleRegisterRequest($request);
        }

        if ($authStatus == 'is_auth') {
            throw new \RuntimeException('Invalid social authentication configuration');
        }

        throw new \RuntimeException("Unexpected \"{$authStatus}\" status");
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function handleRegisterRequest(Request $request)
    {
        $config = $this->container->getParameter('ailove.config.social');
        return $this->redirectToRoute($config['register_route'], $request->query->all());
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function handleAttachSocialNetwork(Request $request)
    {
        $config = $this->container->getParameter('ailove.config.social');

        if (!$config['add_social_network_route']) {
            throw new \RuntimeException("Invalid configuration");
        }

        $result = $this->get('ailove_client')->request(new AddSocialMethodDescription(), [
            'social_type' => $this->social[$request->query->get('social_type')],
            'social_id' => $request->query->get('social_id')
        ]);
        return $this->redirectToRoute($config['add_social_network_route']);
    }


}