<?php

namespace ADW\AiloveBundle\Subscription;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Subscribe\SubscribeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Subscribe\UnsubscribeMethodDescription;
use ADW\AiloveBundle\RestClient\Method\User\ListUserSubscriptionsMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class SubscribeManager.
 *
 * @author Artur Vesker
 */
class SubscribeManager
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * SubscribeManager constructor.
     *
     * @param Client $client
     * @param Logger $logger
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param string $email
     * @param string $subscriptionId
     *
     * @return mixed
     */
    public function subscribe($email, $subscriptionId)
    {
        $this->logger->info("Subscribe user. Email - {$email}. Subscription #{$subscriptionId}");

        return $this->client->request(new SubscribeMethodDescription(), ['email' => $email, 'subscription' => (int)$subscriptionId]);
    }

    /**
     * @param $email
     * @param $subscriptionId
     * @return null
     */
    public function unsubscribe($email, $subscriptionId)
    {
        $this->logger->info("Unsubscribe user. Email - {$email}. Subscription #{$subscriptionId}");
        $this->client->request(new UnsubscribeMethodDescription(), ['email' => $email, 'subscription' => (int)$subscriptionId]);

        return null;
    }

    /**
     * @param User $user
     * @param $subscriptionId
     * @return mixed
     */
    public function getUserSubscriptions(User $user)
    {
        return $this->client->request(new ListUserSubscriptionsMethodDescription(), ['uid' => $user->getId()]);
    }

    /**
     * @param User $user
     * @param $subscriptionId
     * @return bool
     */
    public function isSubscribed(User $user, $subscriptionId)
    {
        $userSubscriptions = $this->getUserSubscriptions($user);

        foreach ($userSubscriptions['subscriptions'] as $subscription) {
            if ($subscription['id'] == $subscriptionId) {
                return true;
            }
        }

        return false;
    }

}
