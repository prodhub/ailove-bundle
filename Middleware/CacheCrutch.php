<?php

namespace ADW\AiloveBundle\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class CacheCrutch.
 *
 * @author Artur Vesker
 */
class CacheCrutch
{
    /**
     * @param callable $handler
     *
     * @return callable
     */
    public function __invoke(callable $handler)
    {
        return function (RequestInterface $request, array $options) use ($handler) {
            return $handler($request, $options)->then(
                function (ResponseInterface $response) use ($request) {
                    if ($cacheControl = $request->getHeaderLine('Cache-Control')) {
                        return $response->withHeader('Cache-Control', $cacheControl);
                    }

                    return $response;
                }
            );
        };
    }
}
