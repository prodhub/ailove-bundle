<?php

namespace ADW\AiloveBundle\Security\Provider;

use ADW\AiloveBundle\Security\Token\AiloveToken;
use ADW\AiloveBundle\Security\Token\ContactPasswordAiloveToken;
use ADW\AiloveBundle\Security\User\UserProvider;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class TokenAuthenticationProvider.
 *
 * @author Artur Vesker
 */
class TokenAuthenticationProvider implements AuthenticationProviderInterface
{

    /**
     * @var UserCheckerInterface
     */
    protected $userChecker;

    /**
     * @var UserProviderInterface
     */
    protected $userProvider;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @param UserCheckerInterface $userChecker
     * @param UserProvider $userProvider
     * @param $secret
     */
    public function __construct(UserCheckerInterface $userChecker, UserProvider $userProvider, $secret)
    {
        $this->userChecker = $userChecker;
        $this->userProvider = $userProvider;
        $this->secret = $secret;
    }

    /**
     * {@inheritdoc}
     *
     * @param AiloveToken $token
     */
    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->getCurrentUser($token);

        if (!$user) {
            $token->setAuthenticated(false);
            return $token;
        }

        $token->setUser($user);
        $token->refreshAuthenticationStatus();

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
        return ($token instanceof AiloveToken) && (!$token instanceof ContactPasswordAiloveToken);
    }
}
