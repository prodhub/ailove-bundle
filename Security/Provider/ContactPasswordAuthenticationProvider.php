<?php

namespace ADW\AiloveBundle\Security\Provider;

use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\RestClient\Method\Auth\LoginMethodDescription;
use ADW\AiloveBundle\Security\AiloveSecurityUtils;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use ADW\AiloveBundle\Security\User\UserProvider;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use ADW\RestClientBundle\Client\Client;

/**
 * Class ContactPasswordAuthenticationProvider.
 *
 * @author Artur Vesker
 */
class ContactPasswordAuthenticationProvider implements AuthenticationProviderInterface
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var UserProvider
     */
    protected $userProvider;

    /**
     * @var string
     */
    protected $providerKey;

    /**
     * @param \ADW\RestClientBundle\Client\Client $client
     * @param \ADW\AiloveBundle\Security\User\UserProvider $userProvider
     * @param $providerKey
     */
    public function __construct(Client $client, UserProvider $userProvider, $providerKey)
    {
        $this->client = $client;
        $this->userProvider = $userProvider;
        $this->providerKey = $providerKey;
    }

    /**
     * {@inheritdoc}
     * @param UsernamePasswordToken $token
     */
    public function authenticate(TokenInterface $token)
    {
        try {
            $authenticationData = $this->client->request(new LoginMethodDescription(), ['username' => $token->getUsername(), 'password' => $token->getCredentials()]);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() == 400) {
                AiloveSecurityUtils::handleException($e);
            }
            throw $e;
        }

        /**
         * @var AiloveToken $token
         */
        $token = $authenticationData['token'];

        $token->setUser($this->userProvider->wrapUser($authenticationData['user']));

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
        return ($token instanceof UsernamePasswordToken && $token->getProviderKey() == $this->providerKey);
    }
}
