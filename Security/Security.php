<?php

namespace ADW\AiloveBundle\Security;

use ADW\AiloveBundle\Model\Message\SuccessMessage;
use ADW\AiloveBundle\Model\PhoneCheck;
use ADW\AiloveBundle\Model\ConfirmKey;
use ADW\AiloveBundle\RestClient\Method\Auth\ChangeEmailMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ChangePasswordMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\CheckPhoneMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\CheckConfirmPhoneMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ChangePhoneMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ConfirmPhoneMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\ConfirmResetPasswordMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\RequestResetPasswordMethodDescription;
use ADW\AiloveBundle\RestClient\Method\Auth\RequestResetPasswordPhoneMethodDescription;
use Symfony\Bridge\Monolog\Logger;
use ADW\RestClientBundle\Client\Client;

/**
 * Class Security.
 *
 * @author Artur Vesker
 */
class Security
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Client $client
     * @param Logger $logger
     */
    public function __construct(Client $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * Change user password.
     *
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $newPasswordRepeat
     * @return SuccessMessage
     */
    public function changePassword($oldPassword, $newPassword, $newPasswordRepeat)
    {
        $result = $this->client->request(new ChangePasswordMethodDescription(), [
            'old_password' => $oldPassword,
            'new_password1' => $newPassword,
            'new_password2' => $newPasswordRepeat,
        ]);

        $this->logger->info('Changed password');

        return $result;
    }

    /**
     * Set link for password restoring.
     *
     * @param $email
     * @return SuccessMessage
     */
    public function sendRestorePasswordLink($email)
    {
        return $this->client->request(new RequestResetPasswordMethodDescription(), [
            'email' => $email,
        ]);

        return new SuccessMessage('Мы отправим ссылку для восстановления пароля, если пользователь с таким email существует');
    }


    /**
     * Set link for password restoring phone.
     *
     * @param $phone
     * @return SuccessMessage
     */
    public function sendRestorePasswordPhoneLink($phone)
    {
        return $this->client->request(new RequestResetPasswordPhoneMethodDescription(), [
            'phone' => $phone
        ]);

        return new SuccessMessage('Мы отправим ссылку для восстановления пароля, если пользователь с таким email существует');
    }


    /**
     * Send SMS Pre-Registration to confirm phone.
     *
     * @param $phone
     * @return PhoneCheck
     */
    public function sendPhoneCheck($phone)
    {
        return $this->client->request(new CheckPhoneMethodDescription(), [
            'phone' => $phone,
        ]);
    }

    /**
     * Confirm Phone SMS Pre-Registration.
     *
     * @param $data
     * @return ConfirmKey
     */
    public function sendPhoneCheckConfirm($data)
    {
        return $this->client->request(new CheckConfirmPhoneMethodDescription(), [
            'hashkey' => $data['hashkey'],
            'token' => $data['token'],
        ]);
    }


    /**
     * @param $uid
     * @param $token
     * @param $newPassword1
     * @param $newPassword2
     * @return \ADW\AiloveBundle\Model\Message\SuccessMessage
     */
    public function setNewPassword($uid, $token, $newPassword1, $newPassword2)
    {
        $result = $this->client->request(new ConfirmResetPasswordMethodDescription(), [
            'uid' => $uid,
            'token' => $token,
            'new_password1' => $newPassword1,
            'new_password2' => $newPassword2,
        ]);

        $this->logger->info('Set new password (restore)');

        return $result;
    }

    /**
     * Set new phone for user. Phone will be changed after confirmation.
     *
     * @param string $newPhone
     * @return SuccessMessage
     */
    public function changePhone($newPhone)
    {
        $result = $this->client->request(new ChangePhoneMethodDescription(), ['phone' => $newPhone]);

        $this->logger->info('Change phone to '.$newPhone);

        return $result;
    }

    /**
     * Apply changed phone.
     *
     * @param $key
     * @return SuccessMessage
     */
    public function confirmPhone($key)
    {
        $result = $this->client->request(new ConfirmPhoneMethodDescription(), ['key' => $key]);

        $this->logger->info('Phone confirmation with key '.$key);

        return $result;
    }

    /**
     * @param $email
     * @return SuccessMessage
     */
    public function changeEmail($email)
    {
        $result = $this->client->request(new ChangeEmailMethodDescription(), ['email' => $email]);

        $this->logger->info('Change email to '. $email);

        return $result;
    }

}
