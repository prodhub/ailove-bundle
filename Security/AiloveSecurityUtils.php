<?php

namespace ADW\AiloveBundle\Security;

use ADW\AiloveBundle\Security\Token\AiloveToken;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\LockedException;

/**
 * Class AiloveSecurityUtils.
 *
 * @author Artur Vesker
 */
class AiloveSecurityUtils
{
    const REGEXP = '/AiloveToken ([^"]+)/';
    const SEPARATOR = ' ';

    /**
     * @param $raw
     *
     * @return AiloveToken
     */
    public static function extractToken($raw)
    {
        try {
            $value = base64_decode($raw);

            list($key, $expire) = explode(self::SEPARATOR, $value);

            return new AiloveToken($key, $expire);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $raw
     *
     * @return AiloveToken
     */
    public static function decodeToken($raw)
    {
        return explode(self::SEPARATOR,  base64_decode($raw));
    }

    /**
     * @param $key
     * @param $expire
     *
     * @return string
     */
    public static function encodeToken($key, $expire)
    {
        return base64_encode($key.self::SEPARATOR.$expire);
    }

    /**
     * @param ClientException $e
     */
    public static function handleException(ClientException $e)
    {
        $data = json_decode($e->getResponse()->getBody(), true)['data'];

        switch ($data['error_code']) {
            case 'incorrect_login': throw new BadCredentialsException($data['error_message']);
            case 'validation_error': throw new BadCredentialsException($data['error_message']);
            case 'customer_locked': throw new LockedException($data['error_message']);
        }

        throw new AuthenticationException($data['error_message']);
    }
}
