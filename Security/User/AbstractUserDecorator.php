<?php

namespace ADW\AiloveBundle\Security\User;

use ADW\AiloveBundle\Model\Clinic;
use ADW\AiloveBundle\Model\Contact;
use ADW\AiloveBundle\Model\Institution;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Model\Vet;
use ADW\AiloveBundle\Model\VetData;
use ADW\AiloveBundle\Repository\PetRepository;
use JMS\Serializer\Annotation as Serialized;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class AbstractUserDecorator.
 *
 * @author Artur Vesker
 *
 * TODO: handle all
 */
class AbstractUserDecorator extends User implements UserInterface, UserDecoratorInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @Serialized\Type("integer")
     * @Serialized\Groups({"load"})
     */
    protected $id;

    /**
     * @var User|Vet
     *
     * @Serialized\Exclude()
     */
    protected $decorated;

    /**
     * @var PetRepository|callable
     */
    protected $lazyPetRepository;

    /**
     * @inheritdoc
     */
    public function setEmail($email)
    {
        $this->decorated->setEmail($email);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPhone($phone)
    {
        $this->decorated->setPhone($phone);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setId($id)
    {
        $this->decorated->setId($id);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getFullName()
    {
        return $this->decorated->getFullName();
    }

    /**
     * @inheritdoc
     */
    public function setType($type)
    {
        $this->decorated->setType($type);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPassword2($password2)
    {
        $this->decorated->setPassword2($password2);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPassword1($password1)
    {
        $this->decorated->setPassword1($password1);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setAvatar($avatar)
    {
        $this->decorated->setAvatar($avatar);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMainContacts()
    {
        return $this->decorated->getMainContacts();
    }

    /**
     * @inheritdoc
     */
    public function getPassword1()
    {
        return $this->decorated->getPassword1();
    }

    /**
     * @inheritdoc
     */
    public function getPassword2()
    {
        return $this->decorated->getPassword2();
    }

    /**
     * @inheritdoc
     */
    public function setVet(VetData $vetData)
    {
        $this->decorated->setVet($vetData);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPlainPassword($password)
    {
        $this->decorated->setPlainPassword($password);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addAnotherPhone($phone)
    {
        $this->decorated->addAnotherPhone($phone);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getFacebookId()
    {
        return $this->decorated->getFacebookId();
    }

    /**
     * @inheritdoc
     */
    public function setFacebookId($facebookId)
    {
        $this->decorated->setFacebookId($facebookId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getVkontakteId()
    {
        return $this->decorated->getVkontakteId();
    }

    /**
     * @inheritdoc
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->decorated->setVkontakteId($vkontakteId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getGoogleId()
    {
        return $this->decorated->getGoogleId();
    }

    /**
     * @inheritdoc
     */
    public function setGoogleId($googleId)
    {
        $this->decorated->setGoogleId($googleId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getTwitterId()
    {
        return $this->decorated->getTwitterId();
    }

    /**
     * @inheritdoc
     */
    public function setTwitterId($twitterId)
    {
        $this->decorated->setTwitterId($twitterId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getOdnoklassnikiId()
    {
        return $this->decorated->getOdnoklassnikiId();
    }

    /**
     * @inheritdoc
     */
    public function setOdnoklassnikiId($odnoklassnikiId)
    {
        $this->decorated->setOdnoklassnikiId($odnoklassnikiId);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setPets(array $pets)
    {
        $this->decorated->setPets($pets);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCats()
    {
        return $this->decorated->getCats();
    }

    /**
     * @inheritdoc
     */
    public function getDogs()
    {
        return $this->decorated->getDogs();
    }

    /**
     * @return User
     */
    public function getDecorated()
    {
        $this->init();

        return $this->decorated;
    }

    public function __construct(User $user = null)
    {
        if ($user) {
            $this->decorate($user);
        }

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function decorate(User $user)
    {
        $this->id = $user->getId();

        if ($this->decorated) {
            throw new \RuntimeException('Already Decorated');
        }

        $this->decorated = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function isDecorated()
    {
        return (boolean) $this->decorated;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if (!$this->decorated) {
            throw new \Exception('User must be decorated');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        $this->init();
        return $this->decorated->getEmail();
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        $this->init();
        return $this->decorated->getPhone();
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstName()
    {
        $this->init();

        return $this->decorated->getFirstName();
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstName($firstName)
    {
        $this->init();
        $this->decorated->setFirstName($firstName);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastName()
    {
        $this->init();
        return $this->decorated->getLastName();
    }

    /**
     * {@inheritdoc}
     */
    public function setLastName($lastName)
    {
        $this->init();
        $this->decorated->setLastName($lastName);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMiddleName()
    {
        $this->init();
        return $this->decorated->getMiddleName();
    }

    /**
     * {@inheritdoc}
     */
    public function setMiddleName($middleName)
    {
        $this->init();
        $this->decorated->setMiddleName($middleName);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getBirthDay()
    {
        $this->init();
        return $this->decorated->getBirthDay();
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        $this->init();
        return $this->decorated->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function getGender()
    {
        $this->init();
        return $this->decorated->getGender();
    }

    /**
     * {@inheritdoc}
     */
    public function getAvatar()
    {
        $this->init();
        return $this->decorated->getAvatar();
    }

    /**
     * {@inheritdoc}
     */
    public function getContacts()
    {
        $this->init();
        return $this->decorated->getContacts();
    }

    /**
     * {@inheritdoc}
     */
    public function setContacts($contacts)
    {
        $this->init();
        $this->decorated->setContacts($contacts);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        $this->init();
        return $this->decorated->getVersion();
    }

    /**
     * {@inheritdoc}
     */
    public function getVet()
    {
        $this->init();
        return $this->decorated->getVet();
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $this->init();
        if (!$type = $this->decorated->type) {
            return [];
        }
        return ['ROLE_USER', 'ROLE_'.strtoupper($type->getSystemName())];
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * {@inheritdoc}
     */
    public function setBirthDay($birthDay)
    {
        $this->init();
        $this->decorated->setBirthDay($birthDay);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setGender($gender)
    {
        $this->init();
        $this->decorated->setGender($gender);
        return $this;
    }




    /**
     * {@inheritdoc}
     */
    public function hasProcessDataPermission()
    {
        $this->init();
        return $this->decorated->hasProcessDataPermission();
    }

    /**
     * {@inheritdoc}
     */
    public function hasSendEmailPermission()
    {
        $this->init();
        return $this->decorated->hasSendEmailPermission();
    }

    /**
     * {@inheritdoc}
     */
    public function hasSendSmsPermission()
    {
        $this->init();
        return $this->decorated->hasSendSmsPermission();
    }

    /**
     * {@inheritdoc}
     */
    public function setProcessDataPermission($processDataPermission)
    {
        $this->init();
        $this->decorated->setProcessDataPermission($processDataPermission);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSendEmailPermission($sendEmailPermission)
    {
        $this->init();
        $this->decorated->setSendEmailPermission($sendEmailPermission);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSendSmsPermission($sendSmsPermission)
    {
        $this->init();
        $this->decorated->setSendSmsPermission($sendSmsPermission);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        return;
    }

    /**
     * Set pet repository loader. Loader must be return PetRepository instance for this user.
     *
     * @param callable $petRepositoryLoader
     */
    public function setPetRepositoryLoader(callable $petRepositoryLoader)
    {
        $this->lazyPetRepository = $petRepositoryLoader;
    }

    /**
     * Get pets repository for this user.
     *
     * @return PetRepository
     */
    public function getPets()
    {
        $this->init();
        if ($this->lazyPetRepository instanceof PetRepository) {
            return $this->lazyPetRepository;
        }
        if ($this->lazyPetRepository instanceof \Closure) {
            $loader = $this->lazyPetRepository;
            $this->lazyPetRepository = $loader($this->decorated);
            if (!$this->lazyPetRepository instanceof PetRepository) {
                throw new \RuntimeException('Pets repository loader must be return PetRepository instance');
            }
            return $this->lazyPetRepository;
        }
        throw new \RuntimeException('Invalid pets repository loader');
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize($this->decorated);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $user = unserialize($serialized);
        if ($user instanceof User) {
            $this->decorate($user);
        }
    }

    /**
     * @return boolean
     */
    public function isEmailVerified()
    {
        $this->init();
        return $this->decorated->isEmailVerified();
    }

    /**
     * @return boolean
     */
    public function isPhoneVerified()
    {
        $this->init();
        return $this->decorated->isPhoneVerified();
    }

    /**
     * @return int
     */
    public function getGraduationYear()
    {
        $this->assertVet();
        return $this->decorated->getGraduationYear();
    }

    /**
     * @param int $graduationYear
     *
     * @return self
     */
    public function setGraduationYear($graduationYear)
    {
        $this->assertVet();
        $this->decorated->setGraduationYear($graduationYear);

        return $this;
    }

    /**
     * @return Institution
     */
    public function getInstitution()
    {
        $this->assertVet();
        return $this->decorated->getInstitution();
    }

    /**
     * @param Institution $institution
     *
     * @return self
     */
    public function setInstitution(Institution $institution)
    {
        $this->assertVet();
        $this->decorated->setInstitution($institution);

        return $this;
    }

    /**
     * @return Institution
     */
    public function getClinic()
    {
        $this->assertVet();
        return $this->decorated->getClinic();
    }

    /**
     * @param Clinic $clinic
     *
     * @return self
     */
    public function setClinic(Clinic $clinic)
    {
        $this->assertVet();
        $this->decorated->setClinic($clinic);

        return $this;
    }
    
    /**
     * @return \ADW\AiloveBundle\Model\Contact|null
     * @throws \Exception
     */
    public function getMainAddress()
    {
        $this->init();
        return $this->decorated->getContacts()->getMainContact();
    }

    /**
     * @param Contact $contact
     * @return $this
     * @throws \Exception
     */
    public function setMainAddress(Contact $contact)
    {
        $this->init();
        $this->decorated->getContacts()->save($contact);

        return $this;
    }
    
    /**
     * @throws \Exception
     */
    private function assertVet()
    {
        $this->init();
        if (!$this->decorated instanceof Vet) {
            throw new \InvalidArgumentException('Decorated user is not vet');
        }
    }

    /**
     * @return string
     */
    public function getPhoneconfirmkey()
    {
        return $this->phoneconfirmkey;
    }

    /**
     * @param string $phoneconfirmkey
     * @return User
     */
    public function setPhoneconfirmkey($phoneconfirmkey)
    {
        $this->phoneconfirmkey = $phoneconfirmkey;
        return $this;
    }
}
