<?php

namespace ADW\AiloveBundle\Security\User;

use ADW\AiloveBundle\Model\User;

/**
 * Interface UserDecoratorInterface.
 */
interface UserDecoratorInterface
{
    public function getId();

    public function decorate(User $user);

    public function isDecorated();
}
