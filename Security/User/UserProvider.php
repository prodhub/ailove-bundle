<?php

namespace ADW\AiloveBundle\Security\User;

use ADW\AiloveBundle\AiloveEvents;
use ADW\AiloveBundle\Event\UserDecoratedEvent;
use ADW\AiloveBundle\EventListener\Doctrine\UserDecoratorSubscriber;
use ADW\AiloveBundle\Exception\InvalidUserTokenException;
use ADW\AiloveBundle\Model\User;
use ADW\AiloveBundle\Repository\UserRepository;
use ADW\AiloveBundle\RestClient\Method\Auth\GetCurrentUserMethodDescription;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use Doctrine\Common\EventManager;
use Doctrine\Common\Persistence\ObjectManager;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use ADW\RestClientBundle\Client\Client;

/**
 * Class UserProvider.
 *
 * @author Artur Vesker
 */
class UserProvider implements UserProviderInterface//, OAuthAwareUserProviderInterface
{
    /**
     * @var ObjectManager
     */
    protected $em;

    /**
     * @var \ADW\AiloveBundle\EventListener\Doctrine\UserDecoratorSubscriber
     */
    protected $userDecoratorSubscriber;

    /**
     * @var \ADW\AiloveBundle\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @var string
     */
    protected $decoratorClass;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @param Client                  $client
     * @param ObjectManager           $objectManager
     * @param UserDecoratorSubscriber $userDecoratorSubscriber
     * @param $decoratorClass
     * @param UserRepository           $userRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        Client $client,
        ObjectManager $objectManager,
        UserDecoratorSubscriber $userDecoratorSubscriber,
        $decoratorClass,
        UserRepository $userRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->client = $client;
        $this->em = $objectManager;
        $this->userDecoratorSubscriber = $userDecoratorSubscriber;
        $this->decoratorClass = $decoratorClass;
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        $users = $this->userRepository->findByEmailOrPhoneOrSocial($username);

        if (count($users) < 1) {
            throw new UsernameNotFoundException();
        }

        foreach ($users as $user) {
            if ($user->getEmail() == $user) {
                return $user;
            }
        }

        throw new UsernameNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        //In this case, user = token.
        if (!$user instanceof AiloveToken) {
            throw new \Exception('Magic not work! (Serialized user must be token)');
        }

        $user = $this->getCurrentUser($user);

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentUser(AiloveToken $ailoveToken)
    {
        try {
            /**
             * @var User
             */
            $user = $this->client->request(new GetCurrentUserMethodDescription(), ['key' => $ailoveToken->getKey()]);
            return $this->wrapUser($user);
        } catch (ClientException $e) {
            throw new InvalidUserTokenException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function wrapUser(User $user)
    {
        /**
         * @var EventManager
         */
        $eventManager = $this->em->getEventManager();

        /*
         * Temporarily remove user decorator subscriber for load custom user
         */
        $eventManager->removeEventSubscriber($this->userDecoratorSubscriber);
        $decorator = $this->em->find($this->decoratorClass, $user->getId());
        $eventManager->addEventSubscriber($this->userDecoratorSubscriber);

        if ($decorator instanceof UserDecoratorInterface) {
            if ($decorator->isDecorated()) {
                return $decorator;
            }
        }

        if (!$decorator) {
            $decorator = new $this->decoratorClass();

            if (!$decorator instanceof UserDecoratorInterface) {
                throw new \RuntimeException('Decorator must be implement UserDecoratorInterface');
            }

            $decorator->decorate($user);

            $this->em->persist($decorator);
            $this->em->flush($decorator);
        } else {
            $decorator->decorate($user);
        }

        $this->eventDispatcher->dispatch(AiloveEvents::POST_USER_DECORATED, new UserDecoratedEvent($decorator));

        return $decorator;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === $this->decoratorClass;
    }
}
