<?php

namespace ADW\AiloveBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

interface AiloveUserInterface extends UserInterface, \Serializable
{
    public function getId();
}
