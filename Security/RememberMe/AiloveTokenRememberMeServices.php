<?php

namespace ADW\AiloveBundle\Security\RememberMe;

use ADW\AiloveBundle\Security\AiloveSecurityUtils;
use ADW\AiloveBundle\Security\Token\AiloveToken;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CookieTheftException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Symfony\Component\Security\Http\ParameterBagUtils;
use Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface;

/**
 * Class AiloveTokenRememberMeServices
 *
 * @package ADW\AiloveBundle\CRM\Security\RememberMe
 * @author Artur Vesker
 */
class AiloveTokenRememberMeServices implements RememberMeServicesInterface, LogoutHandlerInterface
{

    const COOKIE_DELIMITER = ':';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $options = [
        'secure' => false,
        'httponly' => true,
    ];

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $userProviders;

    /**
     * Constructor.
     *
     * @param array           $userProviders
     * @param string          $secret
     * @param string          $providerKey
     * @param array           $options
     * @param LoggerInterface $logger
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $userProviders, $secret, $providerKey, array $options = array(), LoggerInterface $logger = null)
    {
        if (empty($secret)) {
            throw new \InvalidArgumentException('$secret must not be empty.');
        }
        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }
        if (0 === count($userProviders)) {
            throw new \InvalidArgumentException('You must provide at least one user provider.');
        }

        $this->userProviders = $userProviders;
        $this->secret = $secret;
        $this->providerKey = $providerKey;
        $this->options = array_merge($this->options, $options);
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    protected function decodeCookie($rawCookie)
    {
        return AiloveSecurityUtils::decodeToken($rawCookie);
    }

    /**
     * @inheritdoc
     */
    protected function encodeCookie(array $cookieParts)
    {
        return AiloveSecurityUtils::encodeToken($cookieParts[0], $cookieParts[1]);
    }

    /**
     * @inheritdoc
     */
    public function autoLogin(Request $request)
    {
        if (null === $cookie = $request->cookies->get($this->options['name'])) {
            return;
        }

        if (null !== $this->logger) {
            $this->logger->debug('Remember-me cookie detected.');
        }

        $cookieParts = $this->decodeCookie($cookie);

        try {
            if (null !== $this->logger) {
                $this->logger->info('Ailove token cookie accepted.');
            }

            return new AiloveToken($cookieParts[0], $cookieParts[1]);
        } catch (CookieTheftException $e) {
            $this->cancelCookie($request);

            throw $e;
        } catch (UsernameNotFoundException $e) {
            if (null !== $this->logger) {
                $this->logger->info('User for remember-me cookie not found.');
            }
        } catch (UnsupportedUserException $e) {
            if (null !== $this->logger) {
                $this->logger->warning('User class for remember-me cookie not supported.');
            }
        } catch (AuthenticationException $e) {
            if (null !== $this->logger) {
                $this->logger->debug('Remember-Me authentication failed.', array('exception' => $e));
            }
        }

        $this->cancelCookie($request);
    }

    /**
     * @internal
     */
    public function loginSuccess(Request $request, Response $response, TokenInterface $token)
    {
        // Make sure any old remember-me cookies are cancelled
        $this->cancelCookie($request);

        if (!$token->getUser() instanceof UserInterface) {
            if (null !== $this->logger) {
                $this->logger->debug('Remember-me ignores token since it does not contain a UserInterface implementation.');
            }

            return;
        }

        if (!$request->get('_save_token', false)) {
            if (null !== $this->logger) {
                $this->logger->debug('Remember-me was not requested.');
            }

            return;
        }

        if (null !== $this->logger) {
            $this->logger->debug('Remember-me was requested; setting cookie.');
        }

        // Remove attribute from request that sets a NULL cookie.
        // It was set by $this->cancelCookie()
        // (cancelCookie does other things too for some RememberMeServices
        // so we should still call it at the start of this method)
        $request->attributes->remove(self::COOKIE_ATTR_NAME);

        if (!$token instanceof AiloveToken) {
            throw new \InvalidArgumentException('Invalid token type');
        }

        $response->headers->setCookie(
            new Cookie(
                $this->options['name'],
                $this->encodeCookie([$token->getKey(), $token->getExpire()]),
                $this->options['lifetime'] === null ? $token->getExpire() : $this->options['lifetime'],
                $this->options['path'],
                $this->options['domain'],
                $this->options['secure'],
                $this->options['httponly']
            )
        );
    }

    /**
     * @internal
     */
    protected function cancelCookie(Request $request)
    {
        if (null !== $this->logger) {
            $this->logger->debug('Clearing remember-me cookie.', array('name' => $this->options['name']));
        }

        $request->attributes->set(self::COOKIE_ATTR_NAME, new Cookie($this->options['name'], null, 1, $this->options['path'], $this->options['domain'], $this->options['secure'], $this->options['httponly']));
    }

    /**
     * @internal
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        $this->cancelCookie($request);
    }

    /**
     * @internal
     */
    final public function loginFail(Request $request)
    {
        $this->cancelCookie($request);
    }

}