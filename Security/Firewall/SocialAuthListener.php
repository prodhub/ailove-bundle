<?php

namespace ADW\AiloveBundle\Security\Firewall;

use ADW\AiloveBundle\Security\Token\AiloveToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;

/**
 * Class SocialNetworkListener.
 *
 * @author Artur Vesker
 */
class SocialAuthListener extends AbstractAuthenticationListener
{

    /**
     * @inheritdoc
     */
    protected function attemptAuthentication(Request $request)
    {
        if (!$key = $request->get('key')) {
            throw new BadRequestHttpException('Missing token');
        }

        return $this->authenticationManager->authenticate(new AiloveToken($key));
    }

    /**
     * @inheritdoc
     */
    protected function requiresAuthentication(Request $request)
    {
        return ($this->httpUtils->checkRequestPath($request, $this->options['check_path'])) && ($request->get('auth_status') === 'is_auth');
    }

}
