<?php

namespace ADW\AiloveBundle\Security\Firewall;

use ADW\AiloveBundle\Security\Token\ContactPasswordAiloveToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;

/**
 * Class ContactPasswordListener.
 *
 * @author Artur Vesker
 */
class ContactPasswordListener extends AbstractAuthenticationListener
{

    /**
     * @inheritdoc
     */
    protected function attemptAuthentication(Request $request)
    {
        $request->attributes->set('_save_token', true);
        return $this->authenticationManager->authenticate(new UsernamePasswordToken($request->get('username', ''), $request->get('password', null), $this->providerKey));
    }

    protected function requiresAuthentication(Request $request)
    {
        return ($request->getRealMethod() === 'POST') && parent::requiresAuthentication($request);
    }


}
