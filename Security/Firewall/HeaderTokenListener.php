<?php

namespace ADW\AiloveBundle\Security\Firewall;

use ADW\AiloveBundle\Security\AiloveSecurityUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

/**
 * Class HeaderTokenListener.
 *
 * @author Artur Vesker
 *
 * TODO: json response
 */
class HeaderTokenListener implements ListenerInterface
{
    const REGEXP = '/AiloveToken ([^"]+)/';
    const SEPARATOR = '%|%';

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $header = $event->getRequest()->headers->get('Authorization');

        if (!$header) {
            return;
        }

        $matches = [];
        preg_match(self::REGEXP, $header, $matches);

        if (!isset($matches[1])) {
            return;
        }

        $rawToken = $matches[1];

        try {
            $token = AiloveSecurityUtils::extractToken($rawToken);

            if (!$token) {
                throw new AuthenticationException('Неверный токен');
            }

            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);

            return;
        } catch (AuthenticationException $e) {
            $response = new Response($e->getMessage() ?: 'Неверный токен', 403);
            $event->setResponse($response);
        }
    }
}
