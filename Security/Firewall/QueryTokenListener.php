<?php

namespace ADW\AiloveBundle\Security\Firewall;

use ADW\AiloveBundle\Security\AiloveSecurityUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\Security\Http\RememberMe\RememberMeServicesInterface;

/**
 * Class QueryTokenListener.
 *
 * @author Artur Vesker
 */
class QueryTokenListener implements ListenerInterface
{

    /**
     * @var AuthenticationManagerInterface
     */
    protected $authenticationManager;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var RememberMeServicesInterface|null
     */
    protected $rememberMeServices;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param array $options
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, array $options)
    {
        $this->options = $options;
        $this->authenticationManager = $authenticationManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Sets the RememberMeServices implementation to use.
     *
     * @param RememberMeServicesInterface $rememberMeServices
     */
    public function setRememberMeServices(RememberMeServicesInterface $rememberMeServices)
    {
        $this->rememberMeServices = $rememberMeServices;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $rawToken = $event->getRequest()->query->get($this->options['name']);

        if (!$rawToken) {
            return;
        }

        try {
            $token = AiloveSecurityUtils::extractToken($rawToken);

            if (!$token) {
                throw new AuthenticationException();
            }

            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
            return;
        } catch (AuthenticationException $e) {
            $event->setResponse(new RedirectResponse($event->getRequest()->getPathInfo()));
        }
    }

}
