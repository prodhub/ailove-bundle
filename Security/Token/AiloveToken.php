<?php

namespace ADW\AiloveBundle\Security\Token;

use ADW\AiloveBundle\Security\User\AbstractUserDecorator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class AiloveToken.
 *
 * @author Artur Vesker
 *
 * @Serialized\ExclusionPolicy("ALL")
 */
class AiloveToken implements TokenInterface, UserInterface
{
    /**
     * @var AbstractUserDecorator
     */
    protected $user;

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var bool
     */
    protected $authenticated = false;

    /**
     * @var string
     *
     * @Serialized\Expose()
     * @Serialized\Type("string")
     */
    protected $key;

    /**
     * @var int
     *
     * @Serialized\Expose()
     * @Serialized\Type("integer")
     */
    protected $expire;

    /**
     * @param string|null $key
     * @param string|null $expire
     */
    public function __construct($key = null, $expire = null)
    {
        $this->key = $key;
        $this->expire = $expire;

        $this->authenticated = (time() < $expire);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return int
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser($user)
    {
        if (!($user instanceof UserInterface || (is_object($user) && method_exists($user, '__toString')) || is_string($user))) {
            throw new \InvalidArgumentException('$user must be an instanceof UserInterface, an object implementing a __toString method, or a primitive string.');
        }

        if ($user instanceof UserInterface) {
            $this->setRoles($user->getRoles());
        }

        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    private function setRoles($roles)
    {
        $this->roles = [];

        foreach ($roles as $role) {
            if (is_string($role)) {
                $role = new Role($role);
            } elseif (!$role instanceof RoleInterface) {
                throw new \InvalidArgumentException(sprintf('$roles must be an array of strings, or RoleInterface instances, but got %s.', gettype($role)));
            }

            $this->roles[] = $role;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        if ($this->user instanceof UserInterface) {
            return $this->user->getUsername();
        }

        return (string) $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function isAuthenticated()
    {
        return ($this->expire ? $this->expire > time() : true) && $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
        $this->authenticated = (bool) $authenticated;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        if ($this->getUser() instanceof UserInterface) {
            $this->getUser()->eraseCredentials();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(
            [
                $this->attributes,
                $this->key,
                $this->expire,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list($this->attributes, $this->key, $this->expire) = unserialize($serialized);
        $this->user = $this;
        $this->refreshAuthenticationStatus();
    }

    /**
     * Returns the token attributes.
     *
     * @return array The token attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Sets the token attributes.
     *
     * @param array $attributes The token attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Returns true if the attribute exists.
     *
     * @param string $name The attribute name
     *
     * @return bool true if the attribute exists, false otherwise
     */
    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * Returns an attribute value.
     *
     * @param string $name The attribute name
     *
     * @return mixed The attribute value
     *
     * @throws \InvalidArgumentException When attribute doesn't exist for this token
     */
    public function getAttribute($name)
    {
        if (!array_key_exists($name, $this->attributes)) {
            throw new \InvalidArgumentException(sprintf('This token has no "%s" attribute.', $name));
        }

        return $this->attributes[$name];
    }

    /**
     * Sets an attribute.
     *
     * @param string $name  The attribute name
     * @param mixed  $value The attribute value
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        $class = get_class($this);
        $class = substr($class, strrpos($class, '\\') + 1);

        $roles = array();
        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }

        return sprintf('%s(user="%s", authenticated=%s, roles="%s")', $class, $this->getUsername(), json_encode($this->authenticated), implode(', ', $roles));
    }

    /**
     * @Serialized\PostDeserialize()
     */
    public function refreshAuthenticationStatus()
    {
        $this->authenticated = ($this->expire ? $this->expire > time() : true) && $this->user;
    }

    /**
     * @internal
     */
    public function getPassword()
    {
        throw new \RuntimeException('Use user');
    }

    /**
     * @internal
     */
    public function getSalt()
    {
        throw new \RuntimeException('Use user');
    }

}
