<?php

namespace ADW\AiloveBundle\Security\Token;

use ADW\AiloveBundle\CRM\Security\Token;

/**
 * Class DirectAiloveToken.
 *
 * @author Artur Vesker
 */
class DirectAiloveToken extends AiloveToken
{
    public function __construct(Token $apiToken = null)
    {
        parent::__construct($apiToken);
        $this->setAuthenticated(false);
    }
}
