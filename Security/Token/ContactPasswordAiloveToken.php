<?php

namespace ADW\AiloveBundle\Security\Token;

use ADW\AiloveBundle\CRM\Security\Token;

/**
 * Class ContactPasswordAiloveToken.
 *
 * @author Artur Vesker
 */
class ContactPasswordAiloveToken extends AiloveToken
{
    const TYPE_VET = 'vet';
    const TYPE_OWNER = 'owner';
    const TYPE_ANY = 'any';

    /**
     * @var string
     */
    protected $contact;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $type;

    /**
     * @param string $contact
     * @param string $password
     */
    public function __construct($contact, $password, $type = self::TYPE_ANY, Token $apiToken = null)
    {
        $this->contact = $contact;
        $this->password = $password;
        $this->type = $type;
        parent::__construct($apiToken);
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
