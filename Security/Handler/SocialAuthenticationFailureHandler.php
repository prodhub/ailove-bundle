<?php

namespace ADW\AiloveBundle\Security\Handler;

use ADW\AiloveBundle\Exception\SocialAuthenticationException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

/**
 * Class SocialAuthenticationFailureHandler.
 *
 * @author Artur Vesker
 */
class SocialAuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    /**
     * @var Router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if (!$exception instanceof SocialAuthenticationException) {
            return;
        }

        $response = $exception->getUserResponse();

        $id = $response->getUsername();
        $provider = $response->getResourceOwner()->getName();
        $avatar = $response->getProfilePicture();

        return new RedirectResponse($this->router->generate('registration', [
            'provider' => $provider,
            'id' => $id,
            'avatar' => $avatar,
        ]));
    }
}
